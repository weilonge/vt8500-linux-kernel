/*++
	drivers/input/touchscreen/wmt/spi/ts_hx.c

	Some descriptions of such software. Copyright (c) 2008  WonderMedia Technologies, Inc.

	This program is free software: you can redistribute it and/or modify it under the
	terms of the GNU General Public License as published by the Free Software Foundation,
	either version 2 of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE.  See the GNU General Public License for more details.
	You should have received a copy of the GNU General Public License along with
	this program.  If not, see <http://www.gnu.org/licenses/>.

	WonderMedia Technologies, Inc.
	10F, 529, Chung-Cheng Road, Hsin-Tien, Taipei 231, R.O.C.

	History:
		2009/04/27 First Version

--*/
#include <linux/errno.h>
#include <linux/input.h>
#include <linux/device.h>
#include <asm/mach-types.h>
#include <mach/gpio.h>
#include <linux/interrupt.h>
#include <mach/hardware.h>

#include <mach/wmt_spi.h>
#include "wmt_ts.h"


/*touchscreen AD7843*/
#define ad7843_start_bit BIT7
#define ad7843_a2_bit BIT6
#define ad7843_a1_bit BIT5
#define ad7843_a0_bit BIT4
#define ad7843_mode_bit BIT3
#define ad7843_ser_bit BIT2
#define ad7843_pd1_bit BIT1
#define ad7843_pd0_bit BIT0

/*cmd*/
#define VERIFY_ID 0xE0
#define CMD_X_8BIT (ad7843_start_bit|ad7843_a0_bit|ad7843_mode_bit|ad7843_ser_bit)
#define CMD_Y_8BIT (ad7843_start_bit|ad7843_a2_bit|ad7843_a0_bit|ad7843_mode_bit|ad7843_ser_bit)
/*
#define CMD_X_12BIT (ad7843_start_bit|ad7843_a0_bit|ad7843_ser_bit|ad7843_pd1_bit);
#define CMD_Y_12BIT (ad7843_start_bit|ad7843_a2_bit|ad7843_a0_bit|ad7843_ser_bit|ad7843_pd1_bit);
*/
#define CMD_X_12BIT (ad7843_start_bit|ad7843_a0_bit|ad7843_ser_bit);
#define CMD_Y_12BIT (ad7843_start_bit|ad7843_a2_bit|ad7843_a0_bit|ad7843_ser_bit);
//#define CMD_BAT_12BIT (ad7843_start_bit|ad7843_a1_bit|ad7843_ser_bit);
#define CMD_BAT_12BIT (ad7843_start_bit|ad7843_a2_bit|ad7843_a1_bit|ad7843_ser_bit); // Bow
#define AD7843_ID 0x800

struct spi_user_rec_s *spi_user_rec_p;
#define SPI_USER_ID 0
#define SPI_USER_NAME "ad7843"
#define SPI_FREQ 3/*3k*/
#define GPIO_TS_SPI_INT 2
#define GPIO_TS_SPI_IND_MODE 0 /*input mode*/

#define SET_GPIO_TS_INT() {\
	REG32_VAL(wmt_ts_gpt.ctraddr) |= wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.ocaddr) &= ~wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.peaddr) |= wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.pcaddr) |= wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.isaddr) |= wmt_ts_gpt.isbmp; \
}


struct wmt_gpt_s {
	char name[10];
	unsigned int bitmap;
	unsigned int ctraddr;
	unsigned int ocaddr;
	unsigned int idaddr;
	unsigned int peaddr;
	unsigned int pcaddr;
	unsigned int itbmp;
	unsigned int itaddr;
	unsigned int isbmp;
	unsigned int isaddr;
	unsigned int irq;
};

static struct wmt_gpt_s wmt_ts_gpt = {
	.name = "cs7146",
	.bitmap = 0x04,/*GPIO 2*/
	.ctraddr = 0xd8110040,
	.ocaddr = 0xd8110080,
	.idaddr = 0xd8110000,
	.peaddr = 0xd8110480,
	.pcaddr = 0xd81104c0,
	.itbmp = 0x00800000, /* low level */
	.itaddr = 0xd8110300,
	.isbmp = 0x4,
	.isaddr = 0xd8110320,
	.irq = 6,
};

static int hx_ts_penup(void);
static int hx_ts_probe(struct wmt_ts_t *ts);
static void hx_ts_read(unsigned int flag);
static void hx_ts_enable(struct wmt_ts_t *ts);
static void hx_ts_disable(void);
#ifdef	MODULE
static void hx_ts_remove(void);
#endif
static void hx_ts_clear_irq(void);
static void hx_ts_parse_data(u16 *data, u8 value1, u8 value2);
static void hx_ts_set_freq(unsigned int);
static unsigned int is_cs7146;
/*
static unsigned int gpio_ts_spi_int = 2;
*/

extern int wmt_getsyspara(char *varname, unsigned char *varval, int *varlen);
static void ts_enable_gpio(unsigned int gpio_num, int type)/*type 0 :input, 1: output*/
{
	SET_GPIO_TS_INT();
}

static int ts_gpio_get_value(unsigned int gpio_num, int type)/*type 0:input, 1:output*/
{
	return (REG32_VAL(wmt_ts_gpt.idaddr) & wmt_ts_gpt.bitmap) == (wmt_ts_gpt.bitmap) ? 1 : 0;
}

struct ts_device hx_ts = {
	.probe 		= hx_ts_probe,
	.read 		= hx_ts_read,
	.enable 	= hx_ts_enable,
	.disable 	= hx_ts_disable,
	.remove 	= __exit_p(hx_ts_remove),
	.penup 		= hx_ts_penup,
	.clear_irq	= hx_ts_clear_irq,
	.parse_data     = hx_ts_parse_data,
	.frequency      = SPI_FREQ,
	.set_freq       = hx_ts_set_freq,
};

static int hx_ts_penup(void)
{
	int ret = 0;
	if (ts_gpio_get_value(GPIO_TS_SPI_INT, GPIO_TS_SPI_IND_MODE))
		return ret;
	return !ret;

}

#if 0
static int verify_ts(void)
{
	unsigned char wbuf[3];
	unsigned char rbuf[3];
	unsigned short id ;
	wbuf[0] = VERIFY_ID;
	wbuf[1] = 0;
	wbuf[2] = 0;
	rbuf[0] = 0;
	rbuf[1] = 0;
	rbuf[2] = 0;

	spi_write_and_read_data(spi_user_rec_p, wbuf, rbuf, 3);
	id = (rbuf[1] << 5) | (rbuf[2] >> 3);
	if (id != AD7843_ID) {
		printk(KERN_ERR "id[0x%x] was wrong\n", id);
		return 1;
	}
	return 0 ;
}
#endif

static void hx_ts_clear_irq(void)
{
	/*
	GPIO_INT_REQ_STS_VAL = BIT2;
	*/
	REG32_VAL(wmt_ts_gpt.isaddr) = wmt_ts_gpt.isbmp;
}

static void hx_ts_set_freq(unsigned int freq)
{
	spi_set_freq(spi_user_rec_p, (int)freq);
}
/*
 * set the time which ostimer irq happen
 * ostimer irq would not happened until delay_time(ms) was arrival
 */
int hx_ts_set_ostimer_irq(unsigned int delay_time)
{
	unsigned int val, sw_count;
	unsigned int trigger_time;
	int ret = 0;
	val = REG32_VAL(OSTC_ADDR);
	sw_count = 300000;
	if ((val&0x02) == 0) {
		val |= 0x02;
		REG32_VAL(OSTC_ADDR) = val;
	}
	while(1) {
		val = REG32_VAL(OSTA_ADDR);
		if ((val&0x20) != 0x20)
			break;
		if (--sw_count == 0) {
			ret = -1;
			printk(KERN_ERR "Read OST Count Request Failed\n");
			break;
		}
	}
	val = REG32_VAL(OSCR_ADDR);
	trigger_time = val;
	sw_count = 300000;
	while(1) {
		val = REG32_VAL(OSTA_ADDR);
		if ((val&0x08) != 0x08)
			break;
		if (--sw_count == 0) {
			ret = -1;
			printk(KERN_ERR "OST Match3 Request Failed\n");
			break;
		}
	}
	REG32_VAL(OSM3_ADDR) = trigger_time + delay_time*3*1000;
	REG32_VAL(OSTS_ADDR) = 0x08;
	REG32_VAL(OSTI_ADDR) |= 0x08;
	return ret;
}

int hx_ts_set_ostimer1_irq(unsigned int delay_time)
{
	unsigned int val, sw_count;
	unsigned int trigger_time;
	int ret = 0;
	val = REG32_VAL(OSTC_ADDR);
	sw_count = 300000;
	if ((val&0x02) == 0) {
		val |= 0x02;
		REG32_VAL(OSTC_ADDR) = val;
	}
	while(1) {
		val = REG32_VAL(OSTA_ADDR);
		if ((val&0x20) != 0x20)
			break;
		if (--sw_count == 0) {
			ret = -1;
			printk(KERN_ERR "Read OST Count Request Failed\n");
			break;
		}
	}
	val = REG32_VAL(OSCR_ADDR);
	trigger_time = val;
	sw_count = 300000;
	while(1) {
		val = REG32_VAL(OSTA_ADDR);
		if ((val&0x04) != 0x04)
			break;
		if (--sw_count == 0) {
			ret = -1;
			printk(KERN_ERR "OST Match2 Request Failed\n");
			break;
		}
	}
	REG32_VAL(OSM2_ADDR) = trigger_time + delay_time*3*1000;
	REG32_VAL(OSTS_ADDR) = 0x04;
	REG32_VAL(OSTI_ADDR) |= 0x04;
	return ret;
}
void hx_ts_clear_ostimer_irq(void)
{
	REG32_VAL(OSTS_ADDR) = 0x08;
	REG32_VAL(OSTI_ADDR) &= ~0x08;
}

void hx_ts_clear_ostimer1_irq(void)
{
	REG32_VAL(OSTS_ADDR) = 0x04;
	REG32_VAL(OSTI_ADDR) &= ~0x04;
}

void hx_ts_set_callback(void (*callback)(unsigned char *))
{
	set_callback_func(spi_user_rec_p, callback);
}

static void parse_gpt_arg(void)
{
	char *varname = "wmt.gpt.ts";
	unsigned int varlen = 160;
	unsigned char buf[160];
	int retval;
	int i = 0;
	retval = wmt_getsyspara(varname, buf, &varlen);
	if (retval != 0)
		return;
	if (buf[0] != 'c')
		return;
	if (buf[1] != 's')
		return;
	if (buf[2] != '7')
		return;
	if (buf[3] != '1')
		return;
	if (buf[4] != '4')
		return;
	if (buf[5] != '6')
		return;
	i = 7;
	sscanf((buf + i), "%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x",
			&wmt_ts_gpt.bitmap,
			&wmt_ts_gpt.ctraddr,
			&wmt_ts_gpt.ocaddr,
			&wmt_ts_gpt.idaddr,
			&wmt_ts_gpt.peaddr,
			&wmt_ts_gpt.pcaddr,
			&wmt_ts_gpt.itbmp,
			&wmt_ts_gpt.itaddr,
			&wmt_ts_gpt.isbmp,
			&wmt_ts_gpt.isaddr,
			&wmt_ts_gpt.irq);
		
	/*
	printk("wmt.gpt.ts = %x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x\n",
		wmt_ts_gpt.bitmap,
		wmt_ts_gpt.ctraddr,
		wmt_ts_gpt.ocaddr,
		wmt_ts_gpt.idaddr,
		wmt_ts_gpt.peaddr,
		wmt_ts_gpt.pcaddr,
		wmt_ts_gpt.itbmp,
		wmt_ts_gpt.itaddr,
		wmt_ts_gpt.isbmp,
		wmt_ts_gpt.isaddr,
		wmt_ts_gpt.irq);
	*/
}

static int __init hx_ts_probe(struct wmt_ts_t *ts)
{
	ts->irq = IRQ_GPIO;
	ts->ostimer_irq = IRQ_OST3;
	ts->ostimer1_irq = IRQ_OST2;
	ts->irq_type = IRQF_SHARED;
	ts->ts_delay_time = 20;
	is_cs7146 = ts->touch_use;
	parse_gpt_arg();
	if (wmt_ts_gpt.itbmp & 0xff)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff00;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff0000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff000000;
	ts_enable_gpio(GPIO_TS_SPI_INT, GPIO_TS_SPI_IND_MODE);
	hx_ts_clear_irq();
	/* Register a SPI channel.*/
	spi_user_rec_p = register_user(SPI_USER_NAME, SPI_USER_ID);
	/*Configure SPI Mode*/
	spi_set_arbiter(spi_user_rec_p, SPI_ARBITER_MASTER);  /*SPI as Master*/
	spi_set_op_mode(spi_user_rec_p, SPI_POLLING_MODE);
	spi_set_port_mode(spi_user_rec_p, SPI_SSN_PORT_PTP); /*Point to Point*/
	spi_set_clk_mode(spi_user_rec_p, SPI_MODE_3); /*(polarity, phase) => (1, 1)*/
	spi_set_freq(spi_user_rec_p, SPI_FREQ);
	if (ts->callback)
		set_callback_func(spi_user_rec_p, ts->callback);
	else
		printk("no spi callback func\n");
	printk(KERN_INFO "[touchscreen] Set SPI%d Frequence to %d.%dMHz\n",
	spi_user_rec_p->spi_port->port, SPI_FREQ/1000, (SPI_FREQ % 1000)/100);
#if 0
	if (verify_ts()) {
		printk(KERN_INFO "AD7843 did not detect\n");
		unregister_user(spi_user_rec_p, SPI_USER_ID);
		return 1;
	}
#endif
	spi_set_op_mode(spi_user_rec_p, SPI_INTERRUPT_MODE);
	printk(KERN_INFO "Touchscreen detected \n");
	return 0;
}

static void hx_ts_read(unsigned int flag)
{
	/*read X*/
	if (flag == 0) {
		hx_ts.wbuf[0] = CMD_X_12BIT;
		hx_ts.wbuf[1] = 0;
		hx_ts.wbuf[2] = 0;
		hx_ts.rbuf[0] = 0;
		hx_ts.rbuf[1] = 0;
		hx_ts.rbuf[2] = 0;
		spi_write_and_read_data(spi_user_rec_p, hx_ts.wbuf, hx_ts.rbuf, 3);
	} else if (flag == 1) {/*read Y*/
		hx_ts.wbuf[0] = CMD_Y_12BIT;
		hx_ts.wbuf[1] = 0;
		hx_ts.wbuf[2] = 0;
		hx_ts.rbuf[0] = 0;
		hx_ts.rbuf[1] = 0;
		hx_ts.rbuf[2] = 0;
		spi_write_and_read_data(spi_user_rec_p, hx_ts.wbuf, hx_ts.rbuf, 3);
	} else {/*read battery status*/
		hx_ts.wbuf[0] = CMD_BAT_12BIT;
		hx_ts.wbuf[1] = 0;
		hx_ts.wbuf[2] = 0;
		hx_ts.rbuf[0] = 0;
		hx_ts.rbuf[1] = 0;
		hx_ts.rbuf[2] = 0;
		spi_write_and_read_data(spi_user_rec_p, hx_ts.wbuf, hx_ts.rbuf, 3);
	}
}

static void hx_ts_parse_data(u16 *data, u8 value1, u8 value2)
{
	*data = (value1 << 5) | (value2 >> 3);
}

static void hx_ts_enable(struct wmt_ts_t *ts)
{
	if (wmt_ts_gpt.itbmp & 0xff)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff00;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff0000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff000000;
	ts_enable_gpio(GPIO_TS_SPI_INT, GPIO_TS_SPI_IND_MODE);
	hx_ts_clear_irq();
	/* Register a SPI channel.*/
	spi_user_rec_p = register_user(SPI_USER_NAME, SPI_USER_ID);
	/*Configure SPI Mode*/
	spi_set_arbiter(spi_user_rec_p, SPI_ARBITER_MASTER);  /*SPI as Master*/
	spi_set_op_mode(spi_user_rec_p, SPI_POLLING_MODE);
	spi_set_port_mode(spi_user_rec_p, SPI_SSN_PORT_PTP); /*Point to Point*/
	spi_set_clk_mode(spi_user_rec_p, SPI_MODE_3); /*(polarity, phase) => (1, 1)*/
	spi_set_freq(spi_user_rec_p, SPI_FREQ);
	if (ts->callback)
		set_callback_func(spi_user_rec_p, ts->callback);
	spi_set_op_mode(spi_user_rec_p, SPI_INTERRUPT_MODE);
	hx_ts_enable_penirq();
	return;
}

static void hx_ts_disable(void)
{
	unregister_user(spi_user_rec_p, SPI_USER_ID);
}

void hx_ts_enable_penirq(void)
{
	
	if (wmt_ts_gpt.itbmp & 0xff)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x80;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x8000;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x800000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x80000000;
}

int hx_ts_check_irq_en(void)
{
	int ret = 0;
	if (wmt_ts_gpt.itbmp & 0xff) {
		if (REG32_VAL(wmt_ts_gpt.itaddr) & 0x80)
			ret = 1;
		else
			ret = 0;
	} else if (wmt_ts_gpt.itbmp & 0xff00) {
		if (REG32_VAL(wmt_ts_gpt.itaddr) & 0x8000)
			ret = 1;
		else
			ret = 0;
	} else if (wmt_ts_gpt.itbmp & 0xff0000) {
		if (REG32_VAL(wmt_ts_gpt.itaddr) & 0x800000)
			ret = 1;
		else
			ret = 0;
	} else {
		if (REG32_VAL(wmt_ts_gpt.itaddr) & 0x80000000)
			ret = 1;
		else
			ret = 0;
	}
	return ret;
}

void hx_ts_disable_penirq(void)
{
	if (wmt_ts_gpt.itbmp & 0xff)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x80;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x8000;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x800000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x80000000;
}

int hx_ts_read_irqstatus(void)
{
	return REG32_VAL(wmt_ts_gpt.isaddr) & wmt_ts_gpt.isbmp;
}

#ifdef	MODULE
static void __exit hx_ts_remove(void)
{
}
#endif
