/*++
	Copyright (c) 2008  WonderMedia Technologies, Inc.

	This program is free software: you can redistribute it and/or modify it under the
	terms of the GNU General Public License as published by the Free Software Foundation,
	either version 2 of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE.  See the GNU General Public License for more details.
	You should have received a copy of the GNU General Public License along with
	this program.  If not, see <http://www.gnu.org/licenses/>.

	WonderMedia Technologies, Inc.
	10F, 529, Chung-Cheng Road, Hsin-Tien, Taipei 231, R.O.C.
--*/

#include <linux/init.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/input.h>
#include <linux/random.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/i2c.h>
#include <linux/i2c-id.h>
#include <linux/platform_device.h>
#include <mach/hardware.h>
#include <mach/wmt_gpio.h>
#include "vt1603a_mt_i2c.h"

/* to select which platform vt1603 worked on */
#define WMT_VT3465

/* select gpio as irq input according to your specific hardware */
#ifdef WMT_VT3465
#define WMT_VT3465_USE_GPIO0
//#define WMT_VT3465_USE_GPIO2
#endif

//#define DEBUG
#ifdef  DEBUG
#define ts_dbg(fmt, args...) printk(KERN_ERR "[%s]_%d: " fmt, __func__ , __LINE__, ## args)
#define ts_trace()           printk(KERN_ERR "trace in %s %d\n", __func__, __LINE__);
#else
#define ts_dbg(fmt, args...)
#define ts_trace()
#endif

//#define CAL_DBG
#ifdef  CAL_DBG 
#define cal_dbg(fmt, args...) printk(KERN_ERR "[%s]_%d: " fmt, __func__ , __LINE__, ## args)
#define cal_trace()           printk(KERN_ERR "trace in %s %d\n", __func__, __LINE__);
#else
#define cal_dbg(fmt, args...)
#define cal_trace()
#endif

//#define TM_DBG
#ifdef  TM_DBG
static struct timeval tts;
#endif

/* 
 * vt1603_ts_i2c_fix_addr - fix vt1603 i2c address, this function MUST
 *    be invoked before seting/getting vt1603's register via i2c bus
 * @i2c:  client for vt1603 (as i2c slave)
 */
static inline void vt1603_ts_i2c_fix_addr(struct i2c_client *i2c)
{
	i2c->addr = VT1603_I2C_FIX_ADDR;
}

/*
 * vt1603_ts_i2c_fake_addr - fake vt1603 i2c address, this function MUST
 *    be invoked after setting/getting vt1603's register via i2c bus
 * @i2c:  client for vt1603 (as i2c slave)
 */
static inline void vt1603_ts_i2c_fake_addr(struct i2c_client *i2c)
{
	i2c->addr = VT1603_I2C_FAKE_ADDR;
}

/*
 * vt1603_ts_i2c_write - write a u8 data to vt1603 via i2c bus
 * @i2c:  client for vt1603 (as i2c slave)
 * @addr: vt1603 register address
 * @data: data write to register
 */
#ifdef I2C_SRC_SYNC
static int vt1603_ts_i2c_write(struct i2c_client *i2c, u8 reg, char *data)
{
	int ret = 0;
	struct i2c_msg msg;
	unsigned char buf[2];

	buf[0] = reg;
	buf[1] = data[0];

	vt1603_ts_i2c_fix_addr(i2c);
	msg.addr  = i2c->addr;
	msg.flags = VT1603_TS_I2C_WCMD & I2C_CLIENT_TEN ;
	msg.len   = 2;
	msg.buf   = buf;
	ret = wmt_i2c_xfer_continue_if_4(&msg, 1, 0);
	vt1603_ts_i2c_fake_addr(i2c);

	return ret;
}
#else
static int vt1603_ts_i2c_write(struct i2c_client *i2c, u8 reg, u8 *data)
{
	int ret = 0;
	u8 cmd[2] = {0};

	cmd[0] = reg;
	cmd[1] = data[0];
	vt1603_ts_i2c_fix_addr(i2c);
	ret = i2c_master_send(i2c, cmd, 2);
	vt1603_ts_i2c_fake_addr(i2c);
	return ret;
}
#endif

/*
 * vt1603_ts_i2c_read - read a u8 data to vt1603 via i2c bus
 * @i2c:  client for vt1603 (as i2c slave)
 * @addr: vt1603 register address
 * @data: data read form register
 */
static int vt1603_ts_i2c_read(struct i2c_client *i2c, u8 reg, u8 *data)
{
	int i = 100;
	int ret = 0;
	struct i2c_msg msg[2] = { {0} };

	/* fix i2c address first */
	vt1603_ts_i2c_fix_addr(i2c);
	/* build msg to write    */
	msg[0].flags = VT1603_TS_I2C_RWCMD;
	msg[0].addr  = i2c->addr;
	msg[0].len   = 1;
	msg[0].buf   = &reg;
	/* build msg to read     */
	msg[1].flags = VT1603_TS_I2C_RCMD;
	msg[1].addr  = i2c->addr;
	msg[1].len   = 1;
	msg[1].buf   = data;
#ifdef I2C_SRC_SYNC
	/* start data transfer   */
	while (i--) {
	    ret = wmt_i2c_xfer_continue_if_4(msg, 2, VT1603_I2C_BUS_ID);
	    if (ret == 2)
		break;
	    udelay(1);
	}
#else
	/* start data transfer   */
	while (i--) {
	    ret = i2c_transfer(i2c->adapter, msg, 2);
	    if (ret == 2)
		break;
	    udelay(1);
	}
#endif
	/* fake i2c address last */
	vt1603_ts_i2c_fake_addr(i2c);
	/* ret == 2, means transfer success */
	if (ret == 2)
	    ret = 0;
	return ret;
}

/*
 * vt1603_set_reg8 - set register value of vt1603
 * @ts_drv: vt1603 driver data
 * @reg: vt1603 register address
 * @val: value register will be set
 */
static int
vt1603_set_reg8(struct vt1603_ts_drvdata *ts_drv, u8 reg, u8 val)
{
	return vt1603_ts_i2c_write(ts_drv->i2c, reg, &val);
}

/*
 * vt1603_get_reg8 - get register value of vt1603
 * @ts_drv: vt1603 driver data
 * @reg: vt1603 register address
 */
static u8 vt1603_get_reg8(struct vt1603_ts_drvdata *ts_drv, u8 reg)
{
	u8  val = 0;
	int ret = 0;

	ret = vt1603_ts_i2c_read(ts_drv->i2c, reg, &val);
	if (ret)
		ts_dbg("vt1603 ts i2c read error, errno%d\n", ret);
	return val;
}

/*
 * vt1603_reg_dump - dubug function, for dump vt1603 related registers
 * @ts_drv: vt1603 driver data
 */
static void vt1603_reg_dump(struct vt1603_ts_drvdata *ts_drv)
{
	u8 i;    
	for (i = 0; i < 15; i++)
		ts_dbg("reg[%d]:0x%02X,  reg[%d]:0x%02X\n", 
		    i, vt1603_get_reg8(ts_drv, i), 
		    i + 0xC0, vt1603_get_reg8(ts_drv, i + 0xC0));
}

/*
 * vt1603_setbits - write bit1 to related register's bit
 * @ts_drv: vt1603 driver data
 * @reg: vt1603 register address
 * @mask: bit setting mask
 */
static void
vt1603_setbits(struct vt1603_ts_drvdata *ts_drv, u8 reg, u8 mask)
{
	u8 tmp = vt1603_get_reg8(ts_drv, reg) | mask;
	vt1603_set_reg8(ts_drv, reg, tmp);
}

/*
 * vt1603_clr_ts_irq -  clear touch panel pen down/up and 
 *    conversion end/timeout interrupts
 * @ts_drv: vt1603 driver data
 * @mask: which interrupt will be cleared
 */
static int vt1603_clr_ts_irq(struct vt1603_ts_drvdata *ts_drv, u8 mask)
{
	vt1603_setbits(ts_drv, VT1603_INTS_REG, mask);
	return 0;
}

#ifdef VT1603_TS_EN
/*
 * vt1603_ts_get_pen_state - get touch panel pen state from vt1603 
 *   interrup status register
 * @ts_drv: vt1603 driver data
 */
static int vt1603_ts_get_pen_state(struct vt1603_ts_drvdata *ts_drv)
{
	u8 state = vt1603_get_reg8(ts_drv, VT1603_INTS_REG);

	if (state & BIT4)
		return TS_PENUP_STATE;
	else
		return TS_PENDOWN_STATE;
}

/*
 * vt1603_ts_report_penup - report touch panel pen up event to
 *    kernel input system
 * @ts_drv: vt1603 driver data
 */
static void vt1603_ts_report_penup(struct vt1603_ts_drvdata *ts_drv)
{
	if (ts_drv->ts_state == TS_PENUP_STATE)
		return ;

	input_report_key(ts_drv->input, BTN_TOUCH, 0);
	input_sync(ts_drv->input);
}

/*
 * vt1603_ts_report_pos - report touch panel touched position to
 *     kernel input system
 * @ts_drv: vt1603 driver data
 * @pos: vt1603 touch panel touched point conversion data
 */
static int panelres_x;
static int panelres_y;
static int g_bCalibrating = false;
static struct vt1603_ts_cal_info g_CalcParam;
static int vt1603_ts_is_calibration(void)
{
	return g_bCalibrating;
}

static int vt1603_ts_get_resolvY(void)
{
	return panelres_y;
}

static int vt1603_ts_get_resolvX(void)
{
	return panelres_x;
}

static int vt1603_ts_pos_calibration(struct vt1603_ts_pos *to_cal)
{
	int x, y;

	x = (g_CalcParam.a1 * to_cal->xpos + g_CalcParam.b1 * to_cal->ypos +
	     g_CalcParam.c1) / g_CalcParam.delta;
	y = (g_CalcParam.a2 * to_cal->xpos + g_CalcParam.b2 * to_cal->ypos +
	     g_CalcParam.c2) / g_CalcParam.delta;

	/* pos check */
	if (x < 0)
	    x = 0;
	if (y < 0)
	    y = 0;
	if (x > vt1603_ts_get_resolvX())
	    x = vt1603_ts_get_resolvX();
	if (y > vt1603_ts_get_resolvY())
	    y = vt1603_ts_get_resolvY();

	to_cal->xpos = x;
	to_cal->ypos = y;
	return 0;
}

static struct vt1603_ts_event g_evLast;
static void vt1603_ts_set_rawdata(struct vt1603_ts_pos *pos)
{
	g_evLast.x = pos->xpos;
	g_evLast.y = pos->ypos;
}

static void vt1603_ts_report_pos(struct vt1603_ts_drvdata *ts_drv,
                                 struct vt1603_ts_pos *pos)
{
	if (vt1603_ts_is_calibration()) {
		cal_trace();
		vt1603_ts_set_rawdata(pos);
		pos->xpos = 10;
		pos->ypos = 10;
	} else
		vt1603_ts_pos_calibration(pos);

	cal_dbg("pos caled:x-pos[%d], y-pos[%d]\n", pos->xpos, pos->ypos);
	input_report_abs(ts_drv->input, ABS_X, pos->xpos);
	input_report_abs(ts_drv->input, ABS_Y, pos->ypos);
	input_report_key(ts_drv->input, BTN_TOUCH, 1);
	input_sync(ts_drv->input);
}

/*
 * vt1603_ts_get_pos - get touch panel touched position from vt1603
 *     conversion register
 * @ts_drv: vt1603 driver data
 * @pos: vt1603 touch panel touched point conversion data
 */
static void 
vt1603_ts_get_pos(struct vt1603_ts_drvdata *ts_drv, struct vt1603_ts_pos *pos)
{
	u8 datal, datah;

	/* get x-position */
	datal = vt1603_get_reg8(ts_drv, VT1603_XPL_REG);
	datah = vt1603_get_reg8(ts_drv, VT1603_XPH_REG);
	pos->xpos = ADC_DATA(datal, datah);
	/* get y-positin */
	datal = vt1603_get_reg8(ts_drv, VT1603_YPL_REG);
	datah = vt1603_get_reg8(ts_drv, VT1603_YPH_REG);
	pos->ypos = ADC_DATA(datal, datah);
}

static void vt1603_ts_penup_tmr_isr(unsigned long ts_drv_addr)
{
	unsigned long flags;
	struct vt1603_ts_drvdata *ts_drv = NULL;

	ts_dbg("Enter\n");
#ifdef TM_DBG
	do_gettimeofday(&tts);
	ts_dbg("tts:%dsec, %dusec\n", (int)tts.tv_sec, (int)tts.tv_usec);
#endif
	ts_drv = (struct vt1603_ts_drvdata *)ts_drv_addr;
	spin_lock_irqsave(&ts_drv->spinlock, flags);

	if (ts_drv->ts_state == TS_PENUP_STATE)
		goto out;

	vt1603_ts_report_penup(ts_drv);
	ts_drv->ts_state = TS_PENUP_STATE;

out:
	spin_unlock_irqrestore(&ts_drv->spinlock, flags);
	printk(KERN_ERR "penup timer timeout\n");
	ts_dbg("Exit\n\n\n");
}

/*
 * vt1603_ts_open_penup_tmr - timer for detecte pen up state
 * @ts_drv: vt1603 driver data
 */
static void vt1603_ts_open_penup_tmr(struct vt1603_ts_drvdata *ts_drv)
{
	mod_timer(&ts_drv->penup_tmr, 
		jiffies + msecs_to_jiffies(PENUP_DETECT_TIMEOUT));
}

static void vt1603_ts_close_penup_tmr(struct vt1603_ts_drvdata *ts_drv)
{
	del_timer_sync(&ts_drv->penup_tmr);
}

static void vt1603_ts_init_penup_tmr(struct vt1603_ts_drvdata *ts_drv)
{
	setup_timer(&ts_drv->penup_tmr, vt1603_ts_penup_tmr_isr, (u32)ts_drv);
}

#ifdef WMT_VT3465
#ifdef WMT_VT3465_USE_GPIO0
/* we use GPIO0 as the interrupt signal input on WM8650(VT3465) platform */
static void wm8650_gpio_irq_init(struct vt1603_ts_drvdata *ts_drv)
{
	/* clear GPIO interrupts first */
	GPIO_INT_REQ_STS_VAL = BIT0;
	msleep(5);
	/* as gpio                  */
	GPIO_CTRL_GP0_BYTE_VAL |= BIT0;
	/* as input                 */
	GPIO_OC_GP0_BYTE_VAL &= ~BIT0;
	/* disable pull up/down     */
	GPIO_PULL_EN_GP0_BYTE_VAL &= ~BIT0;

	/* reset trigger type first */
	GPIO_INT_REQ_TYPE_0_VAL &= ~(BIT0 | BIT1 | BIT2);
	/* set trigger type         */
	if (ts_drv->pdata->irq_type == HIGH_ACTIVE)
		 GPIO_INT_REQ_TYPE_0_VAL |= BIT0;
	else if (ts_drv->pdata->irq_type == RISING_EDGE_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL |= BIT0 | BIT1;
	else if (ts_drv->pdata->irq_type == LOW_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL &= ~(BIT0 | BIT1 | BIT2);
	else if (ts_drv->pdata->irq_type == FALLING_EDGE_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL |= BIT1;
	else
		printk(KERN_ERR "SOC irq trigger type error\n");
}

static void wm8650_gpio_irq_disable(struct vt1603_ts_drvdata *ts_drv)
{
	GPIO_INT_REQ_TYPE_0_VAL &= ~BIT7;
}

static void wm8650_gpio_irq_enable(struct vt1603_ts_drvdata *ts_drv)
{
	GPIO_INT_REQ_TYPE_0_VAL |= BIT7;
}

static void wm8650_gpio_irq_clear(struct vt1603_ts_drvdata *ts_drv)
{
	GPIO_INT_REQ_STS_VAL |= BIT0;
}
#endif /* WMT_VT3465_USE_GPIO0 */

#ifdef WMT_VT3465_USE_GPIO2
/* we use GPIO2 as the interrupt signal input on WM8650(VT3465) platform */
static void wm8650_gpio_irq_init(struct vt1603_ts_drvdata *ts_drv)
{
	/* clear GPIO interrupts first */
	GPIO_INT_REQ_STS_VAL = BIT2;
	msleep(5);
	/* as gpio                  */
	GPIO_CTRL_GP0_BYTE_VAL |= BIT2;
	/* as input                 */
	GPIO_OC_GP0_BYTE_VAL &= ~BIT2;
	/* disable pull up/down     */
	GPIO_PULL_EN_GP0_BYTE_VAL &= ~BIT2;

	/* reset trigger type first */
	GPIO_INT_REQ_TYPE_0_VAL &= ~(BIT16 | BIT17 | BIT18);
	/* set trigger type         */
	if (ts_drv->pdata->irq_type == HIGH_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL |= BIT16;
	else if (ts_drv->pdata->irq_type == RISING_EDGE_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL |= BIT16 | BIT17;
	else if (ts_drv->pdata->irq_type == LOW_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL &= ~(BIT16 | BIT17 | BIT18);
	else if (ts_drv->pdata->irq_type == FALLING_EDGE_ACTIVE)
		GPIO_INT_REQ_TYPE_0_VAL |= BIT17;
	else
		printk(KERN_ERR "SOC irq trigger type error\n");
}

static void wm8650_gpio_irq_disable(struct vt1603_ts_drvdata *ts_drv)
{
	GPIO_INT_REQ_TYPE_0_VAL &= ~BIT23;
}

static void wm8650_gpio_irq_enable(struct vt1603_ts_drvdata *ts_drv)
{
	GPIO_INT_REQ_TYPE_0_VAL |= BIT23;
}

static void wm8650_gpio_irq_clear(struct vt1603_ts_drvdata *ts_drv)
{
	GPIO_INT_REQ_STS_VAL |= BIT2;
}
#endif /* WMT_VT3465_USE_GPIO2 */
#endif /* WMT_VT3465           */
#else  /* VT1603_TS_EN         */
static void none_gpio_irq_init(struct vt1603_ts_drvdata *ts_drv)
{
	return ;
}

static void none_gpio_irq_enable(struct vt1603_ts_drvdata *ts_drv)
{
	return ;
}

static void none_gpio_irq_disable(struct vt1603_ts_drvdata *ts_drv)
{
	return ;
}

static void none_gpio_irq_clear(struct vt1603_ts_drvdata *ts_drv)
{
	return ;
}
#endif /* VT1603_TS_EN */

#ifdef WMT_VT3465
/*
 * vt1603_ts_clk_enable - open mclk for vt1603 touch-panel 
 *    and sar-adc module
 */
static void vt1603_ts_init_i2s_for_mclk(void)
{
#ifndef MODULE 
	int temp ;

	/* BIT0:enable I2S MCLK, BIT4:select to I2S, BIT9,BIT10:select to SPDIF,
	   BIT16,BIT17:select to sus_gpio0 and sus_gpio1 */
	GPIO_PIN_SHARING_SEL_4BYTE_VAL |= (BIT0);
	GPIO_PIN_SHARING_SEL_4BYTE_VAL &= ~(BIT4);

	//GPIO_PIN_SHARING_SEL_4BYTE_VAL &= ~(BIT16 | BIT17);

	/* disable GPIO and Pull Down mode for I2S and SPDIF */
	GPIO_CTRL_GP10_I2S_BYTE_VAL &= ~(0xFF);
	GPIO_PULL_EN_GP10_I2S_BYTE_VAL &= ~(0xFF);

	/* enable GPIO function of Suspend GPIO#0 for audio mute gpio */
	//GPIO_CTRL_GP2_WAKEUP_SUS_BYTE_VAL |= BIT5;
	//GPIO_OC_GP2_WAKEUP_SUS_BYTE_VAL |= BIT5;

	/* mute disable */
	//GPIO_OD_GP2_WAKEUP_SUS_BYTE_VAL |= BIT5;

	/* set clock to 12.288MHz */
	do {
		temp = PMCSH_VAL;
	} while (temp);

	temp = 0;
	temp |= (BIT0 | BIT5);
	temp &= ~(BIT1 | BIT2 | BIT4);
	PMAUD_VAL = temp;

	do{
		temp = PMCSH_VAL;
	} while (temp);

	/* enable I2S clock */
	PMCEL_VAL |= BIT6;
#endif /* #ifndef MODULE */
}

static void vt1603_ts_clk_enable(void)
{
#if 0
#define BA_PMC          0xD8130000
#define BA_GPIO         0xD8110000
#define BA_I2S          0xD80ED800
	int status = 0;
	/* BIT0:enable I2S MCLK, BIT4:select to I2S */
	REG32_VAL(BA_GPIO + 0x200) |= BIT0 | BIT9;
	REG32_VAL(BA_GPIO + 0x200) &= ~(BIT4 | BIT10);
	/* disable GPIO and Pull Down mode for I2S and SPDIF */
	REG32_VAL(BA_GPIO + 0x4A) &= ~(0xFF);
	REG32_VAL(BA_GPIO + 0x4B) &= ~(BIT5);

	REG32_VAL(BA_GPIO + 0x48A) &= ~(0xFF);
	REG32_VAL(BA_GPIO + 0x48B) &= ~(BIT5);

	/* set clock to 12.288MHz */
	status |= (BIT0 | BIT5);
	status &= ~(BIT1 | BIT2 | BIT4);
	REG32_VAL(BA_PMC + 0x220) = status;
	/* enable I2S clock */
	REG32_VAL(BA_PMC + 0x250) |= BIT6;
#endif
	vt1603_ts_init_i2s_for_mclk();
}
#endif /* #ifdef WMT_VT3465 */

/*
 * vt1603_gpio1_reset - vt1603 gpio1 configuration, 
 *  gpio1 as interrupt output, low active
 * @ts_drvdata_addr: address of vt1603 driver data
 */
static void vt1603_gpio1_reset(struct vt1603_ts_drvdata *ts_drv)
{
#ifdef VT1603_TS_EN
	/* mask other module interrupts      */
	vt1603_set_reg8(ts_drv, VT1603_IMASK_REG27, 0xFF);
	vt1603_set_reg8(ts_drv, VT1603_IMASK_REG28, 0xFF);
	vt1603_set_reg8(ts_drv, VT1603_IMASK_REG29, 0xFF);
	/* irq output form gpio1 high active */
	if (ts_drv->pdata->irq_type == HIGH_ACTIVE
		|| ts_drv->pdata->irq_type == RISING_EDGE_ACTIVE)
		vt1603_set_reg8(ts_drv, VT1603_IPOL_REG33, 0xDF);
	else
		vt1603_set_reg8(ts_drv, VT1603_IPOL_REG33, 0xFF);
	/* vt1603 gpio1 as IRQ output        */
	vt1603_set_reg8(ts_drv, VT1603_ISEL_REG36, 0x04);
#else
	return ;
#endif
}

/*
 * vt1603_work_mode_switch - switch VT1603 working mode (ts/bat/temp) 
 * @ts_drv: vt1603 driver data
 * @mode: vt1603 working mode
 */
static void
vt1603_work_mode_switch(struct vt1603_ts_drvdata *ts_drv, u8 mode)
{
	ts_dbg("Enter\n");    

	switch (mode) {
	case VT1603_BAT_MODE:
		ts_drv->mode = VT1603_BAT_MODE;
		vt1603_set_reg8(ts_drv, VT1603_AMCR_REG, BIT0);
		vt1603_set_reg8(ts_drv, VT1603_CR_REG, BIT3);
		vt1603_set_reg8(ts_drv, VT1603_CDPR_REG, 0xFF);
		break;
	case VT1603_TEMP_MODE:
		ts_drv->mode = VT1603_TEMP_MODE;
		vt1603_set_reg8(ts_drv, VT1603_AMCR_REG, 0x00);
		vt1603_set_reg8(ts_drv, VT1603_CR_REG, BIT2);
		vt1603_set_reg8(ts_drv, VT1603_CDPR_REG, 0xFF);
		break;
	case VT1603_TS_MODE:
	default:
		ts_drv->mode = VT1603_TS_MODE;
		ts_drv->ts_state = TS_PENUP_STATE;
#ifdef VT1603_TS_EN
	ts_drv->ts_state = vt1603_ts_get_pen_state(ts_drv);
	vt1603_set_reg8(ts_drv, VT1603_CDPR_REG, ts_drv->pdata->sclk_div);
	if (ts_drv->ts_type == PANEL_TYPE_4WIRED)
		vt1603_set_reg8(ts_drv, VT1603_CR_REG, BIT1);
	else
		vt1603_set_reg8(ts_drv, VT1603_CR_REG, BIT1 | BIT0);
#endif
	break;
	}
	msleep(5);
	vt1603_clr_ts_irq(ts_drv, BIT0 | BIT1 | BIT2 | BIT3);

	ts_dbg("Exit\n");
	return ;
}

#if defined(VT1603_BAT_EN) || defined(VT1603_TEMP_EN)
static u16 vt1603_cal_adc_avg(u16 *data, int num)
{
	int i = 0;
	int avg = 0;

	for (i = 0; i < num; i++)
	    avg += data[i];

	return (u16)(avg / num);
}
#endif

#ifdef VT1603_BAT_EN
/*
 * vt1603_bat - global data for battery information maintenance
 */
static struct vt1603_bat_info vt1603_bat;

/*
 * vt1603_get_bat_info - get battery status, API for wmt_battery.c
 */
unsigned short vt1603_get_bat_info(void)
{
	return vt1603_bat.bat_val;
}
EXPORT_SYMBOL(vt1603_get_bat_info);

/*
 * vt1603_get_bat_convert_data - get battery converted data
 * @ts_drv: vt1603 driver data
 */
static u16 vt1603_get_bat_convert_data(struct vt1603_ts_drvdata *ts_drv)
{
	u8 data_l, data_h;
	data_l = vt1603_get_reg8(ts_drv, VT1603_BATL_REG);
	data_h = vt1603_get_reg8(ts_drv, VT1603_BATH_REG);

	return ADC_DATA(data_l, data_h);
}

/*
 * vt1603_bat_tmr_isr - vt1603 battery detect timer isr
 * @ts_drvdata_addr: address of vt1603 driver data
 */
static void vt1603_bat_tmr_isr(unsigned long ts_drvdata_addr)
{
	unsigned long flags;
	struct vt1603_ts_drvdata *ts_drv;
	struct vt1603_bat_info *bat;

	ts_drv = (struct vt1603_ts_drvdata *)ts_drvdata_addr;
	bat    = ts_drv->bat_info;

	ts_dbg("Enter\n");
#ifdef TM_DBG
	do_gettimeofday(&tts);
	ts_dbg("tts:%dsec, %dusec\n", (int)tts.tv_sec, (int)tts.tv_usec);
#endif
	spin_lock_irqsave(&ts_drv->spinlock, flags);
	/* vt1603 in temp mode, delay bat detecting       */
	if (unlikely((ts_drv->mode == VT1603_TEMP_MODE)
		     || (ts_drv->ts_state == TS_PENDOWN_STATE))) {
		ts_dbg("vt1603 in other mode, delay bat detecting 1000msecs\n");
		mod_timer(&bat->bat_tmr, jiffies + msecs_to_jiffies(1000));
		goto out;
	}
	/* disable gpio interrupt input first             */
	ts_drv->soc_gpio_irq_disable(ts_drv);
	/* mark vt1603 working mode                       */
	ts_drv->mode = VT1603_BAT_MODE;
	/* doing battery state detecting in workqueue     */
	schedule_work(&bat->work);

out:
	spin_unlock_irqrestore(&ts_drv->spinlock, flags);
	ts_dbg("Exit\n");
	return ;
}

/*
 * vt1603_bat_work - vt1603 battery workqueue routine, switch 
 *  vt1603 working mode to battery detecting
 * @work: battery work struct
 */
static void vt1603_bat_work(struct work_struct *work)
{
	int i, j;
	unsigned long flags;
	struct vt1603_bat_info *bat;
	struct vt1603_ts_drvdata *ts_drv;
	u16 bat_val[DFLT_BAT_VAL_AVG] = { 0 };

	bat = container_of(work, struct vt1603_bat_info, work);
	ts_drv = bat->ts_drv;

	ts_dbg("Enter\n");
	spin_lock_irqsave(&ts_drv->spinlock, flags);

	if (unlikely(ts_drv->mode != VT1603_BAT_MODE))
		goto out;
#ifdef VT1603_TS_EN
	/* close os timer if ts penup now */
	if (unlikely(ts_drv->ts_state == TS_PENDOWN_STATE))
		vt1603_ts_close_penup_tmr(ts_drv);
#endif
	/* switch to battery detecting mode */
	vt1603_work_mode_switch(ts_drv, VT1603_BAT_MODE);
	/* waitting for conversion end */
	for (i = 0; i < DFLT_BAT_VAL_AVG; i++) {
		for (j = 0; j < 100; j++){
			if (vt1603_get_reg8(ts_drv, VT1603_INTS_REG) & BIT0)
				break;
			udelay(10);
		}
		bat_val[i] = vt1603_get_bat_convert_data(ts_drv);
		vt1603_clr_ts_irq(ts_drv, 0x0F);
	}
	/* get battery conversion data */
	ts_drv->bat_info->bat_val = vt1603_cal_adc_avg(bat_val, DFLT_BAT_VAL_AVG);
	bat->detect_time++;
	bat->time_stamp = jiffies;
	ts_dbg("bat_end:bat_data is %d\n", ts_drv->bat_info->bat_val);
	vt1603_work_mode_switch(ts_drv, VT1603_TS_MODE);
	/* modify battery timer expires and restart timer */
	mod_timer(&bat->bat_tmr, 
		jiffies + msecs_to_jiffies(bat->interval*1000 + random32()%1000));

out:
	spin_unlock_irqrestore(&ts_drv->spinlock, flags);
#ifdef TM_DBG
	do_gettimeofday(&tts);
	ts_dbg("tts:%dsec, %dusec\n", (int)tts.tv_sec, (int)tts.tv_usec);
#endif
	ts_dbg("Exit\n\n\n");

	ts_drv->soc_gpio_irq_clear(ts_drv);
	ts_drv->soc_gpio_irq_enable(ts_drv);
	return ;
}

/*
 * vt1603_bat_info_init - vt1603 battery initialization
 * @ts_drvdata_addr: address of vt1603 driver data
 */
static void
vt1603_bat_info_init(struct vt1603_ts_drvdata *ts_drv)
{
	struct vt1603_bat_info *bat = ts_drv->bat_info;

	bat->interval     = DFLT_POLLING_BAT_INTERVAL;
	bat->bat_val      = 0;
	bat->detect_time  = 0;
	bat->ts_drv       = ts_drv;

	INIT_WORK(&bat->work, vt1603_bat_work);
	setup_timer(&bat->bat_tmr, vt1603_bat_tmr_isr, (unsigned long)ts_drv);
	mod_timer(&bat->bat_tmr, 
	    jiffies + msecs_to_jiffies(bat->interval*1000 + random32()%1000));
	bat->time_stamp = jiffies;
}
#endif

#ifdef VT1603_TEMP_EN
/*
 * vt1603_temp - global data for temperature information maintenance
 */
static struct vt1603_temp_info vt1603_temp;

/*
 * vt1603_get_temp_info - get temprature status, API for other module
 */
unsigned short vt1603_get_temp_info(void)
{
	return vt1603_temp.temp_val;
}
EXPORT_SYMBOL(vt1603_get_temp_info);

/*
 * vt1603_get_temp_convert_data - get temperature converted data
 * @ts_drv: vt1603 driver data
 */
static u16 vt1603_get_temp_convert_data(struct vt1603_ts_drvdata *ts_drv)
{
	u8 data_l, data_h;
	data_l = vt1603_get_reg8(ts_drv, VT1603_TEMPL_REG);
	data_h = vt1603_get_reg8(ts_drv, VT1603_TEMPH_REG);

	return ADC_DATA(data_l, data_h);
}

/*
 * vt1603_temp_tmr_isr - vt1603 temperature detect timer isr
 * @ts_drvdata_addr: address of vt1603 driver data
 */
static void vt1603_temp_tmr_isr(unsigned long ts_drvdata_addr)
{
	unsigned long flags;
	struct vt1603_ts_drvdata *ts_drv;
	struct vt1603_temp_info *temp;

	ts_drv = (struct vt1603_ts_drvdata *)ts_drvdata_addr;
	temp   = ts_drv->temp_info;

	ts_dbg("Enter\n");
#ifdef TM_DBG
	do_gettimeofday(&tts);
	ts_dbg("tts:%dsec, %dusec\n", (int)tts.tv_sec, (int)tts.tv_usec);
#endif
	spin_lock_irqsave(&ts_drv->spinlock, flags);
	/* vt1603 in bat mode, delay temp detecting    */
	if (unlikely((ts_drv->mode == VT1603_BAT_MODE)
		     || (ts_drv->ts_state == TS_PENDOWN_STATE))) {
		ts_dbg("vt1603 in other mode, delay temp detecting 1000msecs\n");
		mod_timer(&temp->temp_tmr, jiffies + msecs_to_jiffies(1000));
		goto out;
	}
	/* disable gpio interrupt input first          */
	ts_drv->soc_gpio_irq_disable(ts_drv);
	/* mark vt1603 working mode                    */
	ts_drv->mode = VT1603_TEMP_MODE;
	/* doing temp state detecting in workqueue     */
	schedule_work(&temp->work);

out:
	spin_unlock_irqrestore(&ts_drv->spinlock, flags);
	ts_dbg("Exit\n");
	return ;
}

/*
 * vt1603_temp_work - vt1603 temperature workqueue routine, switch 
 *  vt1603 working mode to temperature detecting
 * @work: temperature work struct
 */
static void vt1603_temp_work(struct work_struct *work)
{
	int i, j;
	unsigned long flags;
	struct vt1603_temp_info *temp;
	struct vt1603_ts_drvdata *ts_drv;
	u16 temp_val[DFLT_TEMP_VAL_AVG] = { 0 };

	temp = container_of(work, struct vt1603_temp_info, work);
	ts_drv = temp->ts_drv;

	ts_dbg("Enter\n");
	spin_lock_irqsave(&ts_drv->spinlock, flags);

	if (unlikely(ts_drv->mode != VT1603_TEMP_MODE))
		goto out;
#ifdef VT1603_TS_EN
	/* close os timer if ts penup now         */
	if (unlikely(ts_drv->ts_state == TS_PENDOWN_STATE))
		vt1603_ts_close_penup_tmr(ts_drv);
#endif
	/* switch to temperature detecting mode   */
	vt1603_work_mode_switch(ts_drv, VT1603_TEMP_MODE);
	/* waitting for conversion end */
	for (i = 0; i < DFLT_TEMP_VAL_AVG; i++) {
		for (j = 0; j < 100; j++) {
			if (vt1603_get_reg8(ts_drv, VT1603_INTS_REG) & BIT0)
				break;
			udelay(10);
		}
		temp_val[i] = vt1603_get_temp_convert_data(ts_drv);
		vt1603_clr_ts_irq(ts_drv, 0x0F);
	}
	/* get temperature conversion data */
	ts_drv->temp_info->temp_val = vt1603_cal_adc_avg(temp_val, DFLT_TEMP_VAL_AVG);
	temp->detect_time++;
	temp->time_stamp = jiffies;
	ts_dbg("temp_end:temp_data is %d\n", ts_drv->temp_info->temp_val);
	vt1603_work_mode_switch(ts_drv, VT1603_TS_MODE);
	/* modify temperature timer expires and restart timer */
	mod_timer(&temp->temp_tmr, 
		jiffies + msecs_to_jiffies(temp->interval*1000 + random32()%1000));

out:
	spin_unlock_irqrestore(&ts_drv->spinlock, flags);
#ifdef TM_DBG
	do_gettimeofday(&tts);
	ts_dbg("tts:%dsec, %dusec\n", (int)tts.tv_sec, (int)tts.tv_usec);
#endif
	ts_dbg("Exit\n\n\n");

	ts_drv->soc_gpio_irq_clear(ts_drv);
	ts_drv->soc_gpio_irq_enable(ts_drv);
	return ;
}

/*
 * vt1603_temp_info_init - vt1603 temperature initialization
 * @ts_drvdata_addr: address of vt1603 driver data
 */
static void
vt1603_temp_info_init(struct vt1603_ts_drvdata *ts_drv)
{
	struct vt1603_temp_info *temp = ts_drv->temp_info;

	temp->interval    = DFLT_POLLING_TEMP_INTERVAL;
	temp->temp_val    = 0;
	temp->detect_time = 0;
	temp->ts_drv      = ts_drv;

	INIT_WORK(&temp->work, vt1603_temp_work);
	setup_timer(&temp->temp_tmr, vt1603_temp_tmr_isr, 
	    (unsigned long)ts_drv);
	mod_timer(&temp->temp_tmr, 
	    jiffies + msecs_to_jiffies(temp->interval*1000 + random32()%1000));
	temp->time_stamp = jiffies;
}
#endif


#ifdef VT1603_TS_EN
/*
 * vt1603_irq_type_check - check the interrupts type that getting from 
 *   vt1603 gpio1 configuration
 * @ts_drvdata_addr: address of vt1603 driver data
 */
#define VT1603_IRQ_STATUS   (vt1603_get_reg8(ts_drv, VT1603_INTS_REG))
static int vt1603_irq_type_check(struct vt1603_ts_drvdata *ts_drv)
{
	u8 input_ch   = vt1603_get_reg8(ts_drv, VT1603_AMCR_REG);
	u8 adc_mode   = vt1603_get_reg8(ts_drv, VT1603_CR_REG);

	/* is other module irq   */
	if (unlikely(!(VT1603_IRQ_STATUS & 0x0F)))
		return VT1603_OTHER_MODULE_IRQ;

	msleep(2);
	/* is ts pen down irq    */
	if ((VT1603_IRQ_STATUS & BIT1) && !(VT1603_IRQ_STATUS & BIT4))
		return VT1603_TS_PENDOWN_IRQ;

	/* is ts pen up irq      */
	if ((VT1603_IRQ_STATUS & BIT2) && (VT1603_IRQ_STATUS & BIT4))
		return VT1603_TS_PENUP_IRQ;

	/* is conversion end irq */
	if (likely(VT1603_IRQ_STATUS & BIT0)) {
		/* is ts conversion end irq              */
		if (adc_mode & BIT1)
			return VT1603_TS_CONVEND_IRQ;
		/* is temperature conversion end irq     */
		else if ((adc_mode & BIT2) && (input_ch == 0))
			return VT1603_TEMP_CONVEND_IRQ;
		/* is battery conversion end irq         */
		else if ((adc_mode & BIT3) && (input_ch & BIT0))
			return VT1603_BAT_CONVEND_IRQ;
		else
			/* unkown irq type                       */
			return VT1603_UNKOWN_IRQ;
	}

	/* is conversion timeout irq                 */
	if (VT1603_IRQ_STATUS & BIT3) {
		/* is ts conversion timeout irq          */
		if (adc_mode & BIT1)
			return VT1603_TS_CONVTOUT_IRQ;
		/* is temperature conversion timeout irq */
		else if ((adc_mode & BIT2) && (input_ch == 0))
			return VT1603_TEMP_CONVTOUT_IRQ;
		/* is battery conversion timeout irq     */
		else if ((adc_mode & BIT3) && (input_ch & BIT0))
			return VT1603_BAT_CONVTOUT_IRQ;
		else
			/* unkown irq type */
			return VT1603_UNKOWN_IRQ;
	}

	/* unkown irq type */
	return VT1603_UNKOWN_IRQ; 
}

/* TODO: we do not report the firest point, it is better for calibration */
static void vt1603_ts_pendown(struct vt1603_ts_drvdata *ts_drv)
{
	//struct vt1603_ts_pos pos;

	//vt1603_ts_get_pos(ts_drv, &pos);
	ts_dbg("ts_pendown:ints_reg is 0x%02x\n", VT1603_IRQ_STATUS);
	vt1603_clr_ts_irq(ts_drv, BIT1 | BIT0);
	ts_drv->soc_gpio_irq_clear(ts_drv);
	//vt1603_ts_report_pos(ts_drv, &pos);
	//ts_drv->ts_state = TS_PENDOWN_STATE;
	//vt1603_ts_open_penup_tmr(ts_drv);
}

static void vt1603_ts_penup(struct vt1603_ts_drvdata *ts_drv)
{
	vt1603_ts_close_penup_tmr(ts_drv);
	ts_dbg("ts_penup:ints_reg is 0x%02x\n", VT1603_IRQ_STATUS);
	vt1603_clr_ts_irq(ts_drv, BIT3 | BIT2 | BIT1 | BIT0);
	ts_drv->soc_gpio_irq_clear(ts_drv);
	vt1603_ts_report_penup(ts_drv);
	ts_drv->ts_state = TS_PENUP_STATE;
}

static void vt1603_ts_convert_end(struct vt1603_ts_drvdata *ts_drv)
{
	struct vt1603_ts_pos pos;

	vt1603_ts_close_penup_tmr(ts_drv);
	vt1603_ts_get_pos(ts_drv, &pos);
	ts_dbg("convert_end:x-pos[%d], y-pos[%d]\n", pos.xpos, pos.ypos);        
	vt1603_clr_ts_irq(ts_drv, BIT0);
	ts_drv->soc_gpio_irq_clear(ts_drv); 
	vt1603_ts_report_pos(ts_drv, &pos);
	ts_drv->ts_state = TS_PENDOWN_STATE;
	vt1603_ts_open_penup_tmr(ts_drv);
}

/*
 * vt1603_ts_work - vt1603 touch-panel/sar-adc interrupt isr bottom half,
 *    all interrupts will processed here
 *
 *  TODO: We don't do battery/temperature detect in ts_work any more, so if 
 *  received a battery/temperature related irq in ts_work, that means
 *  something is wrong. Please invoke vt1603_work_mode_switch to switch
 *  vt1603 into touch-mode to resolve this issue.
 *
 * @work: vt1603 driver work struct
 */
static void vt1603_ts_work(struct work_struct *work)
{
	int irq_type;
	unsigned long flags;
	struct vt1603_ts_drvdata *ts_drv;

	ts_drv = container_of(work, struct vt1603_ts_drvdata, work);
	spin_lock_irqsave(&ts_drv->spinlock, flags);
	irq_type = vt1603_irq_type_check(ts_drv);

	ts_dbg("Enter, irq_type:%d, ints_reg:0x%02x\n", irq_type,
			vt1603_get_reg8(ts_drv, VT1603_INTS_REG));    
	switch (irq_type) {
	case VT1603_OTHER_MODULE_IRQ:
		ts_drv->soc_gpio_irq_clear(ts_drv);
		break;
	case VT1603_TS_PENDOWN_IRQ:
		vt1603_ts_pendown(ts_drv);
		break;
	case VT1603_TS_CONVEND_IRQ:
		vt1603_ts_convert_end(ts_drv);
		break;
	case VT1603_TS_PENUP_IRQ:
		vt1603_ts_penup(ts_drv);
		break;
#ifdef VT1603_BAT_EN
	case VT1603_BAT_CONVEND_IRQ:
	case VT1603_BAT_CONVTOUT_IRQ:
#endif
#ifdef VT1603_TEMP_EN
	case VT1603_TEMP_CONVEND_IRQ:
	case VT1603_TEMP_CONVTOUT_IRQ:
#endif
	case VT1603_TS_CONVTOUT_IRQ:
	case VT1603_UNKOWN_IRQ:
	default:
		vt1603_work_mode_switch(ts_drv, VT1603_TS_MODE);
		vt1603_clr_ts_irq(ts_drv, BIT3 | BIT2 | BIT1 | BIT0);
		ts_drv->soc_gpio_irq_clear(ts_drv);
		break;
	}

	spin_unlock_irqrestore(&ts_drv->spinlock, flags);
	ts_dbg("EXIT\n\n\n");

	ts_drv->soc_gpio_irq_enable(ts_drv);
	return ;
}

/*
 * vt1603_ts_isr - vt1603 ts/bat/temp module interrupt routine
 *
 * TODO: we just close soc gpio interrupt input and schedule the
 *    workqueue here, the soc gpio interrupt will open in workqueue
 *    routine function, so we can ensure every intrrupt 
 *    will be handled 
 *
 * @irq: irq number
 * @dev_id: irq handler data, actually is vt1603 driver data
 */
static irqreturn_t vt1603_ts_isr(int irq, void *dev_id)
{
	struct vt1603_ts_drvdata *ts_drv = dev_id;

#ifdef WMT_VT3465_USE_GPIO0
	u32 tmp = BIT0;
#endif
#ifdef WMT_VT3465_USE_GPIO2
	u32 tmp = BIT2;
#endif
#ifdef TM_DBG
	do_gettimeofday(&tts);
	ts_dbg("tts:%dsec, %dusec\n", (int)tts.tv_sec, (int)tts.tv_usec);
#endif
	if ((GPIO_INT_REQ_STS_VAL & tmp) == 0)
		return IRQ_HANDLED;

	spin_lock_irq(&ts_drv->spinlock);
	ts_drv->soc_gpio_irq_disable(ts_drv);
	schedule_work(&ts_drv->work);
	spin_unlock_irq(&ts_drv->spinlock);

	//ts_dbg("IRQ is:%d\n", irq);
	return IRQ_HANDLED;
}
#endif

static void vt1603_ts_soc_gpio_handle_init(struct vt1603_ts_drvdata * ts_drv)
{
#ifdef VT1603_TS_EN
#ifdef WMT_VT3465
	ts_drv->soc_gpio_irq_init    = wm8650_gpio_irq_init;
	ts_drv->soc_gpio_irq_enable  = wm8650_gpio_irq_enable;
	ts_drv->soc_gpio_irq_disable = wm8650_gpio_irq_disable;
	ts_drv->soc_gpio_irq_clear   = wm8650_gpio_irq_clear;
#endif
#else
	ts_drv->soc_gpio_irq_init    = none_gpio_irq_init;
	ts_drv->soc_gpio_irq_enable  = none_gpio_irq_enable;
	ts_drv->soc_gpio_irq_disable = none_gpio_irq_disable;
	ts_drv->soc_gpio_irq_clear   = none_gpio_irq_clear;
#endif
}

#ifdef VT1603_TS_EN
static int vt1603_ts_input_dev_init(struct vt1603_ts_drvdata * ts_drv)
{
	ts_drv->input->name = "VT1603 Touch Screen";

	/* those three EVENTs are indispensable for single touchscreen
	   to reporte to input-system*/
	ts_drv->input->evbit[0] = BIT_MASK(EV_KEY) |
				  BIT_MASK(EV_ABS) |
				  BIT_MASK(EV_SYN);

	/* BTN_TOUCH: MUST report */
	ts_drv->input->keybit[BIT_WORD(BTN_TOUCH)] |= BIT_MASK(BTN_TOUCH);
	/* ABS_X, ABS_Y: MUST report, */
	ts_drv->input->absbit[0] = BIT(ABS_X) | BIT(ABS_Y);

	input_set_abs_params(ts_drv->input, ABS_X, 0, vt1603_ts_get_resolvX(), 0, 0);
	input_set_abs_params(ts_drv->input, ABS_Y, 0, vt1603_ts_get_resolvY(), 0, 0);
	input_register_device(ts_drv->input);
	return 0;
}
#endif

/*
 * vt1603_ts_calibration - vt1603 self calibration routine
 * @ts_drv: vt1603 driver data
 */
static void vt1603_ts_calibration(struct vt1603_ts_drvdata *ts_drv)
{
	unsigned char i, j, tmp;
	unsigned char cal[5][8] = {{0}};
	unsigned int cal_sum[8] = {0};
	struct vt1603_ts_platform_data *ts_pdata;

	ts_dbg("Enter\n");
	ts_pdata = ts_drv->i2c->dev.platform_data;    
	for (j = 0; j < 5; j++) {
		tmp = BIT6 | BIT0 | (ts_pdata->cal_sel << 4);
		vt1603_set_reg8(ts_drv, VT1603_CCCR_REG, tmp);
		msleep(100);
		for (i = 0; i < 8; i++)
			cal[j][i] = vt1603_get_reg8(ts_drv, VT1603_ERR8_REG + i);
	}
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 5; j++)
			cal_sum[i] += cal[j][i];
		tmp = (u8)cal_sum[i]/5;
		vt1603_set_reg8(ts_drv, VT1603_DBG8_REG + i, tmp);
	}

	ts_dbg("Exit\n");
	return ;
}

/*
 * vt1603_ts_reset - reset vt1603, auto postition conversion mode,
 *     do self calibration if enable
 * @ts_drv: vt1603 driver data
 */
static void vt1603_ts_reset(struct vt1603_ts_drvdata * ts_drv)
{    
	struct vt1603_ts_platform_data *ts_pdata;
	ts_pdata = ts_drv->i2c->dev.platform_data;

	/* power control enable */
	vt1603_set_reg8(ts_drv, VT1603_PWC_REG, 0x18);
	/* begin calibrate if calibration enable */
	if ((ts_pdata != NULL) && (ts_pdata->cal_en == CALIBRATION_ENABLE)) {
	    vt1603_ts_calibration(ts_drv);
	}

#ifdef VT1603_TS_EN
	/* auto position conversion mode and panel type config */
	if (ts_drv->ts_type == PANEL_TYPE_4WIRED)
		vt1603_set_reg8(ts_drv, VT1603_CR_REG, BIT1);
	else
		vt1603_set_reg8(ts_drv, VT1603_CR_REG, BIT1 | BIT0);
#endif
	/* interrupt control, pen up/down detection enable */
	vt1603_set_reg8(ts_drv, VT1603_INTCR_REG, 0xff);

	/* interrupt enable register */
	vt1603_setbits(ts_drv, VT1603_INTEN_REG, BIT3 | BIT2 | BIT1 | BIT0);

	/* clock divider */
	vt1603_set_reg8(ts_drv, VT1603_CDPR_REG, ts_drv->sclk_div);    

	/* clear irq firest */
	vt1603_setbits(ts_drv, VT1603_INTS_REG, BIT3 | BIT2 | BIT1 | BIT0);
}

static struct vt1603_ts_device vt1603_ts_dev;
static int __devinit 
vt1603_ts_i2c_probe(struct i2c_client *i2c, const struct i2c_device_id *id)
{
	int ret = 0;
	struct vt1603_ts_drvdata *ts_drv = NULL;
	struct vt1603_ts_platform_data *ts_pdata = NULL;

	/* revisit vt1603 i2c client, fix i2c address */
	//i2c->addr = VT1603_I2C_ADDR;

	ts_pdata = i2c->dev.platform_data;
	if (NULL == ts_pdata) {
		ts_dbg("get vt1603 ts platform data\n");
		ret = -ENODEV;
		goto out;
	}

	/* touch panel controller driver data allocation */
	ts_drv = kmalloc(sizeof(*ts_drv), GFP_KERNEL);
	if (!ts_drv) {
		ts_dbg("vt1603_ts: alloc driver data failed\n");
		ret = -ENOMEM;
		goto out;
	}

	/* touch panel controller driver data initialization */
	memset(ts_drv, 0x00, sizeof(*ts_drv));
	ts_drv->i2c        = i2c;
	ts_drv->pdata      = ts_pdata;
	ts_drv->ts_type    = ts_pdata->panel_type;
	ts_drv->gpio_irq   = ts_pdata->soc_gpio_irq;
	ts_drv->sclk_div   = ts_pdata->sclk_div;
	ts_drv->mode       = VT1603_TS_MODE;
	ts_drv->ts_state   = TS_PENUP_STATE;

	spin_lock_init(&ts_drv->spinlock);
	vt1603_ts_dev.ts_drv = ts_drv;
	dev_set_drvdata(&i2c->dev, ts_drv);
	/* soc-side gpio handle function init */
	vt1603_ts_soc_gpio_handle_init(ts_drv);

#ifdef VT1603_TS_EN
	/* GPIO IRQ request */
	if (ts_drv->gpio_irq <= 0) {
		ts_dbg("vt1603_ts: IRQ number error\n");
		ret = -ENODEV;
		goto release_driver_data;
	}

	INIT_WORK(&ts_drv->work, vt1603_ts_work);
	/* we disable gpio irq to avoid interrupt in vt1603 initialization */
	ts_drv->soc_gpio_irq_disable(ts_drv);
	if (request_irq(ts_drv->gpio_irq, vt1603_ts_isr, IRQF_SHARED,
			"vt1603_ts", ts_drv)) {
		ts_dbg("vt1603_ts: request IRQ %d failed\n",
			ts_drv->gpio_irq);
		ret = -ENODEV;
		goto release_driver_data;
	}

	/* iuput device routine */
	ts_drv->input = input_allocate_device();
	if (!ts_drv->input) {
		ts_dbg("vt1603_ts: alloc input device failed");
		ret = -ENOMEM;
		goto release_gpio_irq;
	}
	vt1603_ts_input_dev_init(ts_drv);

	/* vt1603 penup detecting timer init */
	vt1603_ts_init_penup_tmr(ts_drv);
#endif

    /* initial battery if battery detection enable  */
#ifdef VT1603_BAT_EN
	ts_drv->bat_info = &vt1603_bat;
	vt1603_bat_info_init(ts_drv);
#endif
    /* initial temperature if temperature detection enable */
#ifdef VT1603_TEMP_EN
	ts_drv->temp_info = &vt1603_temp;
	vt1603_temp_info_init(ts_drv);
#endif

	/* hardware prepare here */
	/* 1. mclk enable        */
	vt1603_ts_clk_enable();
	/* 2.vt1603 touch-panel and sar-adc module reset */
	vt1603_ts_reset(ts_drv);
	/* 3. vt1603 gpio1 reset, as irq output */
	vt1603_gpio1_reset(ts_drv);
	/* 4. dump vt1603 to ensure setting ok  */
	vt1603_reg_dump(ts_drv);

	/* enable soc gpio irq */
	ts_drv->soc_gpio_irq_init(ts_drv);
	printk(KERN_INFO "VT1603 Touch Panel Driver OK!\n");
	ts_drv->soc_gpio_irq_enable(ts_drv);
	goto out;

#ifdef VT1603_TS_EN
release_gpio_irq:
	free_irq(ts_drv->gpio_irq, ts_drv);
release_driver_data:
	kfree(ts_drv);
	ts_drv = NULL;
#endif
out:
	return ret;
}

static __devexit int vt1603_ts_i2c_remove(struct i2c_client *i2c)
{
	struct vt1603_ts_drvdata *ts_drv;
	ts_drv = dev_get_drvdata(&i2c->dev);

	ts_dbg("Enter\n");

	/* disable gpio3 irq first         */
	ts_drv->soc_gpio_irq_disable(ts_drv);
	ts_drv->soc_gpio_irq_clear(ts_drv);

#ifdef VT1603_BAT_EN
	del_timer_sync(&ts_drv->bat_info->bat_tmr);
	ts_drv->bat_info->bat_val = 0;
	ts_drv->bat_info->detect_time = 0;
#endif

#ifdef VT1603_TEMP_EN
	del_timer_sync(&ts_drv->temp_info->temp_tmr);
	ts_drv->temp_info->temp_val = 0;
	ts_drv->temp_info->detect_time = 0;
#endif

#ifdef VT1603_TS_EN
	/* pen up timer close               */
	vt1603_ts_close_penup_tmr(ts_drv);
	/* gpio irq free first              */
	free_irq(ts_drv->gpio_irq, ts_drv);
	/* input unregister                 */
	input_unregister_device(ts_drv->input);
#endif

	/* free vt1603 driver data          */
	dev_set_drvdata(&i2c->dev, NULL);
	kfree(ts_drv);   
	ts_drv = NULL;

	ts_dbg("Exit\n");
	return 0;
}

#ifdef CONFIG_PM
#if 0
static void vt1603_ts_disable(struct vt1603_ts_drvdata *ts_drv)
{
	spin_lock_irq(&ts_drv->spinlock);

	/* clear interrupts              */
	vt1603_clr_ts_irq(ts_drv, BIT0 | BIT1 | BIT2 | BIT3);
	/* disable interrupt detecting   */
	vt1603_clrbits(ts_drv, VT1603_INTEN_REG, BIT0 | BIT1 | BIT2 | BIT3);
	/* disable pen down/up detecting */
	vt1603_clrbits(ts_drv, VT1603_INTCR_REG, BIT7);
	/* calibration disable           */
	vt1603_clrbits(ts_drv, VT1603_CCCR_REG, BIT0);
	/* change to manually mode       */
	vt1603_set_reg8(ts_drv, VT1603_CR_REG, 0x00);
	/* power and clock down          */
	vt1603_set_reg8(ts_drv, VT1603_PWC_REG, 0x21);
	ts_drv->ts_state = TS_PENUP_STATE;

	spin_unlock_irq(&ts_drv->spinlock);
}
#endif

/*
 * TODO: when vt1603 ts suspend, battery and temperature aslo
 *       suspend now, that means the bat_val and temp_val will
 *       not be updated until vt1603 ts resumed    
 */
static int vt1603_ts_i2c_suspend(struct i2c_client *i2c, pm_message_t message)
{
	struct vt1603_ts_drvdata *ts_drv;

	ts_dbg("Enter\n");
	ts_drv = dev_get_drvdata(&i2c->dev);
	/* disable and clear gpio irq first      */
	ts_drv->soc_gpio_irq_disable(ts_drv);
	ts_drv->soc_gpio_irq_clear(ts_drv);
#ifdef VT1603_TS_EN
	/* disable penup monitor timer           */
	vt1603_ts_close_penup_tmr(ts_drv);
#endif
	/* suspend battery and temperature timer */
#ifdef VT1603_BAT_EN
	del_timer_sync(&ts_drv->bat_info->bat_tmr);
#endif
#ifdef VT1603_TEMP_EN
	del_timer_sync(&ts_drv->temp_info->temp_tmr);
#endif
	/* vt1603 ts hareware shutdown           */
	/*vt1603_ts_disable(ts_drv);*/
	ts_drv->ts_state   = TS_PENUP_STATE;

	/* enable soc gpio irq */
	ts_drv->soc_gpio_irq_enable(ts_drv);

	ts_dbg("Exit\n");
	return 0;
}

static int
vt1603_ts_i2c_resume(struct i2c_client *i2c)
{
	struct vt1603_ts_drvdata *ts_drv = dev_get_drvdata(&i2c->dev);

	ts_dbg("Enter\n");

	ts_drv->ts_state = TS_PENUP_STATE;
	ts_drv->mode = VT1603_TS_MODE;

	ts_drv->soc_gpio_irq_disable(ts_drv);
	/* must ensure mclk is available */
	vt1603_ts_clk_enable();
	/* vt1603 ts hardware resume     */
	vt1603_ts_reset(ts_drv);
	/* vt1603 gpio1 reset            */
	vt1603_gpio1_reset(ts_drv);

	/* enable gpio irq last          */
	/* FIXME: it's no need init gpio irq? */
	ts_drv->soc_gpio_irq_init(ts_drv);

    /* detecte bat/temp after 1sec   */
#ifdef VT1603_BAT_EN
	mod_timer(&ts_drv->bat_info->bat_tmr, jiffies + msecs_to_jiffies(5000));
#endif

#ifdef VT1603_TEMP_EN
	mod_timer(&ts_drv->temp_info->temp_tmr, 
			jiffies + msecs_to_jiffies(6000 + 30));
#endif
	/* clear irq before enale gpio irq */
	vt1603_clr_ts_irq(ts_drv, BIT0 | BIT1 | BIT2 | BIT3 );
	ts_drv->soc_gpio_irq_clear(ts_drv);
	ts_drv->soc_gpio_irq_enable(ts_drv);

	ts_dbg("Exit\n");
	return 0;
}

#else
#define vt1603_ts_i2c_suspend NULL
#define vt1603_ts_i2c_resume  NULL
#endif

static const struct i2c_device_id vt1603_ts_i2c_id[] = {
	{"vt1603_ts", 0 },
	{ }
};

static struct i2c_driver vt1603_ts_i2c_driver = {
	.driver    = {
		.name  = "vt1603_ts",
		.owner = THIS_MODULE,
	 },
	.probe     = vt1603_ts_i2c_probe,
	.remove    = vt1603_ts_i2c_remove,
	.suspend   = vt1603_ts_i2c_suspend,
	.resume    = vt1603_ts_i2c_resume,
	.id_table  = vt1603_ts_i2c_id,
};

static struct vt1603_ts_platform_data vt1603_ts_pdata = {
	.panel_type   = PANEL_TYPE_4WIRED,
	.cal_en       = CALIBRATION_DISABLE,
	.cal_sel      = 0x00,
	.shift        = 0x00,
	.sclk_div     = 0x08,
	.soc_gpio_irq = IRQ_GPIO,
	//.irq_type     = RISING_EDGE_ACTIVE,
	.irq_type     = HIGH_ACTIVE,
};

static struct i2c_board_info vt1603_ts_i2c_board_info = {
	.type          = "vt1603_ts",
	.flags         = 0x00,
	.addr          = VT1603_I2C_FAKE_ADDR,
	//.addr          = 0x1A,
	.platform_data = &vt1603_ts_pdata,
	.archdata      = NULL,
	.irq           = -1,
};

static int vt1603_ts_dev_open(struct inode *inode, struct file *filp)
{
	struct vt1603_ts_device *ts_dev;

	ts_dbg("Enter\n");

	ts_dev = container_of(inode->i_cdev, struct vt1603_ts_device, cdev);
	if (ts_dev->ts_drv == NULL) {
		ts_dbg("can not get vt1603_ts driver data\n");
		return -ENODATA;
	}
	filp->private_data = ts_dev;

	ts_dbg("Exit\n");
	return 0;
}

static int vt1603_ts_dev_close(struct inode *inode, struct file *filp)
{
	struct vt1603_ts_device *ts_dev;

	ts_dbg("Enter\n");

	ts_dev = container_of(inode->i_cdev, struct vt1603_ts_device, cdev);
	if (ts_dev->ts_drv == NULL) {
		ts_dbg("can not get vt1603_ts driver data\n");
		return -ENODATA;
	}

	ts_dbg("Exit\n");
	return 0;
}

static int vt1603_ts_dev_ioctl(struct inode *inode, struct file *filp,
                               unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	int err = 0;
#ifdef VT1603_TS_EN
	int nBuff[7] = { 0 };
	char env_val[96] = { 0 };
#endif
	struct vt1603_ts_device *ts_dev;
	struct vt1603_ts_drvdata *ts_drv;
	struct vt1603_reg_ioc ts_ioc;

	ts_dbg("Enter\n");
	/* check type and command number */
	if (_IOC_TYPE(cmd) != VT1603_TS_IOC_MAGIC)
		return -ENOTTY;

	    /* Check access direction once here; don't repeat below.
	     * IOC_DIR is from the user perspective, while access_ok is
	     * from the kernel perspective; so they look reversed.
	     */
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
	if (err == 0 && _IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
	if (err)
		return -EFAULT;

	ts_dev = filp->private_data;
	ts_drv = ts_dev->ts_drv;

	switch (cmd) {
#ifdef VT1603_BAT_EN
	/* get bat conversion value      */
	case VT1603_IOC_GET_BAT_VAL:
		ret = put_user(ts_drv->bat_info->bat_val, (u16 __user *)arg);
		break;
	/* get/set bat detect interval   */
	case VT1603_IOC_GET_BAT_INTERVAL:
		ret = put_user(ts_drv->bat_info->interval, (u16 __user *)arg);
		break;
	case VT1603_IOC_SET_BAT_INTERVAL:
		ret = get_user(ts_drv->bat_info->interval, (u16 __user *)arg);
		mod_timer(&ts_drv->bat_info->bat_tmr, 
			jiffies + msecs_to_jiffies(ts_drv->bat_info->interval*1000 
			+ random32()%1000));
		break;
#endif
#ifdef VT1603_TEMP_EN
	/* get temp conversion value     */
	case VT1603_IOC_GET_TEMP_VAL:
		ret = put_user(ts_drv->temp_info->temp_val, (u16 __user *)arg);
		break;

	/* get/set temp detect interval  */
	case VT1603_IOC_GET_TEMP_INTERVAL:
		ret = put_user(ts_drv->temp_info->interval, (u16 __user *)arg);
		break;
	case VT1603_IOC_SET_TEMP_INTERVAL:
		ret = get_user(ts_drv->temp_info->interval, (u16 __user *)arg);
		mod_timer(&ts_drv->temp_info->temp_tmr, 
		    jiffies + msecs_to_jiffies(ts_drv->temp_info->interval*1000 
			+ random32()%1000));
		break;
#endif

	/* get/set register value        */
	case VT1603_IOC_RD_REG:
		ret = copy_from_user(&ts_ioc, (void __user *)arg, sizeof(ts_ioc));
		if (ret == 0) {
			ts_ioc.reg_val = vt1603_get_reg8(ts_drv, ts_ioc.reg_addr);
			ret = copy_to_user((void __user *)arg, &ts_ioc, sizeof(ts_ioc));
		}
		break;
	case VT1603_IOC_WR_REG:
		ret = copy_from_user(&ts_ioc, (void __user *)arg, sizeof(ts_ioc));
		if (ret == 0)
			vt1603_set_reg8(ts_drv, ts_ioc.reg_addr, ts_ioc.reg_val);
		break;

	/* get/set sclk dividor          */
	case VT1603_IOC_GET_CLK_DIV:
		ret = put_user(ts_drv->sclk_div, (u8 __user *)arg);
		break;
	case VT1603_IOC_SET_CLK_DIV:
		ret = get_user(ts_drv->sclk_div, (u8 __user *)arg);
		break;

#ifdef VT1603_TS_EN
	/* vt1603 touch calibration      */
	case VT1603_TS_IOC_CAL_START:
		cal_dbg("vt1603 ts ioctl cal start\n");
		g_bCalibrating = true;
		break;
	case VT1603_TS_IOC_CAL_DONE:
		cal_dbg("vt1603 ts ioctl cal done\n");
		copy_from_user(nBuff, (unsigned int *)arg, 7 * sizeof(int));
			g_CalcParam.a1 = nBuff[0];
			g_CalcParam.b1 = nBuff[1];
			g_CalcParam.c1 = nBuff[2];
			g_CalcParam.a2 = nBuff[3];
			g_CalcParam.b2 = nBuff[4];
			g_CalcParam.c2 = nBuff[5];
			g_CalcParam.delta = nBuff[6];
			if (g_CalcParam.delta == 0)
				g_CalcParam.delta = 1;
			sprintf(env_val, "%d %d %d %d %d %d %d",
				nBuff[0], nBuff[1], nBuff[2], nBuff[3], nBuff[4], nBuff[5], nBuff[6]);
			printk("%d %d %d %d %d %d %d",
				nBuff[0], nBuff[1], nBuff[2], nBuff[3], nBuff[4], nBuff[5], nBuff[6]);
			wmt_setsyspara("wmt.io.ts.2dcal", env_val);
			cal_dbg("TSC calibrate done data: [%s]\n", env_val);
			g_bCalibrating = false;
			break;
	case VT1603_TS_IOC_CAL_QUIT:
		cal_dbg("vt1603 ts ioctl cal quit\n");
		copy_from_user(nBuff, (unsigned int *)arg, 7 * sizeof(int));
		g_CalcParam.a1 = nBuff[0];
		g_CalcParam.b1 = nBuff[1];
		g_CalcParam.c1 = nBuff[2];
		g_CalcParam.a2 = nBuff[3];
		g_CalcParam.b2 = nBuff[4];
		g_CalcParam.c2 = nBuff[5];
		g_CalcParam.delta = nBuff[6];
		if (g_CalcParam.delta == 0)
			g_CalcParam.delta = 1;
		cal_dbg("cal_quit g_CalcParam = %d, %d, %d, %d, %d, %d, %d\n",
				g_CalcParam.a1, g_CalcParam.b1, g_CalcParam.c1,
				g_CalcParam.a2, g_CalcParam.b2, g_CalcParam.c2, g_CalcParam.delta);
		g_bCalibrating = false;
		break;
	case VT1603_TS_IOC_CAL_RAWDATA:
		cal_dbg("vt1603 ts ioctl cal raw data\n");
		if (!g_bCalibrating) {
			cal_dbg("g_bCalibrating is false\n");
			return -EINVAL;
		}
		nBuff[0] = g_evLast.x;
		nBuff[1] = g_evLast.y;
		copy_to_user((unsigned int *)arg, nBuff, 2 * sizeof(int));
		cal_dbg("raw data: x=%d, y=%d\n", nBuff[0], nBuff[1]);
		break;
#endif
	default:
		ret = -EINVAL;
		break;
	}

	ts_dbg("Exit\n");
	return ret;
}

static struct file_operations vt1603_ts_fops = {
	.owner   = THIS_MODULE,
	.open    = vt1603_ts_dev_open,
	.ioctl   = vt1603_ts_dev_ioctl,
	.release = vt1603_ts_dev_close,
};

static int vt1603_ts_dev_major = VT1603_TS_DEV_MAJOR;
static int vt1603_ts_dev_minor = 0;
module_param(vt1603_ts_dev_major, int, S_IRUGO);
module_param(vt1603_ts_dev_minor, int, S_IRUGO);

#define DEV_NAME  "wmtts"
static struct class *vt1603_ts_class;
static int vt1603_ts_dev_setup(void)
{
	dev_t dev_no = 0;
	int ret = 0;
	struct device *dev = NULL;

	ts_dbg("Enter\n");
	if (vt1603_ts_dev_major) {
		dev_no = MKDEV(vt1603_ts_dev_major, vt1603_ts_dev_minor);
		ret = register_chrdev_region(dev_no, VT1603_TS_NR_DEVS, DEV_NAME);
	} else {
		ret = alloc_chrdev_region(&dev_no, vt1603_ts_dev_minor, 
				    VT1603_TS_NR_DEVS, DEV_NAME);
		vt1603_ts_dev_major = MAJOR(dev_no);
		vt1603_ts_dev_minor = MINOR(dev_no);
		ts_dbg("vt1603_ts device major is %d\n", vt1603_ts_dev_major);
	}

	if (ret < 0) {
		ts_dbg("can not get major %d\n", vt1603_ts_dev_major);
		goto out;
	}

	cdev_init(&vt1603_ts_dev.cdev, &vt1603_ts_fops);
	vt1603_ts_dev.ts_drv     = NULL;
	vt1603_ts_dev.cdev.owner = THIS_MODULE;
	vt1603_ts_dev.cdev.ops   = &vt1603_ts_fops;
	ret = cdev_add(&vt1603_ts_dev.cdev, dev_no, VT1603_TS_NR_DEVS);
	if (ret) {
		ts_dbg("add char dev for vt1603 ts failed\n");
		goto release_region;
	}

	vt1603_ts_class = class_create(THIS_MODULE, "touch_panel");
	if (IS_ERR(vt1603_ts_class)) {
		ts_dbg("create vt1603_ts class failed\n");
		ret = PTR_ERR(vt1603_ts_class);
		goto release_cdev;
	}

	/* FIXME: parent should be i2c->dev */
	dev = device_create(vt1603_ts_class, NULL, dev_no, NULL, DEV_NAME);
	if (IS_ERR(dev)) {
		ts_dbg("create device for vt1603 ts failed\n");
		ret = PTR_ERR(dev);
		goto release_class;
	}

	ts_dbg("Exit\n");
	goto out;

release_class:
	class_destroy(vt1603_ts_class);
	vt1603_ts_class = NULL;
release_cdev:
	cdev_del(&vt1603_ts_dev.cdev);
release_region:
	unregister_chrdev_region(dev_no, VT1603_TS_NR_DEVS);
out:
	return ret;
}

static void vt1603_ts_dev_cleanup(void)
{
	dev_t dev_no = MKDEV(vt1603_ts_dev_major, vt1603_ts_dev_minor);

	ts_dbg("Enter\n");
	cdev_del(&vt1603_ts_dev.cdev);
	unregister_chrdev_region(dev_no, VT1603_TS_NR_DEVS);
	device_destroy(vt1603_ts_class, dev_no);
	class_destroy(vt1603_ts_class);
	ts_dbg("Exit\n");
}

#ifdef VT1603_TS_EN
static unsigned long
wmt_strtoul(const char *cp, char **endp, unsigned int base)
{
	unsigned long result = 0, value;

	if (*cp == '0') 
		cp++;
	if (!base) 
		base = 10;
    	while ((*cp) != ',') {
		if (*cp >= '0' && *cp <= '9')
			value = *cp - '0';
        	else if(*cp >= 'a'&& *cp <= 'f') 
			value = *cp - 'a' + 10;
        	else if(*cp>='A'&&*cp<='F') 
			value = *cp - 'A' + 10;
        	else 
			break;
        	if (value >= base) 
			break;

 		result = result * base + value;
 		cp++;
 	}

	if (endp)
		*endp = (char *)cp;

	return result;
}

static int
wmt_getenv2int(char *varname, unsigned char *varval, int varlen, int *pInt)
{
	int len = varlen;
  	wmt_getsyspara(varname, varval, &len);
    	if (!varval)
		return false;
    	*pInt = wmt_strtoul(varval, NULL, 0);
    	return true;
}

static int __init vt1603_ts_uboot_env_check(void)
{
	char *p;
	unsigned int i = 0;
	int len = 96;
	char retval[96];
	int nBuff[7];

	/* Get u-boot parameter */
	wmt_getsyspara("wmt.io.ts.dis", retval, &len);
	if (!strcmp(retval, "1")) {
		printk(KERN_ERR "Touch screen is disabled, wmt.io.ts.dis = 1\n");
		return -ENODEV;
	}

	wmt_getsyspara("touchic", retval, &len);
	if (!strcmp(retval, "false")) {
		printk(KERN_ERR "Touch IC not exist, please check!\n");
		return -ENODEV;
	}

	wmt_getsyspara("touchcodec", retval, &len);
	if (strcmp(retval, "vt1603")) {
		printk(KERN_ERR "Touch IC should be vt1603!\n");
		return -ENODEV; 
	}

	wmt_getenv2int("panelres.x", retval, 32, (int*)&panelres_x);
	wmt_getenv2int("panelres.y", retval, 32, (int*)&panelres_y);
	ts_dbg("panelres.x = %d, panelres.y = %d\n", panelres_x, panelres_y);

	memset(retval, 0, sizeof(retval));
	wmt_getsyspara("wmt.io.ts.2dcal", retval, &len);
	for (i = 0; i < sizeof(retval); i++) {
		if (retval[i] == ' ' || retval[i] == ',' || retval[i] == ':')
			retval[i] = '\0';
	}

	p = retval;
	for (i = 0; (i < 7) && (p < (retval + sizeof(retval))); ) {
		if (*p == '\0')
			p++;
		else {
			sscanf(p, "%d", &nBuff[i]);
			p = p + strlen(p);
			i++;
		}
	}
	ts_dbg("TSC calibrate init data: [%d %d %d %d %d %d %d]\n",
        	nBuff[0], nBuff[1], nBuff[2], nBuff[3], nBuff[4], nBuff[5], nBuff[6]);

	g_CalcParam.a1 = nBuff[0];
	g_CalcParam.b1 = nBuff[1];
	g_CalcParam.c1 = nBuff[2];
	g_CalcParam.a2 = nBuff[3];
	g_CalcParam.b2 = nBuff[4];
	g_CalcParam.c2 = nBuff[5];
	g_CalcParam.delta = nBuff[6];
	g_bCalibrating = false;

	if (g_CalcParam.delta == 0)
		g_CalcParam.delta = 1;

	return 0;
}
#endif

static int __init vt1603_ts_i2c_init(void)
{
	int ret = 0;
	struct i2c_adapter *adapter = NULL;
	struct i2c_client *client   = NULL;
	struct i2c_board_info *vt1603_i2c_bi;

	ts_dbg("Enter\n");

#ifdef VT1603_TS_EN
	ret = vt1603_ts_uboot_env_check();
	if (ret) {
		ts_dbg("vt1603_ts uboot env check failed\n");
		goto out;
	}
#endif

	ret = vt1603_ts_dev_setup();
	if (ret) {
		ts_dbg("vt1603_ts create device node failed\n");
		goto out;
	}

	vt1603_i2c_bi = &vt1603_ts_i2c_board_info;
	adapter = i2c_get_adapter(VT1603_I2C_BUS_ID);
	if (NULL == adapter) {
		ts_dbg("can not get i2c adapter, client address error\n");
		ret = -ENODEV;
		goto release_dev;
	}

	client = i2c_new_device(adapter, vt1603_i2c_bi);
	if (client == NULL) {
		ts_dbg("allocate i2c client failed\n");
		ret = -ENOMEM;
		goto release_dev;
	}
	i2c_put_adapter(adapter);

	/* register i2c driver now */
	ret = i2c_add_driver(&vt1603_ts_i2c_driver);
	if (ret) {
		ts_dbg("register vt1603 ts i2c driver failed\n");
		goto release_client;
	}

	ts_dbg("Exit\n");
	goto out;

release_client:
	i2c_unregister_device(client);
	client = NULL;
release_dev:
	vt1603_ts_dev_cleanup();
out:
	return ret;
}
module_init(vt1603_ts_i2c_init);

static void __exit vt1603_ts_i2c_exit(void)
{
	ts_dbg("Enter\n");

	i2c_unregister_device(vt1603_ts_dev.ts_drv->i2c);
	i2c_del_driver(&vt1603_ts_i2c_driver);
	vt1603_ts_dev_cleanup();

	ts_dbg("Exit\n");
}
module_exit(vt1603_ts_i2c_exit);

MODULE_AUTHOR("WonderMedia Technologies, Inc");
MODULE_DESCRIPTION("VT1603A Touch-Panel Controller and SAR-ADC Driver");
MODULE_LICENSE("GPL");
