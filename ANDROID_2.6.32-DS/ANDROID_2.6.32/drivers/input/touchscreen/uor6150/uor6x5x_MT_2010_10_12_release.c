#include <linux/errno.h>
//#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/serio.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/timer.h>
#include <linux/workqueue.h>
#include <linux/poll.h>
#include <linux/spinlock.h>
#include <linux/hrtimer.h>

#include <asm/io.h>
#include <asm/irq.h>
#include <asm/gpio.h>
#include <mach/hardware.h>

#include "wmt_ts.h"

//#define DEBUG_OUR_UTK
#undef dbg

#ifdef DEBUG_OUR_UTK
	#define dbg(format, arg...) printk("[%d]:%s:" format,__LINE__,__FUNCTION__, ## arg)
#else
	#define dbg(format, arg...)
#endif

#undef abs
#define abs(x) (((x)>0)?(x):(-(x)))

#define TRUE				1
#define FALSE 			0

#define UORxL_AddW  	0x90          //UOR Address for Writing
#define UORxL_AddR 		0x91          //UOR Address for Reading

#define InitX2              	0x00
#define InitY2              	0x10
#define CalibX              	0x20
#define CalibY              	0x30
#define MSRX_2T          	0x40
#define MSRY_2T          	0x50
#define MSRX_1T          	0xC0
#define MSRY_1T          	0xD0
#define MSRZ1_1T        	0xE0
#define MSRZ2_1T        	0xF0

#define ReadDx			0x40
#define ReadDy			0x50
#define InitX				0x00
#define InitY				0x10
#define CalibX			0x20
#define CalibY			0x30
#define ReadX			0xC0
#define ReadY			0xD0
#define ReadZ1			0xE0
#define ReadZ2			0xF0

#define Tap				1	//Tap
#define RHorizontal		3	//Right horizontal
#define LHorizontal		4	//Left horizontal
#define UVertical			5	//Up vertical
#define DVertical			6	//Down vertical
#define RArc				7	//Right arc
#define LArc				8	//Left arc
#define CWCircle			9	//Clockwise circle
#define CCWCircle		10	//Counter-clockwise circle
#define RPan				11	//Right pan
#define LPan				12	//Left pan
#define DPan				13	//Right pan
#define UPan				14	//Left pan
#define PressTap			15	//Press and tap
#define PinchIn			16	//Pinch in
#define PinchOut			17	//Pinch out



#define R_xplate			1024
#define R_Threshold 		5000//5000//单指压力参数
#define R_Threshold2 		1000//1300//1000//400//双指压力参数

#define DeltaX 	2//30
#define DeltaY 	2//30
#define XMax 	3500
#define XMin		300
#define YMax 	3000
#define YMin		300

#define ZERO_TOUCH	0	
#define ONE_TOUCH	1
#define TWO_TOUCH	2

#define DX_T				60//53 //56 //64
#define DY_T				60//53 //56 //64
#define DX_MSK			0x00ff
#define DY_MSK			0x00ff
#define DXY_NUM			3
#define DXY_SKIP			0x80//咀奔铬臘禬筁禯瞒DXY_SKIPㄢ翴

#define NumberFilter		7
#define NumberDrop		4	//This value must not bigger than (NumberFilter-1)

#define FIRSTTOUCHCOUNT			3//过滤前面如干个单点
#define ONETOUCHCountAfter2			20//20//过滤2 指之后一指时点数
#define JITTER_THRESHOLD			1200//单点前后2点之间的x/y 变化最大容许值
#define	MAX_READ_PERIOD			10//每次读点的休眠时间
#define FIRST_TWO_TOUCH_FILTER		3//过滤2 手指时前N 个点
#define JITTER_THRESHOLD_DXDY		32//2手指前后2点之间的x/y 变化最大容许值
#define PERIOD_PER_FILTER			4 //5//

#define NFilt NumberFilter
#define NDrop NumberDrop
#define UOR_INIT_PERIOD				30

enum ts_stat{
	GESVALUE_HAVE,
	GESVALUE_NO,
	GESVALUE_CLOSE,
};

typedef signed char VINT8;
typedef unsigned char VUINT8;
typedef signed short VINT16;
typedef unsigned short VUINT16;
typedef unsigned long VUINT32;
typedef signed long VINT32;

struct uor_touch_screen_struct {
	int xp;
	int yp;
	int pX;
	int pY;
	int pDX;
	int pDY;
	int count;
	int shift;
	int irq;
	int gpio;
	int touch_cnt;

	wait_queue_head_t wq;
	struct input_dev *dev;
};

struct wmt_gpt_s {
	char name[10];
	unsigned int bitmap;
	unsigned int ctraddr;
	unsigned int ocaddr;
	unsigned int idaddr;
	unsigned int peaddr;
	unsigned int pcaddr;
	unsigned int itbmp;
	unsigned int itaddr;
	unsigned int isbmp;
	unsigned int isaddr;
	unsigned int irq;
};

static struct wmt_gpt_s wmt_ts_gpt = {
	.name = "uor6150",
	.bitmap = 0x01,/*GPIO 0*/
	.ctraddr = 0xd8110040,
	.ocaddr = 0xd8110080,
	.idaddr = 0xd8110000,
	.peaddr = 0xd8110480,
	.pcaddr = 0xd81104c0,
	.itbmp = 0x00000080, /* low level */
	.itaddr = 0xd8110300,
	.isbmp = 0x1,
	.isaddr = 0xd8110320,
	.irq = 6,
};

static struct work_struct work;
static struct mutex  mutex_i2c_lock ;
static struct uor_touch_screen_struct ts;
static struct workqueue_struct *queue = NULL;
static struct delayed_work delay_penup_wk;


static int FirstTC = 0,OneTCountAfter2 = 0,TWOTouchFlag = 0;
static int two_touch_count = 0, pre_dx = 0, pre_dy = 0;
extern int wmt_getsyspara(char *varname, unsigned char *varval, int *varlen);
extern unsigned int wmt_read_oscr(void); 
#define TOUCH1_AVG
#define TOUCH2_AVG
#define WMT_FILTER_NUM 7

struct ST_UOR_BUF {
	unsigned short pXbuf[WMT_FILTER_NUM];
	unsigned short pYbuf[WMT_FILTER_NUM];
	unsigned short pDXbuf[WMT_FILTER_NUM];
	unsigned short pDYbuf[WMT_FILTER_NUM];
};
#define WMT_FIFO_LEN 7
struct ST_UOR_XY_FIFO{
	unsigned short pXbuf[WMT_FIFO_LEN];
	unsigned short pYbuf[WMT_FIFO_LEN];
	int head;
	int full;
};

static int gSampleNum = 3;
static struct ST_UOR_XY_FIFO xy_buf;
static struct ST_UOR_XY_FIFO xy_buf1;
static struct ST_UOR_XY_FIFO xy_buf2;


///////////////////////////////////////////////
//        GPIO SETUP REF      
///////////////////////////////////////////////
#define SET_GPIO_TS_INT() {\
	REG32_VAL(wmt_ts_gpt.ctraddr) |= wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.ocaddr) &= ~wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.peaddr) &= ~wmt_ts_gpt.bitmap; \
	REG32_VAL(wmt_ts_gpt.isaddr) |= wmt_ts_gpt.isbmp; \
}

static int setup_int_uor_gpio(unsigned int num) 
{
        if (wmt_ts_gpt.itbmp & 0xff)
                REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff;
        else if (wmt_ts_gpt.itbmp & 0xff00)
                REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff00;
        else if (wmt_ts_gpt.itbmp & 0xff0000)
                REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff0000;
        else if (wmt_ts_gpt.itbmp & 0xff000000)
                REG32_VAL(wmt_ts_gpt.itaddr) &= ~0xff000000;

	SET_GPIO_TS_INT();
	return 0;
}

#if 0
static int setup_int_gpio_out(unsigned int num, unsigned int val)
{
	if(num > 3)
		return -1;
	
	REG32_VAL(0xd8110040) |= (1<<(num)); //enable 
	REG32_VAL(0xd8110080) |= (1<<(num)); //set output
	if(val)
		REG32_VAL(0xd81100c0) |= (1<<(num)); //set ouput data
	else
		REG32_VAL(0xd81100c0) &= ~(1<<(num)); //set ouput data

	return 0;
}
#endif

static int check_int_status_gpio(unsigned int num)
{
	return (REG32_VAL(wmt_ts_gpt.isaddr) & (wmt_ts_gpt.isbmp));
}

static int clean_int_status_gpio(unsigned int num)
{
	REG32_VAL(wmt_ts_gpt.isaddr) = wmt_ts_gpt.isbmp;
	
	return 0;
}

static int enable_int_gpio(unsigned int num)
{
	if (wmt_ts_gpt.itbmp & 0xff)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x80;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x8000;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x800000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		REG32_VAL(wmt_ts_gpt.itaddr) |= 0x80000000;

	return 0;
}

static int disable_int_gpio(unsigned int num)
{
	if (wmt_ts_gpt.itbmp & 0xff)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x80;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x8000;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x800000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		REG32_VAL(wmt_ts_gpt.itaddr) &= ~0x80000000;

	return 0;
}

static int check_int_enable_gpio(unsigned int num)
{
	if (wmt_ts_gpt.itbmp & 0xff)
		return REG32_VAL(wmt_ts_gpt.itaddr) & 0x80;
	else if (wmt_ts_gpt.itbmp & 0xff00)
		return REG32_VAL(wmt_ts_gpt.itaddr) & 0x8000;
	else if (wmt_ts_gpt.itbmp & 0xff0000)
		return REG32_VAL(wmt_ts_gpt.itaddr) & 0x800000;
	else if (wmt_ts_gpt.itbmp & 0xff000000)
		return REG32_VAL(wmt_ts_gpt.itaddr) & 0x80000000;
	return 0;
}

static int check_pen_isup(unsigned int gpio)
{
	return (REG32_VAL(wmt_ts_gpt.idaddr) & (1<<(gpio))) ;
}
///////////////////////////////////////////////

extern int wmt_i2c_xfer_continue_if_4(struct i2c_msg *msg, unsigned int num,int bus_id);

static int uor_i2c_read(u8 command,char *pdata,int len)
{
	int ret = 0;
	struct i2c_msg msg[2];
	unsigned char buf[2];

	buf[0] = command;
	buf[1] = 0x0;

	msg[0].addr = UORxL_AddW>>1;
	msg[0].flags = 0 ;
	msg[0].flags &= ~(I2C_M_RD);
	msg[0].len = 1;
	msg[0].buf = buf;

	msg[1].addr = UORxL_AddW>>1;
	msg[1].flags = 0 ;
	msg[1].flags |= (I2C_M_RD);
	msg[1].len = len;
	msg[1].buf = pdata;

	ret = wmt_i2c_xfer_continue_if_4(msg, 2,1);

	
    	return ret;
} /* End of sensor_i2c_read */

static int uor_i2c_write(u8 command)    
{
	int ret = 0;
	struct i2c_msg msg[2];
	unsigned char buf[2];

	buf[0] = command;
	buf[1] = 0x0;

	msg[0].addr = UORxL_AddW>>1;
	msg[0].flags = 0 ;
	msg[0].flags &= ~(I2C_M_RD);
	msg[0].len = 1;
	msg[0].buf = buf;

	ret = wmt_i2c_xfer_continue_if_4(msg, 1,1);
	
	return ret;
		
} 

/*
Read bytes from UOR via I2C
*/
static VINT8 UOR_IICRead(VUINT8 Command,VUINT8 *readdata,VINT8 nread)
{
	mutex_lock(&mutex_i2c_lock);
	uor_i2c_write(Command);
	if (uor_i2c_read(Command, readdata, nread) < 0) {
		printk("ERR: %s() failed![0x%02x,0x%02x]\n",__FUNCTION__,readdata[0],readdata[1]);
		mutex_unlock(&mutex_i2c_lock);
		return 0;	
	} else {
		/*
		printk("%s() OK [0x%02x,0x%02x]\n",__FUNCTION__,readdata[0],readdata[1]);
		*/
		mutex_unlock(&mutex_i2c_lock);
		return 1;
	}
}

static int  XYFilter(int *xFilter, int *yFilter, int Num,int Drop)
{
	unsigned int i, SumTempX = 0, SumTempY = 0;
	int Dp, checkSmx, checkSmy, checkBgx, checkBgy;
	int SmX = 0, SmY = 0;
	int LaX = 0, LaY = 0;
	int SmInX = 0, SmInY = 0;
	int LaInX = 0, LaInY =0;

	if ((Num <= 2) && (Drop > (Num-1)))
		return FALSE; /*ot enough to sample*/
		
	for (i = 0; i < Num; i++) {
		SumTempX += xFilter[i];
		SumTempY += yFilter[i];
	}
	
	Dp = Drop;

	checkSmx = 0;
	checkSmy = 0;
	checkBgx = 0;
	checkBgy = 0;
	while (Dp > 0) {
		SmX = 0x0FFF; SmY = 0x0FFF;
		LaX = 0x0; LaY = 0x0;
		SmInX = 0; SmInY = 0;
		LaInX = 0; LaInY = 0;
		for (i =  0; i < Num; i++) {
			if (checkSmx & (1<<i)) {
			} else if (SmX > xFilter[i]) {
				SmX = xFilter[i];
				SmInX = i;
			}
			if (checkSmy & (1<<i)) {
			} else if (SmY > yFilter[i]) {
				SmY = yFilter[i];
				SmInY = i;
			}
			
			if (checkBgx & (1<<i)) {
			} else if (LaX < xFilter[i]) {
				LaX = xFilter[i];
				LaInX = i;
			}
			
			if (checkBgy & (1<<i)) {
			} else if (LaY < yFilter[i]) {
				LaY = yFilter[i];
				LaInY = i;
			}
		}
		if (Dp) {
			SumTempX -= xFilter[SmInX];
			SumTempX -= xFilter[LaInX];
			SumTempY -= yFilter[SmInY];
			SumTempY -= yFilter[LaInY];
			Dp -= 2;
			/*
			printk(KERN_ERR "in filter :SmInX %d,LaInX %d, SmInY %d , LaInY %d!!!\n", SmInX,LaInX, SmInY, LaInY);
			*/
		}
		checkSmx |= 1<<SmInX;
		checkSmy |= 1<<SmInY;
		checkBgx |= 1<<LaInX;
		checkBgy |= 1<<LaInY;
	}
	
	xFilter[0] = SumTempX/(Num-Drop);
	yFilter[0] = SumTempY/(Num-Drop);
	
	return TRUE;
}


static VINT8 Init_UOR_HW(void)
{
	VUINT8   i, icdata[2];
	VUINT32   Dx_REF, Dy_REF, Dx_Check, Dy_Check;
	int		  TempDx[NumberFilter],TempDy[NumberFilter];

	UOR_IICRead(CalibX,icdata,2);
	UOR_IICRead(CalibY,icdata,2);

	for (i = 0; i< NumberFilter; i++) {
		UOR_IICRead(InitX,icdata,2);
		TempDx[i] = (icdata[0]<<4 | icdata[1]>>4);
		UOR_IICRead(InitY,icdata,2);
		TempDy[i] = (icdata[0]<<4 | icdata[1]>>4);
		/*
		printk(KERN_ERR "filter test:#%d (x,y)=(%d,%d) !!!\n", i,TempDx[i], TempDy[i]);
		*/
	}
	XYFilter(TempDx,TempDy,NumberFilter,2);
	Dx_REF = TempDx[0];
	Dy_REF = TempDy[0];
	/*
	printk(KERN_ERR "filter result:(x,y)=(%d,%d) !!!\n", Dx_REF, Dy_REF);
	*/
	
	i = 0;
	do {
		UOR_IICRead(InitX,icdata,2);
		Dx_Check = abs((icdata[0]<<4 | icdata[1]>>4) - Dx_REF);
		UOR_IICRead(InitY,icdata,2);
		Dy_Check = abs((icdata[0]<<4 | icdata[1]>>4) - Dy_REF);

		i++;

		if (i > NumberFilter)
			return -1;

	} while (Dx_Check > 4 || Dy_Check > 4);

	return 0;
}

static int uor_fifo_add(struct ST_UOR_XY_FIFO *Fifo, int DataX,int DataY)
{
	Fifo->pXbuf[Fifo->head] = DataX;
	Fifo->pYbuf[Fifo->head] = DataY;
	
	++Fifo->head;
	if ((Fifo->head) >= WMT_FIFO_LEN) {
		Fifo->head = 0;
		Fifo->full = 1;
	}
	
	return 0;
}

static int uor_fifo_avg(struct ST_UOR_XY_FIFO Fifo, int *avgX, int *avgY, int Num, int Drop)
{
	unsigned int i, SumTempX = 0, SumTempY = 0;
	int Dp, checkSmx, checkSmy, checkBgx, checkBgy;
	int SmX = 0, SmY = 0;
	int LaX = 0, LaY = 0;
	int SmInX = 0, SmInY = 0;
	int LaInX = 0, LaInY =0;

	if ((Num <= 2) && (Drop > (Num-1)))
		return FALSE; /*not enough to sample*/

	if (!Fifo.full) {
		for (i = 0; i<Fifo.head; i++) {
			SumTempX += Fifo.pXbuf[i];
			SumTempY += Fifo.pYbuf[i];
		}
		*avgX = SumTempX/Fifo.head;
		*avgY = SumTempY/Fifo.head;
		return TRUE;
	}
	
	for (i = 0; i < Num; i++) {
		SumTempX += Fifo.pXbuf[i];
		SumTempY += Fifo.pYbuf[i];
	}
	
	Dp = Drop;
	checkSmx = 0;
	checkSmy = 0;
	checkBgx = 0;
	checkBgy = 0;

	while (Dp > 0) {
		SmX = 0x0FFF;SmY = 0x0FFF;
		LaX = 0x0;LaY = 0x0;
		SmInX = 0;SmInY = 0;
		LaInX = 0;LaInY = 0;
		for (i =  0; i < Num; i++) {
			if (checkSmx & (1<<i)) {
			} else if (SmX > Fifo.pXbuf[i]) {
				SmX = Fifo.pXbuf[i];
				SmInX= i;
			}
			if (checkSmy & (1<<i)) {
			}else if (SmY > Fifo.pYbuf[i]) {
				SmY = Fifo.pYbuf[i];
				SmInY = i;
			}
			
			if (checkBgx & (1<<i)) {
			} else if (LaX < Fifo.pXbuf[i]) {
				LaX = Fifo.pXbuf[i];
				LaInX = i;
			}
			
			if (checkBgy & (1<<i)) {
			} else if (LaY < Fifo.pYbuf[i]) {
				LaY = Fifo.pYbuf[i];
				LaInY = i;
			}
		}
		if (Dp) {
			SumTempX-= Fifo.pXbuf[SmInX];
			SumTempX-= Fifo.pXbuf[LaInX];
			SumTempY-= Fifo.pYbuf[SmInY];
			SumTempY-= Fifo.pYbuf[LaInY];
			Dp -= 2;
			/*
			printk(KERN_ERR "in filter :SmInX %d,LaInX %d, SmInY %d , LaInY %d!!!\n", SmInX,LaInX, SmInY, LaInY);
			*/
		}
		checkSmx |= 1<<SmInX;
		checkSmy |= 1<<SmInY;
		checkBgx |= 1<<LaInX;
		checkBgy |= 1<<LaInY;
	}
	
	*avgX = SumTempX/(Num-Drop);
	*avgY = SumTempY/(Num-Drop);
	
	return TRUE;
}
static unsigned short uor_avg_XY(unsigned short *pData, int num)
{
	int i, sum = 0, max = 0, min = 0;
	unsigned int avg = 0;
	
	max = pData[0];
	min = pData[1];
	for (i = 0; i < num; i++) {
		if (pData[i] > max)
			max = pData[i];
		if (pData[i] < min)
			min = pData[i];

		sum += pData[i];
	}

	sum -= min;
	sum -=max;
	avg = sum/(num-2);
	
	return avg;
}

static unsigned short uor_avg_DXY(unsigned short *pData, int num)
{
	int i,j,k,swap = 1;
	unsigned int avg;

	for (i = num-1; i > 0 && swap; i--) {
		swap = 0;
		for (j = 0; j < i; j++) {
			if (pData[j] > pData[j+1]) {
				k = pData[j];
				pData[j] = pData[j+1];
				pData[j+1] = k;
				swap = 1;
			}			
		}
	}
	i = num >> 1;
	avg = (pData[i]+pData[i-1]) >> 1;
	return avg;
}

static void uor_read_data(int *X, int *Y, 
		    int *DX, int *DY, 
		    int *Z1, int *Z2)
{
	int i = 0;
	VUINT8 EpBuf[16];
	struct ST_UOR_BUF uor_point;

	for (i = 0; i < gSampleNum; i++) {
		memset(EpBuf, 0, sizeof(EpBuf));
		UOR_IICRead(MSRX_1T, EpBuf, 2);
		uor_point.pXbuf[i] = (EpBuf[0]<<4)|(EpBuf[1]>>4);
	
		UOR_IICRead(MSRY_1T, EpBuf, 2);
		uor_point.pYbuf[i] = (EpBuf[0]<<4)|(EpBuf[1]>>4);
	
		UOR_IICRead(MSRX_2T, EpBuf, 3);
		uor_point.pDXbuf[i] = EpBuf[2];
	
		UOR_IICRead(MSRY_2T, EpBuf, 3);
		uor_point.pDYbuf[i] = EpBuf[2];
	}	

	UOR_IICRead(MSRZ1_1T, EpBuf, 2);
	*Z1 = (EpBuf[0]<<4)|(EpBuf[1]>>4); 

	UOR_IICRead(MSRZ2_1T, EpBuf, 2);
	*Z2 =(EpBuf[0]<<4)|(EpBuf[1]>>4); 

	*X = uor_avg_XY(uor_point.pXbuf,gSampleNum);
	*Y = uor_avg_XY(uor_point.pYbuf,gSampleNum);
	*DX = uor_avg_DXY(uor_point.pDXbuf,gSampleNum);
	*DY = uor_avg_DXY(uor_point.pDYbuf,gSampleNum);

	return;
}

static unsigned int touch_finger(int z2, int z1,
			      int Dx, int Dy,
			      int x, int *Rt)
{	
	unsigned int	R_touch = 0;
	unsigned int   nTouch = 0;

	if (z1 == 0)
		z1 = 1;/*avoid divde by zero*/
	R_touch = (abs(((z2*x)/z1-x)))/4; /*(float)((((float) z2)/((float) z1) -1)*(float)x)/4096;*/

	/*if(x < 100 || R_touch > R_Threshold)*/
	if (x < 100 || check_pen_isup(ts.gpio))//pen is up
		nTouch =  ZERO_TOUCH;	
	else if (((Dx < 256 && Dx > DX_T) || (Dy < 256 && Dy > DY_T)) && (R_touch < R_Threshold2))
		nTouch =  TWO_TOUCH;
	else
		nTouch = ONE_TOUCH;

	*Rt = R_touch;
	return nTouch;

}

static void uor_read_loop(struct work_struct *data)
{
	unsigned int Rt = 0;
	int  x, y, x1, y1, x2, y2;
	int  Dx, Dy, z1, z2;
	unsigned int nTouch = 0;
	int cal_x, cal_y;
	int	dx_coord, dy_coord;
	static unsigned int delay_pendown_time;

	cancel_delayed_work(&delay_penup_wk);
	while (1) {
		delay_pendown_time = wmt_read_oscr();
		uor_read_data(&x, &y, &Dx, &Dy, &z1, &z2);
		/*
		printk("Before Filter: (x,y)=(%d,%d) (dx,dy)=(%d,%d) n_touch %d, (z1,z2)=(%d,%d) \n",
			x, y, Dx, Dy, nTouch, z1, z2);
		*/
		nTouch = touch_finger(z2, z1, Dx, Dy, x,&Rt);
		/*
		printk("Before Filter: (x,y)=(%d,%d) (dx,dy)=(%d,%d) n_touch %d, R_touch %d, (z1,z2)=(%d,%d) \n",i
			x, y, Dx, Dy, nTouch,Rt, z1, z2);
		*/
		if (nTouch == ONE_TOUCH || nTouch == TWO_TOUCH) {	
			ts.xp = x;
			ts.yp = y;

			if (nTouch == TWO_TOUCH) {
				if (two_touch_count < FIRST_TWO_TOUCH_FILTER) {
					/*
					printk("filter for first two touch -(x,y)=(%d,%d) (dx,dy)=(%d,%d),count = %d, FIRST_TWO_TOUCH_FILTER = %d .\n",
						x, y, Dx, Dy,two_touch_count, FIRST_TWO_TOUCH_FILTER);
					*/
					two_touch_count++;
					msleep(5);
					xy_buf1.head = 0;
					xy_buf1.full = 0;
					xy_buf2.head = 0;
					xy_buf2.full = 0;
					continue;/*re-start the loop*/
				} else if ((pre_dx!=0) && (pre_dy!=0) && (abs(Dx - pre_dx) > JITTER_THRESHOLD_DXDY  || abs(Dy - pre_dy) > JITTER_THRESHOLD_DXDY)) { 
					/*
					printk("filter for jitter(dual) --(pre_dx,pre_dy)=(%d,%d) ,(dx,dy)=(%d,%d) , JITTER_THRESHOLD_DXDY = %d.\n", pre_dx, pre_dy , Dx, Dy, JITTER_THRESHOLD_DXDY);
					*/
					msleep(MAX_READ_PERIOD);
					continue;/*re-start the loop*/
				} else {
					if (abs(Dx - pre_dx) < 10)
						Dx = pre_dx;
					if (abs(Dy - pre_dy) < 10)
						Dy = pre_dy;

					dx_coord = ((Dx-DeltaX) & DX_MSK) * DXY_NUM;//8;
					dy_coord =  ((Dy-DeltaY) & DY_MSK) * DXY_NUM;//8;
#ifdef TOUCH2_AVG
					uor_fifo_add(&xy_buf2, dx_coord, dy_coord);
					uor_fifo_avg(xy_buf2, &dx_coord, &dy_coord, WMT_FIFO_LEN, 2);

					uor_fifo_add(&xy_buf1, ts.xp, ts.yp);
					uor_fifo_avg(xy_buf1, &ts.xp, &ts.yp, WMT_FIFO_LEN, 2);
#endif
					/*
					x1 = (ts.xp >= dx_coord) ? (2048- dx_coord) : ts.xp;
					y1 = (ts.yp >= dy_coord) ? (2048 - dy_coord) : ts.yp;
					*/
					x1 = (ts.xp >= dx_coord) ? (2048) : ts.xp;
					y1 = (ts.yp >= dy_coord) ? (2048 - (dy_coord+dx_coord)) : ts.yp;
					/*
					x1 = (ts.xp >= dx_coord) ? (2048- dx_coord) : (ts.xp - dx_coord);
					y1 = (ts.yp >= dy_coord) ? (2048 - dy_coord) : (ts.yp - dy_coord);
					*/
					x2 = (ts.xp >= dx_coord) ? (2048) : ts.xp;
					y2 = (ts.yp >= dy_coord) ? (2048 + (dy_coord+dx_coord)) : ts.yp;

					TouchPanelCalibrateAPoint(x1, y1, &cal_x, &cal_y);
					x1 = cal_x;
					y1 = cal_y;

					TouchPanelCalibrateAPoint(x2, y2, &cal_x, &cal_y);
					x2 = cal_x;
					y2 = cal_y;

					/*
					printk("F=2, (dx=%d,dy=%d),x1 = %4d, y1 = %4d, x2 = %4d, y2 = %4d.\n",dx_coord,dy_coord,x1,y1,x2,y2);
					*/
					input_report_abs(ts.dev, ABS_MT_TOUCH_MAJOR, 600 + (Rt%400));
					input_report_abs(ts.dev, ABS_MT_POSITION_X, x1 );
					input_report_abs(ts.dev, ABS_MT_POSITION_Y, y1);
					input_mt_sync(ts.dev);
	
					input_report_abs(ts.dev, ABS_MT_TOUCH_MAJOR, 600 + (Rt%400));
					input_report_abs(ts.dev, ABS_MT_POSITION_X, x2);
					input_report_abs(ts.dev, ABS_MT_POSITION_Y, y2);
					input_mt_sync(ts.dev);

       					input_sync(ts.dev);
					TWOTouchFlag = 1;
					OneTCountAfter2 = 0;
					gSampleNum = 7;
					pre_dx = Dx;
					pre_dy = Dy;
					msleep(MAX_READ_PERIOD);
				}
			} else if (nTouch == ONE_TOUCH) {
				if ((TWOTouchFlag == 1) && (OneTCountAfter2 < ONETOUCHCountAfter2)) {/*メ奔蛮翴ONETOUCHCountAfter2(Y)掸虫翴*/
					/*
					printk("filter after two touch -- (x,y)=(%d,%d) ,OneTCountAfter2 = %d, ONETOUCHCountAfter2 = %d .\n", x, y, OneTCountAfter2, ONETOUCHCountAfter2);
					*/
					OneTCountAfter2++;
					two_touch_count = 0;
					xy_buf.head = 0;
					xy_buf.full = 0;
					msleep(MAX_READ_PERIOD);
					continue;//re-start the loop
				} else if ((TWOTouchFlag == 0) && (FirstTC < FIRSTTOUCHCOUNT)) {/*メ奔玡FIRSTTOUCHCOUNT(X)掸虫翴*/
					/*
					printk("filter before single touch -- (x,y)=(%d,%d) ,FirstTC = %d, FIRSTTOUCHCOUNT = %d .\n", x, y, FirstTC, FIRSTTOUCHCOUNT);
					*/
					FirstTC++;
					/*
					msleep(2);
					*/
					continue;/*re-start the loop*/
				} else if ((ts.pX != 0) && (ts.pY != 0) && (abs(x - ts.pX) > JITTER_THRESHOLD  || abs(y - ts.pY) > JITTER_THRESHOLD)) {/*single touch point 玡畉禯JITTER_THRESHOLD 玥耾翴 */
					/*
					printk("filter for jitter -- (px,py)=(%ld,%ld) ,(x,y)=(%d,%d) , JITTER_THRESHOLD = %d .\n", ts.pX, ts.pY ,x, y, JITTER_THRESHOLD);
					*/
					ts.pX = x;
					ts.pY = y;
					msleep(MAX_READ_PERIOD);
					continue;/*re-start the loop*/
				} else if (Rt > R_Threshold || ts.xp < 150 || ts.yp > 3950 || ts.yp < 150) {
					msleep(MAX_READ_PERIOD);
					continue;
				} else {
					/*save previous single touch point (raw adc)*/
					ts.pX = ts.xp; 
					ts.pY = ts.yp;

					/*update x,y phy coordinary*/
					wmt_ts_set_rawcoord(ts.xp, ts.yp);

#ifdef TOUCH1_AVG		
					uor_fifo_add(&xy_buf, ts.xp, ts.yp);
					uor_fifo_avg(xy_buf, &ts.xp, &ts.yp, WMT_FIFO_LEN, 2);
#endif

					TouchPanelCalibrateAPoint(ts.xp, ts.yp, &cal_x, &cal_y);
					/*
					printk("F=1, x1 = %-4ld, y1 = %-4ld, x2 = %-4d, y2 = %-4d.\n", ts.xp,ts.yp,0,0);
					*/
					input_report_abs(ts.dev, ABS_MT_TOUCH_MAJOR, 600 + (Rt%400) );
					input_report_abs(ts.dev, ABS_MT_POSITION_X, cal_x);
					input_report_abs(ts.dev, ABS_MT_POSITION_Y, cal_y);
					input_mt_sync(ts.dev);

					input_sync(ts.dev);						
					msleep(MAX_READ_PERIOD);
					gSampleNum = 3;
				}
			}

		} else if (nTouch == ZERO_TOUCH) {
			/*check pen is down*/
			if (!check_pen_isup(ts.gpio)) {
				dbg("get zero touch, but pen is keep down,why??\n");
				continue;		
			}
#if 0
			input_report_abs(ts.dev, ABS_MT_TOUCH_MAJOR, 0 );
			input_mt_sync(ts.dev);
			input_sync(ts.dev);
			dbg("===== pen is up   ======\n");

			if(++ts.touch_cnt > UOR_INIT_PERIOD){
				ts.touch_cnt = 0;
				Init_UOR_HW();
				dbg("######### Initial uor hardware! ########\n");
			}
				
			//reset filter parameters
			FirstTC = 0;
			OneTCountAfter2 = 0;
			TWOTouchFlag = 0;
			two_touch_count = 0;
			ts.xp= 0;
			ts.yp = 0;
			ts.pX = 0;
			ts.pY = 0;
			pre_dx = 0;
			pre_dy = 0;

			xy_buf.full = 0;
			xy_buf.head = 0;

			xy_buf1.full = 0;
			xy_buf1.head = 0;

			xy_buf2.full = 0;
			xy_buf2.head = 0;

			gSampleNum = 3;
					
#endif
			schedule_delayed_work(&delay_penup_wk,
				msecs_to_jiffies(100));
			enable_int_gpio(ts.gpio);
			/*ts.isPendown = 0;*/

			break;
		} else
			printk(KERN_ERR "uor_read_loop(): n_touch state error \n");
	}	
				
}

static irqreturn_t uor_isr(int irq,void *dev_id)
{	
	irqreturn_t ret = IRQ_NONE;
	
	if (!check_int_status_gpio(ts.gpio)) {
		/*dbg("It's not my interrupt!\n");*/
		goto exit;
	}

	/*
	 * It will come in this case,when g-sonsor reset 
	 * gpio3 interrupt reference register.
	 */
	if (!check_int_enable_gpio(ts.gpio)) {
		dbg("Get utk gpio irq, but utk gpio interrupt is disabled,why?\n");
		clean_int_status_gpio(ts.gpio);
		goto exit;
	}

	disable_int_gpio(ts.gpio);
	clean_int_status_gpio(ts.gpio);
	udelay(20);
	if (check_pen_isup(ts.gpio)) {
		dbg("@@@@@ Get irq, But pen is up, why? @@@@\n");
		enable_int_gpio(ts.gpio);
		return IRQ_HANDLED;
	}

	/*printk("===== pen is down ======\n");*/
	if (!queue_work(queue, &work)) {
		dbg("uor work queue start failed,why?\n");
		enable_int_gpio(ts.gpio);
	}
	ret = IRQ_HANDLED;
	
exit:

   	return ret;
}

int uor6x5x_suspend(void)
{
	/* disable int*/
	disable_int_gpio(ts.gpio);
	cancel_delayed_work(&delay_penup_wk);
	return 0;
}

int uor6x5x_resume(void)
{
	int retries = 10;

	/* initializing the uor6x5x again*/
	while (--retries  && (Init_UOR_HW() < 0)) {
		msleep(20);
		printk("Init uor615x failed, now try again!\n");
	}

	if (!retries) {
		printk("ERROR: UTK HW INIT FAILED!\n");
		/*return -EIO;*/
	}

	setup_int_uor_gpio(ts.gpio);
	enable_int_gpio(ts.gpio);
	return 0;
}


static void uor_delay_penup_wk(struct work_struct *data)
{
	input_report_abs(ts.dev, ABS_MT_TOUCH_MAJOR, 0 );
	input_mt_sync(ts.dev);
	input_sync(ts.dev);
	dbg("===== pen is up   ======\n");

	if (++ts.touch_cnt > UOR_INIT_PERIOD) {
		ts.touch_cnt = 0;
		Init_UOR_HW();
		dbg("######### Initial uor hardware! ########\n");
	}
	
	/*reset filter parameters*/
	FirstTC = 0;
	OneTCountAfter2 = 0;
	TWOTouchFlag = 0;
	two_touch_count = 0;
	ts.xp= 0;
	ts.yp = 0;
	ts.pX = 0;
	ts.pY = 0;
	pre_dx = 0;
	pre_dy = 0;

	xy_buf.full = 0;
	xy_buf.head = 0;

	xy_buf1.full = 0;
	xy_buf1.head = 0;

	xy_buf2.full = 0;
	xy_buf2.head = 0;

	gSampleNum = 3;
}

static void parse_gpt_arg(void)
{
	char *varname = "wmt.gpt.ts";
	unsigned int varlen = 160;
	unsigned char buf[160];
	int retval;
	int i = 0;
	retval = wmt_getsyspara(varname, buf, &varlen);
	if (retval != 0)
		return;
	if (buf[i] != 'u')
		return;
	++i;
	if (buf[i] != 'o')
		return;
	++i;
	if (buf[i] != 'r')
		return;
	++i;
	if (buf[i] != '6')
		return;
	++i;
	if (buf[i] != '1')
		return;
	++i;
	if (buf[i] != '5')
		return;
	++i;
	if (buf[i] != '0')
		return;
	i += 2;
	sscanf((buf + i), "%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x",
			&wmt_ts_gpt.bitmap,
			&wmt_ts_gpt.ctraddr,
			&wmt_ts_gpt.ocaddr,
			&wmt_ts_gpt.idaddr,
			&wmt_ts_gpt.peaddr,
			&wmt_ts_gpt.pcaddr,
			&wmt_ts_gpt.itbmp,
			&wmt_ts_gpt.itaddr,
			&wmt_ts_gpt.isbmp,
			&wmt_ts_gpt.isaddr,
			&wmt_ts_gpt.irq);
		
	/*
	printk("wmt.gpt.ts = %x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x\n",
		wmt_ts_gpt.bitmap,
		wmt_ts_gpt.ctraddr,
		wmt_ts_gpt.ocaddr,
		wmt_ts_gpt.idaddr,
		wmt_ts_gpt.peaddr,
		wmt_ts_gpt.pcaddr,
		wmt_ts_gpt.itbmp,
		wmt_ts_gpt.itaddr,
		wmt_ts_gpt.isbmp,
		wmt_ts_gpt.isaddr,
		wmt_ts_gpt.irq);
	*/
}

int uor_init(void)
{
	int ret,retries = 8;
	struct input_dev *input_device;

	parse_gpt_arg();
	memset(&ts, 0x00, sizeof(struct uor_touch_screen_struct));/*init data struct ts*/
	mutex_init(&mutex_i2c_lock);

	input_device = input_allocate_device();
	if (!input_device) {
		printk(KERN_ERR "Unable to allocate the input device !!\n");
		return -ENOMEM;
	}
	ts.dev = input_device;
	ts.dev->evbit[0] = BIT_MASK(EV_SYN) | BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);
	ts.dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	/* multi-touch*/
	input_set_abs_params(ts.dev, ABS_MT_POSITION_X, 0, wmt_ts_get_resolvX(), 0, 0);
	input_set_abs_params(ts.dev, ABS_MT_POSITION_Y, 0, wmt_ts_get_resolvY(), 0, 0);
	input_set_abs_params(ts.dev, ABS_MT_TOUCH_MAJOR, 0, 1000, 0, 0);

	ts.dev->name = "utk_touch";
	ts.dev->phys = "utk_touch";
#if 0
	ts.dev->id.bustype = 0;
	ts.dev->id.vendor = 0;
	ts.dev->id.product = 0;
	ts.dev->id.version = 0;
#endif
	ret = input_register_device(ts.dev);
	if (ret) {
		printk(KERN_ERR "%s: unabled to register input device, ret = %d\n",
			__FUNCTION__, ret);
		goto err;
	}

	INIT_WORK(&work, uor_read_loop);

	/*printk(KERN_ERR "uor.c: before UOR workqueue(gpio irq=%d) !\n",ts.gpio);*/
	queue = create_singlethread_workqueue("uor_touch");
	if (!queue) {
		printk("Create kernel thread failed!\n");
		goto err;
	}

	ts.irq = wmt_ts_gpt.irq;
	ts.gpio = wmt_ts_get_gpionum();
	dbg("UTK Touch INT=gpio%d\n",ts.gpio);
	setup_int_uor_gpio(ts.gpio);
	disable_int_gpio(ts.gpio);

	if (request_irq(ts.irq, uor_isr, IRQF_SHARED, "uor615x", &ts)) {
		printk(KERN_ERR "uor.c: Could not allocate GPIO intrrupt(%d) for touch screen !\n",ts.irq);
		destroy_workqueue(queue);
		goto err;	
	}

	while (--retries && (Init_UOR_HW() < 0)) {
		msleep(50);
		printk("Init uor615x failed, now try again %d\n",retries);
	}
	/*
	printk(KERN_ERR "uor.c: before UOR workqueue(gpio irq=%d) !\n",ts.gpio);
	*/
	/*
	It does'nt matter when init failed here, it will init HW every 5 sec. 
	*/
	if (!retries) {
		printk("ERROR: UTK HW INIT FAILED!\n");
		/*
		destroy_workqueue(queue);
		free_irq(ts.irq,&ts);
		goto err;
		*/
	}

	INIT_DELAYED_WORK(&delay_penup_wk, uor_delay_penup_wk);
	/*
	tsc_timer.data =(void*) &ts;
	*/

	enable_int_gpio(ts.gpio);
	return 0;

err:
	input_free_device(ts.dev);
	return -EIO;
}

void uor_exit(void)
{
	disable_int_gpio(ts.gpio);
	free_irq(ts.irq, &ts);

	cancel_delayed_work(&delay_penup_wk);
	cancel_work_sync(&work);
	destroy_workqueue(queue);
	mutex_destroy(&mutex_i2c_lock);

	input_unregister_device(ts.dev);
	input_free_device(ts.dev);
}


/*
module_init(uor_init);
module_exit(uor_exit);
*/

MODULE_DESCRIPTION("UOR Touchscreen driver");
MODULE_AUTHOR("Ming-Wei Chang <mingwei@uutek.com.tw>");
MODULE_LICENSE("GPL");
