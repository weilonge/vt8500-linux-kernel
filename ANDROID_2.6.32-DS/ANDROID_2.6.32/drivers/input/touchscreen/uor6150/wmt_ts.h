
#ifndef WMT_TSH_201010191758
#define WMT_TSH_201010191758

struct wmt_tshw_para {
	int gpionum;
	int xmax;
	int ymax;

};

typedef struct {
	short pressure;
	short x;
	short y;
	//short millisecs;
} TS_EVENT;


extern  int wmt_ts_get_gpionum(void);
extern  int wmt_ts_iscalibrating(void);
extern  int wmt_ts_get_resolvX(void);
extern  int wmt_ts_get_resolvY(void);
extern  int wmt_ts_set_rawcoord(unsigned short x, unsigned short y);
extern void TouchPanelCalibrateAPoint(
    int   UncalX,     //@PARM The uncalibrated X coordinate
    int   UncalY,     //@PARM The uncalibrated Y coordinate
    int   *pCalX,     //@PARM The calibrated X coordinate
    int   *pCalY      //@PARM The calibrated Y coordinate
    );


#endif



