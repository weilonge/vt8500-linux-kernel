# Building Guide for VT8500 kernel on Ubuntu

# 1. Install arm toolchains.
# for Ubuntu 12.04
sudo apt-get install gcc-arm-linux-gnueabi u-boot-tools

# for Ubuntu 10.04
sudo apt-get install gcc-arm-linux-gnueabi uboot-envtools uboot-mkimage

# 2. Create output dir.
mkdir ./ANDROID_2.6.32-DS/output

# 3. Configure wm8650 config.
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 wm8650_linux_defconfig

# 4. Build uImage
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 menuconfig
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 uImage

# 5. Build modules
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 modules
mkdir ANDROID_2.6.32-DS/modules
INSTALL_MOD_PATH=../modules \
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 modules_install

# A. Clean
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 clean

# B. Record timestamp.
date +'%F_%T' >> ./build.ts && \
make -C ANDROID_2.6.32-DS/ANDROID_2.6.32 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- O=../output -j8 uImage && \
date +'%F_%T' >> ./build.ts

# Reference:
http://pond-weed.com/wmt8650/index.html

