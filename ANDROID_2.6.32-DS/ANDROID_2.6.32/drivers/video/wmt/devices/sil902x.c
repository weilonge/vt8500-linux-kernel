/*++ 
 * linux/drivers/video/wmt/sil902x.c
 * WonderMedia video post processor (VPP) driver
 *
 * Copyright c 2010  WonderMedia  Technologies, Inc.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 2 of the License, or 
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * WonderMedia Technologies, Inc.
 * 4F, 533, Chung-Cheng Road, Hsin-Tien, Taipei 231, R.O.C
--*/

/*
 * ChangeLog
 *
 * 2010-08-05  Sam Shen
 *     * Add License declaration and ChangeLog
 */

#define SIL902X_C
/*--------------------------------------------------------------------* 
SiI902x Software customization for PC platform.
Based on :
	Video input configuration=
		24 bit RGB in + Hsync + Vsync + DE + PCLK 
		rising edge is the 1st clock after DE=High
	Audio input configuration=
		SPDIF, word size, sample frequency all refer to stream header

	SiI902x i2c device address=0x72. ( CI2CA pin=LOW) 
//================== i2c routine =========================
SiI902x i2c max speed is 100KHz.
A version max speed is 400KHz

Data = I2C_ReadByte(TX_SLAVE_ADDR, RegOffset);
I2C_WriteByte(TPI_BASE_ADDR, RegOffset, Data);
I2C_WriteBlock(TPI_BASE_ADDR, TPI_Offset, pData, NBytes);
I2C_ReadBlock(TPI_BASE_ADDR, TPI_Offset, pData, NBytes);

------------------------------------------------------------------------------*/
// #define DEBUG
/*----------------------- DEPENDENCE -----------------------------------------*/
#include <linux/delay.h>
#include "../vout.h"

/*----------------------- PRIVATE MACRO --------------------------------------*/
/* #define  SIL902X_XXXX  xxxx    *//*Example*/

/*----------------------- PRIVATE CONSTANTS ----------------------------------*/
/* #define SIL902X_XXXX    1     *//*Example*/
#define SIL902X_ADDR 	0x72


/*----------------------- PRIVATE TYPE  --------------------------------------*/
/* typedef  xxxx sil902x_xxx_t; *//*Example*/
typedef enum {
	VMODE_640x480,
	VMODE_480p60,
	VMODE_480i60,
	VMODE_576p50,
	VMODE_576i50,
	VMODE_800x600,
	VMODE_1024x768,
	VMODE_720p50,
	VMODE_720p60,
	VMODE_1280x1024,
	VMODE_1400x1050,
	VMODE_1600x1200,
	VMODE_1680x1050,
	VMODE_1080i50,
	VMODE_1080p50,
	VMODE_1080i60,
	VMODE_1080p60,
	VMODE_1920x1200,
	VMODE_MAX
} sil902x_mode_t;

/*----------EXPORTED PRIVATE VARIABLES are defined in sil902x.h  -------------*/
/*----------------------- INTERNAL PRIVATE VARIABLES - -----------------------*/
/* int  sil902x_xxx;        *//*Example*/
sil902x_mode_t sil902x_videoMode;
int sil902x_colfmt;
int sil902x_datawidth;
int sil902x_audio_fmt = 24;
int sil902x_audio_freq = 48000;
int sil902x_audio_channel = 2;
int sil902x_hdmi_edid = 1;

/*--------------------- INTERNAL PRIVATE FUNCTIONS ---------------------------*/
/* void sil902x_xxx(void); *//*Example*/

static char I2C_ReadByte(unsigned int addr,unsigned int index)
{
	char temp;

	vpp_i2c_read(addr,index,&temp,1);
	return temp;
}

static void I2C_ReadBlock(unsigned int addr,unsigned int index,char *buf,int len)
{
	vpp_i2c_read(addr,index,buf,len);
}

static void I2C_WriteByte(unsigned int addr,unsigned int index,char data)
{
	vpp_i2c_write(addr,index,&data,1);
}

static void I2C_WriteBlock(unsigned int addr,unsigned int index,char *buf,int len)
{
	vpp_i2c_write(addr,index,buf,len);
}

/*----------------------- Function Body --------------------------------------*/
// To enable access SiI902x regs, need to write Reg0xC7=00 after power on.
void Enable902xRegAccess(void)
{
	char Data;
	Data = I2C_ReadByte( 0x72, 0x1B );		// Dummy read, clear unfinished i2c cycle if any.

	I2C_WriteByte(0x72, 0xc7, 00);		// Reg0xc7=00 
	I2C_WriteByte(0x72, 0x1E,0);		// wakeup SiI902x to D0 state
// swing control resistor =4.3K if enable internal source termination
// swing control resistor =5.1K if enable internal source termination

	I2C_WriteByte(0x72, 0xBC,0x01);				// internal page 2
	I2C_WriteByte(0x72, 0xBD,0x82);				// index reg 0x82
	Data = I2C_ReadByte( 0x72, 0xBE );
	I2C_WriteByte(0x72, 0xBE,Data | 0x01);			// Backdoor reg0x82=1 Turn on internal source termination
}

//================== Check SiI902x device ID =======================
int IsSiI902x(void)
{
	char Data = 0xff;
	Data = I2C_ReadByte( 0x72, 0x1B );
	if (Data==0xB0)	return 1;		// If reg0x1B=0xB0 , then it's SiI9022 or 9024 chip.
	return 0;
}
//================== Monitor detection =======================
int TestHPD_Connected(void)		// return : True=monitor connected.
{
	char Data;
	Data = I2C_ReadByte( 0x72, 0x3D );
	return (Data & 0x04); 	// Reg0x3D[2] is monitor HOTPLUG detect state. Hi=Monitor connected. Lo=No monitor attached.
}
//================== Read EDID =======================
void ReadEDID(char *buf)		// HDMI compliance Test item 7-1 : During access DDC bus to read EDID , the i2c speed must be less than 100KHz .
{			// SiI9024A i2c speed can be 40-400KHz . Non-A version only up to 100KHz.
	char Data, int_en;
	I2C_WriteByte(0x72, 0xc7, 0x00);				// Is going to access backdoor reg

	I2C_WriteByte(0x72, 0xbc, 0x00);				// program back door page 0, reg 0xF5[6:5]=00
	I2C_WriteByte(0x72, 0xbd, 0xf5);				
  	I2C_WriteByte(0x72, 0xbe, 0x00);				// Prevent DDC NoAck lock the i2c ;
	

	int_en = I2C_ReadByte( 0x72, 0x3c );
	I2C_WriteByte(0x72, 0x3c, ( int_en & 0xfe ));		// disable HPD & Rsen interrupt during read EDID
	Data = I2C_ReadByte( 0x72, 0x1A );
	I2C_WriteByte(0x72, 0x1A, (Data | 04 ));			// set Reg0x1A[2]=1 to request DDC access
	mdelay(1);	// Delay1mS ;
	I2C_WriteByte(0x72, 0x1A, (Data | 06 ));			// set Reg0x1A[2:1]=11, lock i2c bypass to DDC
	mdelay(1);	// Delay1mS ;

	// Adjust i2c speed to less than 100KHz .
	I2C_ReadBlock(0xA0,0x00,buf,128);
	if( buf[0x7E] >= 1 ){
		I2C_ReadBlock(0xA0,0x80,&buf[128],128);
	}
	if( buf[0x7E] >= 2 ){
		vpp_i2c_enhanced_ddc_read(0xA0,0x0,&buf[256],128);
	}
	if( buf[0x7E] >= 3 ){
		vpp_i2c_enhanced_ddc_read(0xA0,0x80,&buf[384],128);
	}
	I2C_WriteByte(0x72, 0x1A, (Data & 0xF9 ));		// clear "request DDC access" bit, stop i2c bypass to DDC, switch i2c to 902x regs bank
									// This write will get NoACK . Don't need to retry

	while ((I2C_ReadByte(0x72, 0x1A) & 0x06) != 00);		// to confirm switch i2c to 902x regs bank success 
	I2C_WriteByte(0x72, 0x3c, int_en );					// restore HPD & Rsen interrupt after read EDID

	// Restore i2c speed to higher speed
}
//================== Active902x===============
sil902x_mode_t sil902x_get_vmode(vout_info_t *info)
{
	sil902x_mode_t mode;
	unsigned int pixclock;

	pixclock = info->timing.pixel_clock;
	mode = VMODE_MAX;
	switch( info->resx ){
		case 640:
    		mode = VMODE_640x480;
			break;
		case 720:
			if( info->resy == 480 ){
				if( pixclock == 27000000 ){
					mode = VMODE_480i60;
				}
				else {
					mode = VMODE_480p60;
				}
			}
			if( info->resy == 576 ){
				if( pixclock == 27000000 ){
				    mode = VMODE_576p50;
				}
				else {
				    mode = VMODE_576i50;
				}
			}
			break;
		case 800:
			if( info->resy == 600 ){
				mode = VMODE_800x600;
			}
			break;
		case 1024:
			if( info->resy == 768 ){
				mode = VMODE_1024x768;
			}
			break;
		case 1280:
			if( info->resy == 720 ){
				if( pixclock == 74250050 ){
					mode = VMODE_720p50;
				}
				else {
					mode = VMODE_720p60;
				}
			}
			if( info->resy == 1024 ){
				mode = VMODE_1280x1024;
			}
			break;
		case 1400:
			if( info->resy == 1050 ){
				mode = VMODE_1400x1050;
			}
			break;
		case 1600:
			if( info->resy == 1200 ){
				mode = VMODE_1600x1200;
			}
			break;
		case 1680:
			if( info->resy == 1050 ){
				mode = VMODE_1680x1050;
			}
			break;
    	case 1920:
			if( info->resy == 1080 ){			
				switch( pixclock ){
					case 74250060:
					    mode = VMODE_1080i60;
						break;
				    case 74250050:
				    	mode = VMODE_1080i50;
						break;
					case 148500000:
				    	mode = VMODE_1080p60;
						break;
				}
			}
			if( info->resy == 1200 ){
				mode = VMODE_1920x1200;
			}
			break;
		default:
			break;
	}

	if( mode == VMODE_MAX ){
		DPRINT("[sil902x] *E* not support %dx%d\n",info->resx,info->resy);
	}
	return mode;
}

char sil902x_get_audio(void)
{
	char Data;

	switch( sil902x_audio_freq ){
		case 32000: Data = 0x08; break;
		case 44100: Data = 0x10; break;
		case 48000: Data = 0x18; break;
		case 88200: Data = 0x20; break;
		case 96000: Data = 0x28; break;
		case 176400: Data = 0x30; break;
		case 192000: Data = 0x38; break;
		default: Data = 0x0; break;
	}

	switch( sil902x_audio_fmt ){
		case 16: Data |= 0x40; break;
		case 20: Data |= 0x80; break;
		case 24: Data |= 0xC0; break;
		default: break;
	}
	return Data;
}

void Active902x(sil902x_mode_t videoMode)
{
	char videoKRHV[8];
	char aviInfoFrame[14];
	char audioInfoFrame[15]={0xc2,0x84,0x01,0x0A,0x71,0,0,0,0,0,0,0,0,0,0};	
	char Data;

	Enable902xRegAccess();

	switch( videoMode ){
		case VMODE_640x480:
			{
			char modeKRHV[8] ={0xd5, 0x09, 0x70, 0x17, 0x20, 0x03, 0x0d, 0x02};	//video mode PCLK,refresh rate,Htotal,Vtotal to reg 00-07 
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_800x600:
			{
			char modeKRHV[8] ={0xa0, 0x0f, 0x70, 0x17, 0x20, 0x04, 0x74, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1024x768:
			{
			char modeKRHV[8] ={0xe0, 0x15, 0x70, 0x17, 0x20, 0x05, 0x20, 0x03};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1280x1024:
			{
			char modeKRHV[8] ={0x30, 0x2a, 0x70, 0x17, 0x98, 0x06, 0x2a, 0x04};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1400x1050:
			{
			char modeKRHV[8] ={0x93, 0x2f, 0x70, 0x17, 0x48, 0x07, 0x41, 0x04};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1600x1200:
			{
			char modeKRHV[8] ={0x48, 0x3f, 0x70, 0x17, 0x70, 0x08, 0xe2, 0x04};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1680x1050:
			{
			char modeKRHV[8] ={0x7c, 0x2e, 0x70, 0x17, 0x30, 0x07, 0x38, 0x04};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1920x1200:
			{
			char modeKRHV[8] ={0x28, 0x3C, 0x70, 0x17, 0x20, 0x08, 0xd3, 0x04};	// reduced blanking mode
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_480p60:
			{
			char modeKRHV[8] ={0x8c, 0x0a, 0x70, 0x17, 0x5a, 0x03, 0x0d, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_576p50:
			{
			char modeKRHV[8] ={0x8c, 0x0a, 0x88, 0x13, 0x60, 0x03, 0x71, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_720p50:
			{
			char modeKRHV[8] ={0x01, 0x1d, 0x88, 0x13, 0xbc, 0x07, 0xee, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_720p60:
			{
			char modeKRHV[8] ={0x01, 0x1d, 0x70, 0x17, 0x72, 0x06, 0xee, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1080i50:
			{
			char modeKRHV[8] ={0x01, 0x1d, 0xC4, 0x09, 0x50, 0x0A, 0x32, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1080p50:
			{
			char modeKRHV[8] ={0x02, 0x3a, 0x88, 0x13, 0x50, 0x0A, 0x65, 0x04};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1080i60:
			{
			char modeKRHV[8] ={0x01, 0x1d, 0xb8, 0x0b, 0x98, 0x08, 0x32, 0x02};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_1080p60:
			{
			char modeKRHV[8] ={0x02, 0x3a, 0x70, 0x17, 0x98, 0x08, 0x65, 0x04};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_480i60:
			{
			char modeKRHV[8] ={0x8c, 0x0a, 0x70, 0x17, 0xB4, 0x06, 0x06, 0x01};
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		case VMODE_576i50:
			{
			char modeKRHV[8] ={0x8c, 0x0a, 0x88, 0x13, 0xC0, 0x06, 0x38, 0x01};      
			memcpy(videoKRHV,modeKRHV,8);
			}
			break;
		default:
			DPRINT("[SIL902x] no support\n");
			return;
	}

	I2C_WriteBlock(0x72, 0, videoKRHV, 8);

	{
	char aviInfoFrame_dvi[14] ={0xd1, 0x0e, 0x08, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } ;	// for DVI or un-recognized mode
	memcpy(aviInfoFrame,aviInfoFrame_dvi,14);
	}
	
	if(sil902x_hdmi_edid){
		I2C_WriteByte(0x72, 0x1A,0x11);				// Turn off TMDS , HDMI mode
		I2C_WriteByte(0x72, 0x26,0x50);				// audio in=SPDIF, mute, refer header 
		I2C_WriteByte(0x72, 0x28,0);				// Set speaker =FL+FR
		I2C_WriteByte(0x72, 0x25,0x03);				// Don't check audio stream 
		I2C_WriteByte(0x72, 0x27,sil902x_get_audio());				// Audio size, sampling frequency, channel number all refer to header 

		// I2S config
		I2C_WriteByte(0x72, 0x1F,0x80);
		I2C_WriteByte(0x72, 0x20,0x90);

		Data = (vpp_get_hdmi_spdif())? 0x40:0x80;	// bit7:6	 01:SPDIF, 10:I2S
		I2C_WriteByte(0x72, 0x26,Data);				// SPDIF, un-mute, refer header 
 
	// Following 3 instruction only for 9022/9024 non-A version, that re-set layout=2 channel audio after unMute audio
		I2C_WriteByte(0x72, 0xBC,0x02);	
		I2C_WriteByte(0x72, 0xBD,0x2F);
		Data = I2C_ReadByte( 0x72, 0xBE );
		I2C_WriteByte(0x72, 0xBE,Data & 0xFD);			// Layout=2 channel
	
		I2C_WriteByte(0x72, 0x1A,0x01);			// Turn on TMDS , encoder format=HDMI
		switch( videoMode ){
			case VMODE_640x480:
				{
				char aviInfoFrame1[14]={ 0x5E, 0x00, 0x10, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_480p60:
				{
				char aviInfoFrame1[14]={ 0x73, 0x0d, 0x68, 0x84, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_576p50:
				{
				char aviInfoFrame1[14]={ 0x64, 0x0d, 0x68, 0x84, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_720p50:
				{
				char aviInfoFrame1[14]={ 0x23, 0x0d, 0xa8, 0x84, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_720p60:
				{
				char aviInfoFrame1[14]={ 0x32, 0x0d, 0xa8, 0x84, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_1080i50:
				{
				char aviInfoFrame1[14]={ 0x22, 0x0d, 0xa8, 0x84, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_1080p50:
				{
				char aviInfoFrame1[14]={ 0x17, 0x0d, 0xa8, 0x84, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_1080i60:
				{
				char aviInfoFrame1[14]={ 0x31, 0x0d, 0xa8, 0x84, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_1080p60:
				{
				char aviInfoFrame1[14]={ 0x27, 0x0c, 0xa8, 0x84, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_480i60:
				{
				char aviInfoFrame1[14]={ 0x50, 0x00, 0x18, 0x00, 0x06, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			case VMODE_576i50:
				{
				char aviInfoFrame1[14]={ 0x41, 0x00, 0x18, 0x00, 0x15, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
				memcpy(aviInfoFrame,aviInfoFrame1,14);
				}
				break;
			default:
				break;
		}

		I2C_WriteBlock(0x72, 0xBF, audioInfoFrame, 15);	// enable & send audio infoframe every frame , 
	}else{	// DVI
		I2C_WriteByte(0x72, 0x1A,0x00);			// Turn on TMDS , encoder format=DVI
	}
	
	Data = (sil902x_datawidth)? 0x70:0x50;		// bit5: 0-12bit, 1-24bit
	I2C_WriteByte(0x72, 0x08,Data);				// 12 bit video in, falling edge, no repeat ; 

	Data = sil902x_colfmt;
	I2C_WriteByte(0x72, 0x09,Data);				// video in=RGB 00b, YUV444 01b, YUV 422 10b
	I2C_WriteByte(0x72, 0x0A,Data);				// video out=RGB 00b, YUV444 01b, YUV 422 10b
	aviInfoFrame[1] = (aviInfoFrame[1] & 0x9F);
	switch( sil902x_colfmt ){
		default:
		case 0x0:	// RGB 00
			break;
		case 0x1:	// 444
			aviInfoFrame[1] |= (0x2 << 5);
			break;
		case 0x2:	// 422
			aviInfoFrame[1] |= (0x1 << 5);
			break;
	}
	I2C_WriteBlock(0x72, 0x0c, aviInfoFrame, 14);	// send AVI infoframe 
}
//===========================================================================================================

// VBIOS or Graphic Driver init902x rotine, set video mode routine , HotPlug interrupt need to call Active902x (videoMode) if HDMI display is needed

//================== go to standby mode ( D2 state=Off TMDS Tx, Off audio codec , i2c still alive)===========
void standby902x(void)
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x1E );			//Reg1E[1:0]=10 binary =standby mode
	I2C_WriteByte(0x72, 0x1E, (Data & 0xFC) | 02);
}

//================== Wakeup to normal operation (D0 state =TMDS Tx ON, audio codec ON)===========
void wakeup902x(void)
{	
	char Data;
	Data = I2C_ReadByte( 0x72, 0x1B );			// Dummy read, clear unfinished i2c cycle if any.
	Data=I2C_ReadByte(0x72, 0x1E);
	I2C_WriteByte(0x72, 0x1E, Data & 0xFC) ;		//Reg1E[1:0]=00 binary =D0 mode
	Data=I2C_ReadByte(0x72, 0x1A);
	I2C_WriteByte(0x72, 0x1E, Data & 0xEF) ;		//Reg1A[4]=0=enable TMDS
}

//================== Suspend 902x to D3 hot , HPD  event can generate int =============
void D3Hot902x(void)
{
	char Data;
	
	I2C_WriteByte(0x72, 0x3C, 00 );			//Disable HPD & RSEN
	I2C_WriteByte(0x72, 0x3D, 0xFF );			//Clear HPD & RSEN pending int
	I2C_WriteByte(0x72, 0x3C, 01 );			//Enable HPD 
								
	Data=I2C_ReadByte(0x72, 0x1E );			//Reg1E[1:0]=11 binary =D3 hot mode
	I2C_WriteByte(0x72, 0x1E, (Data & 0xFC) | 03);	// Go D3(i2c stop working) hot mode <-- Write this Data will get NoAck, 
								// A dummy read to device 0x72 , get NoAck can confirm it's in D3 state
								// HPD can generate int to MCU. MCU HW reset# SiI902x back to normal operation mode
}
//================== Suspend 902x to D3 cold , lowest power , HPD & RSEN CAN'T generate int =============
void D3Cold902x(void)
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x1E );			//Reg1E[2]=1 =declare D3 cold mode
	I2C_WriteByte(0x72, 0x1E, (Data & 0xFC) | 04);
	I2C_WriteByte(0x72, 0x1E, (Data & 0xFC) | 07);	//Go D3 cold mode <-- Write this Data Will get NoAck, but reduce to lowest power 
								// A dummy read to device 0x72 , get NoAck can confirm it's in D3 state
}
//================== Enable HotPlug Interrupt ===========
void EnableHPD_Int(void)
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x3C );			//Reg3C[0]=1  Enable HotPlug interrupt	
	I2C_WriteByte(0x72, 0x3C, Data | 01);
}
//================== Disable HotPlug Interrupt ===========
void DisableHPD_Int(void)
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x3C );			//Reg3C[0]=0  Disable HotPlug interrupt	
	I2C_WriteByte(0x72, 0x3C, Data& 0xFE);
}
//================== Has HotPlug Interrupt pending ===========
int IS_HPD_Int(void)
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x3D );			//Reg3C[0]=1  Enable HotPlug interrupt	
	if (Data & 0x01){
		I2C_WriteByte(0x72, 0x3D, Data | 01 ) ;		//clear pending HotPlug interrupt
		return 1;
	}
	return 0;
}
// ===========Int service routine==================
//======================================================================================================

void Mute_Audio(void)
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x26 );	
	I2C_WriteByte(0x72, 0x26, Data | 0x10 ) ;			
}

//When mute audio, Audio source should keep sending MCLK, BCLK, WS signal. Just stop SD data.
//If change MCLK, BCLK,WS, or change video mode, should call Active902x ( ) to umMute.
//======================================================================================================

void UnMute_Audio(void)
{
	char Data;
	char audioInfoFrame[]={0xc2,0x84,0x01,0x0A,0x71,0,0,0,0,0,0,0,0,0,0};
	
	Data=I2C_ReadByte(0x72, 0x26 );	
	I2C_WriteByte(0x72, 0x26, Data & 0xEF ) ;	
// For non-A version, always need to re-set layout = 0 after unMute audio
// For A version , don't need following 4 instructions
	I2C_WriteByte(0x72, 0xBC,0x02);				// internal page 2
	I2C_WriteByte(0x72, 0xBD,0x2F);				// index reg 0x2F
	Data = I2C_ReadByte( 0x72, 0xBE );
	I2C_WriteByte(0x72, 0xBE,Data & 0xFD);			// Layout=2 channel
	
	I2C_WriteBlock(0x72, 0xBF, audioInfoFrame, 15);				// enable & send audio infoframe every frame , 
}
//======================================================================================================

int IsHDCPRx(void)	// Return :True if Rx has HDCP ; For SiI9024 only ; SiI902x doesn't have HDCP 
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x29 );			//Reg3C[0]=1  Enable HotPlug interrupt	
	return ( Data & 0x02);	
}

//======================================================================================================
void EnabelHDCP(void)	// For SiI9024 only ; SiI902x doesn't have HDCP 
{
	char Data;
	Data=I2C_ReadByte(0x72, 0x2A );			//Reg3C[0]=1  Enable HotPlug interrupt	
	I2C_WriteByte(0x72, 0x2A, Data | 01);
}

// Pls sign SiI9024 Software License Agreement to get SiI9024 source code.
// Follow  the HDCP software procedure to implement HDCP & pass the HDCP compliant test easily, 

int sil902x_check_plugin(int hotplug)
{
	int plugin = 0;
	unsigned char *buf;
	int option = 0; 

	IS_HPD_Int();
	plugin = (TestHPD_Connected())? 1:0; 
	if( hotplug ){
		if (plugin){
			buf = vout_get_edid(VOUT_DVO2HDMI);
			option = edid_parse_option(buf);
			sil902x_hdmi_edid = (option & EDID_OPT_HDMI)? 1:0;
			
			Active902x( sil902x_videoMode) ;
		}
		else {
			standby902x();
		}
	}
	printk("[SIL902X] HDMI plug%s,option 0x%x\n",(plugin)?"in":"out",option);
	vppif_reg32_write((GPIO_BASE_ADDR+0x300), 0x3<<(VPP_VOINT_NO*2),VPP_VOINT_NO*2,2);	// GPIO0 3:rising edge, 2:falling edge
	return plugin;
}

void sil902x_set_power_down(int enable)
{
	return;
	if( enable ){
		D3Cold902x();
	}
	else {
		wakeup902x();
	}
}

int sil902x_set_mode(unsigned int *option)
{
	DBGMSG("option %d,%d\n",option[0],option[1]);

	sil902x_datawidth = (option[1] & 0x1)? 1:0;		// 1-24bit,0-12bit
	switch(option[0]){
		case VDO_COL_FMT_ARGB:
			sil902x_colfmt = 0;
			break;
		case VDO_COL_FMT_YUV444:
			sil902x_colfmt = 0x1;
			break;
		case VDO_COL_FMT_YUV422H:
			sil902x_colfmt = 0x2;
			break;
		default:
			return -1;
	}
	return 0;
}	

int sil902x_config(vout_info_t *info)
{
	sil902x_videoMode = sil902x_get_vmode(info);
	DBGMSG("mode %d\n",sil902x_videoMode);
	Active902x(sil902x_videoMode);
	return 0;
}

int sil902x_get_edid(char *buf)
{
	ReadEDID(buf);
	return 0;
}

int sil902x_set_audio(vout_audio_t *arg)
{
	char Data;

	sil902x_audio_freq = arg->sample_rate;
	sil902x_audio_channel = arg->channel;
	sil902x_audio_fmt = arg->fmt;

	I2C_WriteByte(0x72, 0x27,sil902x_get_audio());				// Audio size, sampling frequency, channel number all refer to header 
		
	Data = (vpp_get_hdmi_spdif())? 0x40:0x80;	// bit7:6	 01:SPDIF, 10:I2S
	I2C_WriteByte(0x72, 0x26,Data);				// SPDIF, un-mute, refer header 
	
//	Active902x( sil902x_videoMode) ;
//	printk("[SIL902X] set audio(freq %d)\n",arg->sample_rate);
	return 0;
}

int sil902x_init(void)
{
	Enable902xRegAccess();
	if( IsSiI902x() == 0 ){
		return -1;
	}
	EnableHPD_Int();
	DPRINT("[SIL902X] HDMI ext device\n");
	return 0;
}
/*----------------------- vout device plugin --------------------------------------*/
vout_dev_ops_t sil902x_vout_dev_ops = {
	.mode = VOUT_DVO2HDMI,

	.init = sil902x_init,
	.set_power_down = sil902x_set_power_down,
	.set_mode = sil902x_set_mode,
	.config = sil902x_config,
	.check_plugin = sil902x_check_plugin,
	.get_edid = sil902x_get_edid,
	.set_audio = sil902x_set_audio,
};

static int sil902x_module_init(void)
{	
	vout_device_register(&sil902x_vout_dev_ops);
	return 0;
} /* End of sil902x_module_init */
module_init(sil902x_module_init);
/*--------------------End of Function Body -----------------------------------*/
#undef SIL902X_C

