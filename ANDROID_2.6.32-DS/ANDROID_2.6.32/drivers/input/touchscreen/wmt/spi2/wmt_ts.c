/*++
	drivers/input/touchscreen/wmt/spi/wmt_ts.c

	Some descriptions of such software. Copyright (c) 2008  WonderMedia Technologies, Inc.

	This program is free software: you can redistribute it and/or modify it under the
	terms of the GNU General Public License as published by the Free Software Foundation,
	either version 2 of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE.  See the GNU General Public License for more details.
	You should have received a copy of the GNU General Public License along with
	this program.  If not, see <http://www.gnu.org/licenses/>.

	WonderMedia Technologies, Inc.
	10F, 529, Chung-Cheng Road, Hsin-Tien, Taipei 231, R.O.C.

	History:
		2009/04/27 First Version

		2009/06/25 Add calibration
		           Add IOCTL 

--*/
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/wait.h>
#include <linux/interrupt.h>
#include <linux/suspend.h>
#include <linux/platform_device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/poll.h>
#include <linux/earlysuspend.h>

#include <asm/mach-types.h>

/*#define DEBUG*/

#include "wmt_ts.h"

#define WMT_TS_NAME	"wmt_spi_ts"
#define POINT_COUNT 1
#define IOCTL_GET_CAL 0x1601
#define IOCTL_SET_CAL 0x1602
#define IOCTL_GET_RAW 0x1603
#define IOCTL_SET_RAW 0x1604
#define IOCTL_SET_RESOLUTION 0x1605
#define IOCTL_DO_FILTER 0x1606
#define IOCTL_SET_FILTER_DISTANCE 0x1607
#define IOCTL_SET_SPI_FREQ 0x1608
#define IOCTL_GET_SPI_FREQ 0x1609
#define IOCTL_SET_DELAY_TIME 0x160A
#define IOCTL_GET_DELAY_TIME 0x160B

#define TS_IOC_MAGIC  't'

#define TS_IOCTL_CAL_START    _IO(TS_IOC_MAGIC,   1)
#define TS_IOCTL_CAL_DONE     _IOW(TS_IOC_MAGIC,  2, int*)
#define TS_IOCTL_GET_RAWDATA  _IOR(TS_IOC_MAGIC,  3, int*)

#define READ_X 0
#define READ_Y 1
#define READ_BATTERY 2

static unsigned short DISPLAY_Y = 479;
static unsigned short DISPLAY_X = 799;
static unsigned short min_x = 300;
static unsigned short min_y = 200;
static unsigned short max_x = 3770;
static unsigned short max_y = 3960;
static unsigned int report_raw_data = 0;
static unsigned int do_filter = 1;
static unsigned int origin_point = 1;/*0:left_top
                                       1:right_top
                                       2:left_bottom
                                       3:right_bottom*/
static unsigned int pre_touched;
static unsigned int read_y;


static unsigned int read_bat = 0;
#if 1
static pm_message_t ts_pm_state  = {
	.event = PM_EVENT_ON,
};
#endif

static unsigned int early_suspend_stage = 0;

struct wmt_point {
	unsigned short x;
	unsigned short y;
};

struct wmt_ts_cal {
	struct wmt_point left_top;
	struct wmt_point left_bottom;
	struct wmt_point right_top;
	struct wmt_point right_bottom;
	int xyswap;
	struct wmt_point display_left_top;
	struct wmt_point display_right_bottom;
	struct wmt_point center;
};
struct private_wmt_ts_s {
	/* module parameters */
	char *buf;
	/* char dev struct */
	struct cdev cdev;
};
static unsigned int touch_count; 
static unsigned int is_cs7146;

static struct ts_device *__initdata ts_devs[] = {
	&hx_ts,
};

static struct private_wmt_ts_s private_ts;
static int private_wmt_dev_major = 160;
static int private_wmt_dev_minor = 0 ;
struct class *class_wmt_ts;
static struct early_suspend wmt_ts_earlysuspend;
static struct wmt_ts_cal cal_points = {
#if 0
		.left_top = {3555, 3935},
		.left_bottom = {220, 3935},
		.right_top = {140, 3730},
		.right_bottom = {210, 165},
		.xyswap = 1,
		.display_left_top = {0, 0},
		.display_right_bottom = {799, 479},
		.center = {2048, 2048},
#endif
		.left_top = {3900, 3750},
		.left_bottom = {3900, 255},
		.right_top = {187, 3800},
		.right_bottom = {140, 260},
		.xyswap = 0,
		.display_left_top = {0, 0},
		.display_right_bottom = {799, 479},
		.center = {2048, 2048},
};


/*android calibration mode*/
struct calibration_parameter g_CalcParam = {
	.a1 = -19171,
	.b1 = -91,
	.c1 = 75261868,
	.a2 = -274,
	.b2 = -12907,
	.c2 = 47631035,
	.delta = 90583,
};
volatile bool g_bCalibrating = true;

static struct wmt_ts_t ts_wmt;
static unsigned int disable_ts = 0;

/* for batrery detect*/
static unsigned short battery_status;
static unsigned int battery_detect_time = 5000;/*5s*/
static unsigned long last_read_battery_time;
static unsigned int detect_battery_en = 1;

#define UBOOT_PARA_USED

extern int wmt_getsyspara(char *varname, unsigned char *varval, int *varlen);
extern int wmt_setsyspara(char *varname, char *varval);
static void parse_arg(void);
#ifdef CONFIG_HAS_EARLYSUSPEND
static void wmt_ts_early_suspend(struct early_suspend *h);
static void wmt_ts_late_resume(struct early_suspend *h);
#endif

/*
 * brief : get calibration information from BOOT ROM
 */
int get_calibration_info(void) {
	int retval;
	char varname[] = "ts_cal";
#ifdef UBOOT_PARA_USED
	int varlen = 80;
#endif
	unsigned char buf[80];
	unsigned int left_top_x, left_top_y, right_top_x, right_top_y;
	unsigned int right_bottom_x, right_bottom_y, left_bottom_x, left_bottom_y;
	unsigned int center_x, center_y;
	unsigned int display_left_top_x, display_left_top_y;
	unsigned int display_right_bottom_x, display_right_bottom_y;
	int xyswap;
#ifdef UBOOT_PARA_USED 
	retval = wmt_getsyspara(varname, buf, &varlen);
#else
	retval = 1;
#endif
	if (retval != 0) {
		printk("Use default Calibration points\n");
		return retval;
	} else
		printk("%s = %s\n",varname, buf);
	sscanf(buf, "%u,%u %u,%u %u,%u %u,%u %u,%u %u,%u %u,%u %d",
	    &left_top_x, &left_top_y,
	    &right_top_x, &right_top_y,
	    &right_bottom_x, &right_bottom_y,
	    &left_bottom_x, &left_bottom_y,
	    &center_x,&center_y,
	    &display_left_top_x, &display_left_top_y,
	    &display_right_bottom_x, &display_right_bottom_y,
	    &xyswap);
	cal_points.left_top.x = (unsigned short)left_top_x;
	cal_points.left_top.y = (unsigned short)left_top_y;
	cal_points.right_top.x = (unsigned short)right_top_x;
	cal_points.right_top.y = (unsigned short)right_top_y;
	cal_points.left_bottom.x = (unsigned short)left_bottom_x;
	cal_points.left_bottom.y = (unsigned short)left_bottom_y;
	cal_points.right_bottom.x = (unsigned short)right_bottom_x;
	cal_points.right_bottom.y = (unsigned short)right_bottom_y;
	cal_points.display_left_top.x = (unsigned short)display_left_top_x;
	cal_points.display_left_top.y = (unsigned short)display_left_top_y;
	cal_points.display_right_bottom.x = (unsigned short)display_right_bottom_x;
	cal_points.display_right_bottom.y = (unsigned short)display_right_bottom_y;
	cal_points.center.x = (unsigned short)center_x;
	cal_points.center.y = (unsigned short)center_y;
	cal_points.xyswap = xyswap;
	return retval;
}
/*
 * brief : set calibration information from BOOT ROM
 */
int set_calibration_info(struct wmt_ts_cal points) {
	int retval;
	char buf[80];
	sprintf(buf, "%u,%u %u,%u %u,%u %u,%u %u,%u %u,%u %u,%u %d",
	   cal_points.left_top.x, cal_points.left_top.y,
	   cal_points.right_top.x, cal_points.right_top.y,
	   cal_points.right_bottom.x, cal_points.right_bottom.y,
	   cal_points.left_bottom.x, cal_points.left_bottom.y,
	   cal_points.center.x, cal_points.center.y,
	   cal_points.display_left_top.x, cal_points.display_left_top.y,
	   cal_points.display_right_bottom.x, cal_points.display_right_bottom.y,
	   cal_points.xyswap
	);
#ifdef UBOOT_PARA_USED
	retval = wmt_setsyspara("ts_cal", buf);
#endif
	retval = 0;

	return retval;
}

static int get_twoD_calibration_info(void) {
	int retval = 0;
	parse_arg();
	return retval;
}
/*
 * brief : set calibration information from BOOT ROM
 */
static int set_twoD_calibration_info(void) {
	int retval;
	char buf[160];
	char *varname = "wmt.io.ts";
	int varlen = 160;
	int i = 0;
	int j = 0;
	retval = wmt_getsyspara(varname, buf, &varlen);
	for (i = 0; i < varlen; ++i) {
		if (buf[i] == ':')
			++j;
		if (j >= 2)
			break;
	}
	++i;
	for (j = i; j < varlen; ++j)
		buf[j] = 0;
	sprintf((buf + i), "%d %d %d %d %d %d %d",
		g_CalcParam.a1,
		g_CalcParam.b1,
		g_CalcParam.c1,
		g_CalcParam.a2,
		g_CalcParam.b2,
		g_CalcParam.c2,
		g_CalcParam.delta
	);
#ifdef UBOOT_PARA_USED
	/*
	retval = wmt_setsyspara("wmt.io.ts.2dcal", buf);
	*/
	retval = wmt_setsyspara("wmt.io.ts", buf);
#endif
	retval = 0;

	return retval;
}

/*
 * Offer infomation to convert
 * Since some touchscreen's origin did not in left-top of display, this function will output some data
 * according to calibraion information which input by user, then convet function can calculate
 * the cooridination which the origin was in left-top.
 */
static int calibration(struct wmt_ts_cal cal_points) {
	if (!cal_points.xyswap) {
		/*find orgin point*/
		if (cal_points.left_top.x < cal_points.right_bottom.x) {
			if (cal_points.left_top.y < cal_points.right_bottom.y)
				origin_point = 0;/*left top*/
			else
				origin_point = 2;/*left bottom*/
		} else {
			if (cal_points.left_top.y < cal_points.right_bottom.y)
				origin_point = 1;
			else
				origin_point = 3;
		}
		if (cal_points.left_top.x < cal_points.right_top.x) {/*orign point in left*/
			/*find mix_x, max_x*/
			if (cal_points.left_top.x < cal_points.left_bottom.x)
				min_x = cal_points.left_top.x;
			else
				min_x = cal_points.left_bottom.x;
			if (cal_points.right_top.x < cal_points.right_bottom.x)
				max_x = cal_points.right_bottom.x;
			else
				max_x = cal_points.right_top.x;
		} else {/*orign point in right*/
			/*find mix_x, max_x*/
			if (cal_points.right_top.x < cal_points.right_bottom.x)
				min_x = cal_points.right_top.x;
			else
				min_x = cal_points.right_bottom.x;
			if (cal_points.left_top.x < cal_points.left_bottom.x)
				max_x = cal_points.left_bottom.x;
			else
				max_x = cal_points.left_top.x;
		} 
		if (cal_points.left_top.y < cal_points.left_bottom.y) {/*orign point in top*/
			if (cal_points.left_top.y < cal_points.right_top.y)
				min_y = cal_points.left_top.y;
			else
				min_y = cal_points.right_top.y;
			if (cal_points.left_bottom.y < cal_points.right_bottom.y)
				max_y = cal_points.right_bottom.y;
			else
				max_y = cal_points.left_bottom.y;
		} else {/*orign point in bottom*/
			if (cal_points.left_bottom.y < cal_points.right_bottom.y)
				min_y = cal_points.left_bottom.y;
			else
				min_y = cal_points.right_bottom.y;
			if (cal_points.left_top.y < cal_points.right_top.y)
				max_y = cal_points.right_top.y;
			else
				max_y = cal_points.left_top.y;
		}
	} else {
		/*find orgin point*/
		if (cal_points.left_top.y < cal_points.right_bottom.y) {
			if (cal_points.left_top.x < cal_points.right_bottom.x)
				origin_point = 0;/*left top*/
			else
				origin_point = 2;/*left bottom*/
		} else {
			if (cal_points.left_top.x < cal_points.right_bottom.x)
				origin_point = 1;
			else
				origin_point = 3;
		}
		if (cal_points.left_top.y < cal_points.right_top.y) {/*orign point in left*/
			/*find mix_x, max_x*/
			if (cal_points.left_top.y < cal_points.left_bottom.y)
				min_y = cal_points.left_top.y;
			else
				min_y = cal_points.left_bottom.y;
			if (cal_points.right_top.y < cal_points.right_bottom.y)
				max_y = cal_points.right_bottom.y;
			else
				max_y = cal_points.right_top.y;
		} else {/*orign point in right*/
			/*find mix_x, max_x*/
			if (cal_points.right_top.y < cal_points.right_bottom.y)
				min_y = cal_points.right_top.y;
			else
				min_y = cal_points.right_bottom.y;
			if (cal_points.left_top.y < cal_points.left_bottom.y)
				max_y = cal_points.left_bottom.y;
			else
				max_y = cal_points.left_top.y;
		} 
		if (cal_points.left_top.x < cal_points.left_bottom.x) {/*orign point in top*/
			if (cal_points.left_top.x < cal_points.right_top.x)
				min_x = cal_points.left_top.x;
			else
				min_x = cal_points.right_top.x;
			if (cal_points.left_bottom.x < cal_points.right_bottom.x)
				max_x = cal_points.right_bottom.x;
			else
				max_x = cal_points.left_bottom.x;
		} else {/*orign point in bottom*/
			if (cal_points.left_bottom.x < cal_points.right_bottom.x)
				min_x = cal_points.left_bottom.x;
			else
				min_x = cal_points.right_bottom.x;
			if (cal_points.left_top.x < cal_points.right_top.x)
				max_x = cal_points.right_top.x;
			else
				max_x = cal_points.left_top.x;
		}
	}
	printk("min_x = %d , max_ x = %d , min_y = %d , max_y = %d , origin_point = %d\n",
	        min_x, max_x, min_y, max_y, origin_point);
	return 0;
}
#if 0
/*
 * this function would convert raw data to cooridination 
 * which origin was in the display's left-top according information
 * from function calibration.
 */
static void convert(u16 *x, u16 *y, struct wmt_ts_cal cal_points, unsigned int origin_point)
{
	DISPLAY_X = cal_points.display_right_bottom.x - cal_points.display_left_top.x + 1;
	DISPLAY_Y = cal_points.display_right_bottom.y - cal_points.display_left_top.y + 1;
	if (origin_point == 0) {
		if (*x >= max_x)
			*x = cal_points.display_right_bottom.x;
		else if (*x < min_x)
			*x = cal_points.display_left_top.x;
		else
			*x = ((*x -min_x) * DISPLAY_X) / (max_x - min_x)
			     + cal_points.display_left_top.x;
		if (*y >= max_y)
			*y = cal_points.display_right_bottom.y;
		else if (*y < min_y)
			*y = cal_points.display_left_top.y;
		else
			*y = ((*y -min_y) * DISPLAY_Y) / (max_y - min_y)
			     + cal_points.display_left_top.y;
	} else if (origin_point == 1) {
		if (*x >= max_x)
			*x = cal_points.display_left_top.x;
		else if (*x < min_x)
			*x = cal_points.display_right_bottom.x;
		else
			*x = cal_points.display_right_bottom.x -
			     ((*x -min_x) * DISPLAY_X) / (max_x - min_x) +
			     cal_points.display_left_top.x;
		if (*y >= max_y)
			*y = cal_points.display_right_bottom.y;
		else if (*y < min_y)
			*y = cal_points.display_left_top.y;
		else
			*y = ((*y -min_y) * DISPLAY_Y) / (max_y - min_y)
			     + cal_points.display_left_top.y;
	} else if (origin_point == 2) {
		if (*x >= max_x)
			*x = cal_points.display_right_bottom.x;
		else if (*x < min_x)
			*x = cal_points.display_left_top.x;
		else
			*x = ((*x -min_x) * DISPLAY_X) / (max_x - min_x)
			     + cal_points.display_left_top.x;
		if (*y >= max_y)
			*y = cal_points.display_left_top.y;
		else if (*y < min_y)
			*y = cal_points.display_right_bottom.y;
		else
			*y = DISPLAY_Y - ((*y -min_y) * DISPLAY_Y) / (max_y - min_y)
			     + cal_points.display_left_top.y;
	} else {
		if (*x >= max_x)
			*x = cal_points.display_left_top.x;
		else if (*x < min_x)
			*x = cal_points.display_right_bottom.x;
		else
			*x = DISPLAY_X - ((*x -min_x) * DISPLAY_X) / (max_x - min_x)
			     + cal_points.display_left_top.x;
		if (*y >= max_y)
			*y = cal_points.display_left_top.y;
		else if (*y < min_y)
			*y = cal_points.display_right_bottom.y;
		else
			*y = DISPLAY_Y - ((*y -min_y) * DISPLAY_Y) / (max_y - min_y)
			     + cal_points.display_left_top.y;
	}
}
#endif

static void twoDconvert(u16 *cal_x, u16 *cal_y )
{
	int UncalX;
	int UncalY;
	int x,y;
	UncalX = (int) *cal_x;
	UncalY = (int) *cal_y;
	x = (g_CalcParam.a1 * UncalX + g_CalcParam.b1 * UncalY +
	     g_CalcParam.c1) / g_CalcParam.delta;
	y = (g_CalcParam.a2 * UncalX + g_CalcParam.b2 * UncalY +
	     g_CalcParam.c2) / g_CalcParam.delta;

	if (x < 0)
		x = 0;

	if (y < 0)
		y = 0;
	*cal_x = (u16)x;
	*cal_y = (u16)y;
	
}

static u16 pre_data[2];
static u16 cal_data[2];
/*
 * This function will report raw data mode or cooridination mode
 * User can choose which mode to report by setting variable-report_raw_data by ioctl
 * User can swap x and y by setting variable xyswap in struct cal_points
 */
static int wmt_ts_read(void)
{
	u16 data[2] = {0, 0};
	g_bCalibrating = true;

	if (ts_wmt.touched) {
		data[0] = pre_data[0];
		data[1] = pre_data[1];
		if (!report_raw_data) {
			if (!cal_points.xyswap) {
				cal_data[0] = pre_data[0];
				cal_data[1] = pre_data[1];
				/*convert(&data[0], &data[1], cal_points, origin_point);*/
				twoDconvert(&data[0], &data[1]);
				if (early_suspend_stage == 0) {
					input_report_abs(ts_wmt.inputdevice, ABS_X, data[0]);
					input_report_abs(ts_wmt.inputdevice, ABS_Y, data[1]);
				}
				DEBUG_TS("(X , Y) = (%d, %d)\n", data[0], data[1]);
			} else {
				cal_data[1] = pre_data[0];
				cal_data[0] = pre_data[1];
				/*convert(&data[1], &data[0], cal_points, origin_point);*/
				twoDconvert(&data[0], &data[1]);
				if (early_suspend_stage == 0) {
					input_report_abs(ts_wmt.inputdevice, ABS_X, data[1]);
					input_report_abs(ts_wmt.inputdevice, ABS_Y, data[0]);
				}
				DEBUG_TS("(X , Y) = (%d, %d)\n", data[1], data[0]);
			}
		} else {
			if (early_suspend_stage == 0) {
				input_report_abs(ts_wmt.inputdevice, ABS_X, data[0]);
				input_report_abs(ts_wmt.inputdevice, ABS_Y, data[1]);
			}
			DEBUG_TS("Raw:(X , Y) = (%d, %d)\n", data[0], data[1]);

		}
		touch_count = 0;
	}

	g_bCalibrating = false;

	DEBUG_TS("wmt_ts_read: read x=%d,y=%d\n", data[0], data[1]);

	return 0;
}

static void wmt_ts_timer(unsigned char *data)
{
	unsigned long flags;
	ts_wmt.touched = 1;
	spin_lock_irqsave(&ts_wmt.lock, flags);
	/*hx_ts_set_ostimer_irq(ts_wmt.ts_delay_time);*/
	if (!ts_wmt.dev->penup())
		ts_wmt.touched = 0;

	if (ts_wmt.touched == 1) {
		if (!read_y) {
			read_y = 1;
			ts_wmt.dev->parse_data(&pre_data[0], data[1], data[2]);
			/*Read Y*/
			ts_wmt.dev->read(READ_Y);
		} else {
			read_y = 0;
			pre_touched = ts_wmt.touched;
			ts_wmt.dev->parse_data(&pre_data[1], data[1], data[2]);
			wmt_ts_read();
			if (early_suspend_stage == 0) {
				input_report_key(ts_wmt.inputdevice, BTN_TOUCH, 1);
				input_sync(ts_wmt.inputdevice);
			}
			ts_wmt.irq_enabled = 1;
			ts_wmt.dev->clear_irq();
			/*enable_irq(ts_wmt.irq);*/
			/*
			if (ts_pm_state.event == PM_EVENT_ON || ts_pm_state.event == PM_EVENT_RESUME)
				hx_ts_enable_penirq();
			*/
			if (early_suspend_stage == 0)
				hx_ts_enable_penirq();
		}
	}
	if ((!ts_wmt.irq_enabled && read_y == 0) || (!ts_wmt.irq_enabled && !ts_wmt.touched)) {
		read_y = 0;
		ts_wmt.irq_enabled = 1;
		ts_wmt.dev->clear_irq();
		/*enable_irq(ts_wmt.irq);*/
		/*
		if (ts_pm_state.event == PM_EVENT_ON || ts_pm_state.event == PM_EVENT_RESUME)
			hx_ts_enable_penirq();
		*/
		if (early_suspend_stage == 0)
			hx_ts_enable_penirq();
	}

	spin_unlock_irqrestore(&ts_wmt.lock, flags);
}

static void wmt_read_bat_status(unsigned char *data)
{
	unsigned long flags;
	unsigned short status;
	spin_lock_irqsave(&ts_wmt.lock, flags);
	ts_wmt.dev->parse_data(&status, data[1], data[2]);
	battery_status = status;
	hx_ts_set_ostimer1_irq(battery_detect_time);
	ts_wmt.dev->clear_irq();
	last_read_battery_time = jiffies;
	read_bat = 0;
	/*
	if (ts_pm_state.event == PM_EVENT_ON || ts_pm_state.event == PM_EVENT_RESUME) {
		ts_wmt.irq_enabled = 1;//Dean
		hx_ts_enable_penirq();
	}
	*/
	if (early_suspend_stage == 0) {
		hx_ts_enable_penirq();
	}
	ts_wmt.irq_enabled = 1;
	/*
	read_bat = 0;
	*/
	spin_unlock_irqrestore(&ts_wmt.lock, flags);
}

unsigned short wmt_read_batstatus_if(void)
{
	return battery_status;
}

EXPORT_SYMBOL(wmt_read_batstatus_if);
/*
 * this function process two irq----ostimer2 and penirq
 * ostimer2 irq: read battary status
 * penirq : read touchscreen point
 */
static irqreturn_t wmt_ts_handler(int irq, void *dev_id)
{
	unsigned long flags;
	static unsigned int the_first_read_bat;

	if (detect_battery_en == 1) {
		if ((irq == IRQ_OST2 && ts_wmt.irq_enabled == 1) || the_first_read_bat == 0) {
			spin_lock_irqsave(&ts_wmt.lock, flags);
			hx_ts_clear_ostimer1_irq();
			hx_ts_disable_penirq();
			ts_wmt.irq_enabled = 0;//Dean
			ts_wmt.dev->clear_irq();
			hx_ts_set_callback(wmt_read_bat_status);
			read_bat = 1;
			ts_wmt.dev->read(READ_BATTERY);
			spin_unlock_irqrestore(&ts_wmt.lock, flags);
			if (the_first_read_bat == 0)
				++the_first_read_bat;
			return IRQ_HANDLED;
		}
		if (irq == IRQ_OST2 && ts_wmt.irq_enabled == 0) {
			spin_lock_irqsave(&ts_wmt.lock, flags);
			hx_ts_clear_ostimer1_irq();
			hx_ts_set_ostimer1_irq(battery_detect_time);
			spin_unlock_irqrestore(&ts_wmt.lock, flags);
			return IRQ_HANDLED;
		}
	}

	spin_lock_irqsave(&ts_wmt.lock, flags);
	if (!hx_ts_read_irqstatus()) {
		spin_unlock_irqrestore(&ts_wmt.lock, flags);
		return IRQ_NONE;
		
	}

	if (!hx_ts_check_irq_en()) {
		spin_unlock_irqrestore(&ts_wmt.lock, flags);
		return IRQ_NONE;
	}
		

	if (detect_battery_en == 1) {
		hx_ts_set_callback(wmt_ts_timer);

		hx_ts_clear_ostimer1_irq();
		hx_ts_set_ostimer1_irq(battery_detect_time);
	}

	ts_wmt.dev->clear_irq();

	ts_wmt.touched = 1;
	hx_ts_set_ostimer_irq(ts_wmt.ts_delay_time);
	/*&ts_wmt.dev->read(READ_X);*/

	/* restart acquire*/
	if (ts_wmt.irq_enabled) {
		ts_wmt.irq_enabled = 0;
		/*Read X first*/
		ts_wmt.dev->read(READ_X);
		/*disable_irq(irq);*/
		hx_ts_disable_penirq();
	}
	spin_unlock_irqrestore(&ts_wmt.lock, flags);

	return IRQ_HANDLED;
}
static irqreturn_t wmt_ts_ostimer_isr(int irq, void *dev_id)
{
	hx_ts_clear_ostimer_irq();
	pre_touched = 0;
	ts_wmt.touched = 0;
	if (early_suspend_stage == 0) {
		input_report_key(ts_wmt.inputdevice, BTN_TOUCH, 0);
		input_sync(ts_wmt.inputdevice);
	}
	read_y = 0;
	if (!ts_wmt.irq_enabled && read_y == 0) {
		ts_wmt.irq_enabled = 1;
		ts_wmt.dev->clear_irq();
		/*enable_irq(ts_wmt.irq);*/
		/*
		if (ts_pm_state.event == PM_EVENT_ON || ts_pm_state.event == PM_EVENT_RESUME)
			hx_ts_enable_penirq();
		*/
		if (early_suspend_stage == 0)
			hx_ts_enable_penirq();
	}

	last_read_battery_time = jiffies - last_read_battery_time;

	if (detect_battery_en == 1) {
		if ((last_read_battery_time * 10) > battery_detect_time) {
			last_read_battery_time = jiffies;
			hx_ts_clear_ostimer1_irq();
			hx_ts_set_ostimer1_irq(1);/*1ms*/
		}
	}

	return IRQ_HANDLED;
}
static int private_wmt_ts_ioctl(
	struct inode *inode,	/*!<; //[IN] a pointer point to struct inode */
	struct file *filp,	/*!<; //[IN] a pointer point to struct file  */
	unsigned int cmd,	/*!<; // please add parameters description her*/
	unsigned long arg	/*!<; // please add parameters description her*/
)
{
	int retval = 0;
	unsigned int data = 0;
	struct wmt_point resolution;
	int nBuff[8];
	
	switch (cmd) {
	case IOCTL_GET_CAL:
		DEBUG_TS("IOCTL_GET_CAL\n");
		if (copy_to_user ((void __user *)arg, &cal_points,
				  sizeof (struct wmt_ts_cal)))
			retval = -EFAULT;
		break;
	case IOCTL_SET_CAL:
		DEBUG_TS("IOCTL_SET_CAL\n");
		if (copy_from_user (&cal_points, (void __user *)arg,
				    sizeof (struct wmt_ts_cal)))
			retval = -EFAULT;
		else {
			calibration(cal_points);
			set_calibration_info(cal_points);
		}
		break; 
	case IOCTL_SET_RAW:
		DEBUG_TS("IOCTL_SET_RAW\n");
		if (copy_from_user(&data, (void __user *)arg,
				    sizeof (unsigned int)))
			retval = -EFAULT;
		else
			report_raw_data = data;
		break;
	case IOCTL_GET_RAW:
		DEBUG_TS("IOCTL_GET_RAW\n");
		data = report_raw_data;
		if (copy_to_user ((void __user *)arg, &data,
				  sizeof (unsigned int)))
			retval = -EFAULT;
		break;
	case IOCTL_SET_RESOLUTION:
		DEBUG_TS("IOCTL_SET_RESOLUTIOM");
		if (copy_from_user (&resolution, (void __user *)arg,
				    sizeof (unsigned int)))
			retval = -EFAULT;
		else {
			DISPLAY_X = resolution.x;
			DISPLAY_Y = resolution.y;
		}
		break;
	case IOCTL_DO_FILTER:
		DEBUG_TS("IOCTL_DO_FILTER\n");
		if (copy_from_user(&data, (void __user *)arg,
				    sizeof (unsigned int)))
			retval = -EFAULT;
		else
			do_filter = data;
		break;
	case IOCTL_SET_FILTER_DISTANCE:
		/*not implement*/
		DEBUG_TS("IOCTL_SET_FILTER_DISTANCE");
		break;
	case IOCTL_SET_SPI_FREQ:
		DEBUG_TS("IOCTL_SET_FREQ");
		if (copy_from_user (&data, (void __user *)arg,
				    sizeof (unsigned int)))
			retval = -EFAULT;
		else
			ts_wmt.dev->set_freq(data);
		break;
	case IOCTL_GET_SPI_FREQ:
		DEBUG_TS("IOCTL_GET_SPI_FREQ\n");
		data = ts_wmt.dev->frequency;
		if (copy_to_user ((void __user *)arg, &data,
				  sizeof (unsigned int)))
			retval = -EFAULT;
		break;
	case IOCTL_SET_DELAY_TIME:
		DEBUG_TS("IOCTL_SET_DELAY_TIME");
		if (copy_from_user (&data, (void __user *)arg,
				    sizeof (unsigned int)))
			retval = -EFAULT;
		else {
			if (ts_wmt.dev->frequency/50) {
				if (data < (unsigned int)(2*(ts_wmt.dev->frequency/50) + 1))
					retval = -EFAULT;
				else
					ts_wmt.ts_delay_time = data;
			} else {
				if (data < (unsigned int)(2*(ts_wmt.dev->frequency/50)))
					retval = -EFAULT;
				else
					ts_wmt.ts_delay_time = data;
			}
		}
		break;
	case IOCTL_GET_DELAY_TIME:
		DEBUG_TS("IOCTL_GET_DELAY_TIME\n");
		data = ts_wmt.ts_delay_time;
		if (copy_to_user((void __user *)arg, &data,
				  sizeof (unsigned int)))
			retval = -EFAULT;
		break;
	case TS_IOCTL_CAL_START:
		DEBUG_TS("TS_IOCTL_CAL_START\n");
		break;
	case TS_IOCTL_CAL_DONE:
		DEBUG_TS("TS_IOCTL_CAL_DONE\n");
		copy_from_user(nBuff, (unsigned int*)arg, 8*sizeof(int));
		if (nBuff[7] == 0) {
			printk("calibration failed\n");
			printk("old g_CalcParam = %d, %d, %d, %d, %d, %d, %d\n",
				g_CalcParam.a1, g_CalcParam.b1, g_CalcParam.c1,
				g_CalcParam.a2, g_CalcParam.b2, g_CalcParam.c2, g_CalcParam.delta);
			return 0;
		}
		g_CalcParam.a1 = nBuff[0];
		g_CalcParam.b1 = nBuff[1];
		g_CalcParam.c1 = nBuff[2];
		g_CalcParam.a2 = nBuff[3];
		g_CalcParam.b2 = nBuff[4];
		g_CalcParam.c2 = nBuff[5];
		g_CalcParam.delta = nBuff[6];
		printk("g_CalcParam = %d, %d, %d, %d, %d, %d, %d\n",
			g_CalcParam.a1, g_CalcParam.b1, g_CalcParam.c1,
			g_CalcParam.a2, g_CalcParam.b2, g_CalcParam.c2, g_CalcParam.delta);
		set_twoD_calibration_info();
		break;
	case TS_IOCTL_GET_RAWDATA:
		DEBUG_TS("TS_IOCTL_GET_RAWDATA\n");
		nBuff[0] = cal_data[0];
		nBuff[1] = cal_data[1];
		copy_to_user((unsigned int*)arg, nBuff, 2*sizeof(int));
		break;
	default:
		retval = -EINVAL;
		break;
	}

	return retval;
}
static int private_wmt_ts_open(
	struct inode *inode,	/*!<; //[IN] a pointer point to struct inode */
	struct file *filp	/*!<; //[IN] a pointer point to struct file  */
)
{
	struct private_wmt_ts_s *dev;
	DEBUG_TS("wmt_ts_open\n");
	dev = container_of(inode->i_cdev, struct private_wmt_ts_s, cdev);
	filp->private_data = dev;
	return 0;
}
static int private_wmt_ts_release(
	struct inode *inode,	/*!<; //[IN] a pointer point to struct inode */
	struct file *filp	/*!<; //[IN] a pointer point to struct file  */
)
{
	DEBUG_TS("wmt_ts_release\n");
	
	return 0;
}
struct file_operations wmt_ts_fops = {
	.owner = THIS_MODULE,
	.open = private_wmt_ts_open,
	.ioctl = private_wmt_ts_ioctl,
	.release = private_wmt_ts_release,
};


static int __init wmt_ts_probe(struct platform_device *pdev)
{
	int i;
	int status = -ENODEV;
	dev_t dev_no;
	struct cdev *cdev;
	dev_no = MKDEV(private_wmt_dev_major, private_wmt_dev_minor);
	cdev = &private_ts.cdev;
	cdev_init(cdev,&wmt_ts_fops);
	cdev_add(cdev, dev_no, 8);
	class_wmt_ts = class_create(THIS_MODULE, "wmtts");
	device_create(class_wmt_ts, NULL ,MKDEV(private_wmt_dev_major,private_wmt_dev_minor),
			NULL, "wmtts");

	memset(&ts_wmt, 0, sizeof(ts_wmt));

	ts_wmt.inputdevice = input_allocate_device();
	if (!ts_wmt.inputdevice)
		return -ENOMEM;


	spin_lock_init(&ts_wmt.lock);
	ts_wmt.callback = wmt_ts_timer;
	ts_wmt.touch_use = is_cs7146;

	for (i = 0; i < ARRAY_SIZE(ts_devs); i++) {
		if (!ts_devs[i] || !ts_devs[i]->probe)
			continue;
		status = ts_devs[i]->probe(&ts_wmt);
		if (status == 0) {
			ts_wmt.dev = ts_devs[i];
			break;
		}
	}

	if (status != 0) {
		input_free_device(ts_wmt.inputdevice);
		return status;
	}

	/* request irq */
	if (ts_wmt.irq != -1) {
		if (request_irq(ts_wmt.irq, wmt_ts_handler,
				ts_wmt.irq_type,
				WMT_TS_NAME, &ts_wmt)) {
			printk(KERN_ERR
	  "wmt_ts.c: Could not allocate touchscreen IRQ!\n");
			ts_wmt.irq = -1;
			ts_wmt.dev->remove();
			input_free_device(ts_wmt.inputdevice);
			return -EINVAL;
		}
		ts_wmt.irq_enabled = 1;
	} else {
		printk(KERN_ERR "wmt_ts.c: No touchscreen IRQ assigned!\n");
		ts_wmt.dev->remove();
		input_free_device(ts_wmt.inputdevice);
		return -EINVAL;
	}

	if (ts_wmt.ostimer_irq != -1) {
		if (request_irq(ts_wmt.ostimer_irq, wmt_ts_ostimer_isr,
				IRQF_SHARED,
				WMT_TS_NAME, &ts_wmt)) {
			printk(KERN_ERR
	  "wmt_ts.c: Could not allocate OSTIMER3 IRQ!\n");
			ts_wmt.irq = -1;
			ts_wmt.dev->remove();
			input_free_device(ts_wmt.inputdevice);
			return -EINVAL;
		}
	}

	if (detect_battery_en == 1) {
		if (ts_wmt.ostimer1_irq != -1) {
			if (request_irq(ts_wmt.ostimer1_irq, wmt_ts_handler,
					IRQF_SHARED,
					WMT_TS_NAME, &ts_wmt)) {
				printk(KERN_ERR
		  "wmt_ts.c: Could not allocate OSTIMER2 IRQ!\n");
				ts_wmt.irq = -1;
				ts_wmt.dev->remove();
				input_free_device(ts_wmt.inputdevice);
				return -EINVAL;
			}
			hx_ts_set_ostimer1_irq(battery_detect_time);
		}
	}

	get_calibration_info();
	set_calibration_info(cal_points);
	calibration(cal_points);

	get_twoD_calibration_info();
	/*
	set_twoD_calibration_info();
	*/

	ts_wmt.inputdevice->name = WMT_TS_NAME;
	ts_wmt.inputdevice->dev.parent = &pdev->dev;
	ts_wmt.inputdevice->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);
	ts_wmt.inputdevice->keybit[BIT_WORD(BTN_TOUCH)] |= BIT_MASK(BTN_TOUCH);
	ts_wmt.inputdevice->absbit[0] =
	    BIT(ABS_X) | BIT(ABS_Y) | BIT(ABS_PRESSURE);
	input_register_device(ts_wmt.inputdevice);

	/*
	ts_wmt.dev->enable();
	*/

	hx_ts_enable_penirq();
#ifdef CONFIG_HAS_EARLYSUSPEND
	wmt_ts_earlysuspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + 1;
	wmt_ts_earlysuspend.suspend = wmt_ts_early_suspend;
	wmt_ts_earlysuspend.resume = wmt_ts_late_resume;
	register_early_suspend(&wmt_ts_earlysuspend);
#endif
	printk(KERN_INFO "WMT touchscreen driver initialized\n");

	return 0;
}

static int wmt_ts_remove(struct platform_device *pdev)
{
	ts_wmt.dev->disable();
	input_unregister_device(ts_wmt.inputdevice);
	if (ts_wmt.irq != -1)
		free_irq(ts_wmt.irq, &ts_wmt);

	ts_wmt.dev->remove();

	return 0;
}

static int wmt_ts_suspend(struct platform_device *pdev, pm_message_t state)
{
	ts_pm_state.event = state.event;

	if (disable_ts == 0) {

		hx_ts_disable_penirq();/*disable penirq at first*/
		if (detect_battery_en == 1) {
			while (read_bat)
				;
			disable_irq(ts_wmt.ostimer1_irq);
		}
		/*wait to read touchscreen complete*/
		while (ts_wmt.irq_enabled == 0)
			;
		ts_wmt.dev->disable();
	}
	return 0;
}

static int wmt_ts_resume(struct platform_device *pdev)
{
	return 0;
}


int wmt_ts_pre_suspend(pm_message_t state)
{
	return 0;
}
EXPORT_SYMBOL(wmt_ts_pre_suspend);

#ifdef CONFIG_HAS_EARLYSUSPEND
static void wmt_ts_early_suspend(struct early_suspend *h)
{
	early_suspend_stage = 1;
        if (disable_ts == 0)
                hx_ts_disable_penirq();/*disable penirq at first*/
}
#endif

int wmt_ts_post_resume(void)
{
	return 0;
}
EXPORT_SYMBOL(wmt_ts_post_resume);

#ifdef CONFIG_HAS_EARLYSUSPEND
static void wmt_ts_late_resume(struct early_suspend *h)
{
	early_suspend_stage = 0;
	if (ts_pm_state.event != PM_EVENT_SUSPEND) {
		hx_ts_enable_penirq();
		return;
	}
		
	if (disable_ts == 0) {
		ts_wmt.dev->enable(&ts_wmt);
		if (detect_battery_en == 1) {
			enable_irq(ts_wmt.ostimer1_irq);
			hx_ts_set_ostimer1_irq(battery_detect_time);
		}
		ts_pm_state.event = PM_EVENT_RESUME;
	}

}
#endif

static void wmt_ts_device_release(struct device *dev)
{
	/* Nothing */
}
static struct platform_driver wmt_ts_driver = {
	.probe 		= wmt_ts_probe,
	.remove 	= wmt_ts_remove,
	.suspend 	= wmt_ts_suspend,
	.resume 	= wmt_ts_resume,
	.driver = {
		.name	= WMT_TS_NAME,
	},
};

static struct platform_device wmt_ts_device = {
	.name 		= WMT_TS_NAME,
	.id 		= -1,
	.dev = {
		.release 	= wmt_ts_device_release,
	},
};

static void parse_arg(void)
{
	int retval;
	unsigned char buf[80];
	unsigned char tmp_buf[80];
	int i = 0;
	int j = 0;
	int varlen = 80;
	char *varname = "wmt.io.ts";
	retval = wmt_getsyspara(varname, buf, &varlen);
	if (retval == 0) {
		for (i = 0; i < 80; ++i) {
			if (buf[i] == ':')
				break;
			disable_ts = (buf[i] - '0' == 0)?1:0;
		}
		++i;
		for (; i < 80; ++i) {
			if (buf[i] == ':')
				break;
			tmp_buf[j] = buf[i];
			++j;
		}
		if (tmp_buf[0] == 'c' && tmp_buf[1] == 's'
			&& tmp_buf[2] == '7' && tmp_buf[3] == '1'
			&& tmp_buf[4] == '4' && tmp_buf[5] == '6')
			is_cs7146 = 1;
		else
			disable_ts = 1;
		++i;
	        sscanf((buf+i), "%d %d %d %d %d %d %d",
		    &g_CalcParam.a1,
		    &g_CalcParam.b1,
		    &g_CalcParam.c1,
		    &g_CalcParam.a2,
		    &g_CalcParam.b2,
		    &g_CalcParam.c2,
		    &g_CalcParam.delta
         	);

	} else
		disable_ts = 1;
	if (is_cs7146 == 0)
		disable_ts = 1;

	/*
	printk("disble_ts = %d\n", disable_ts);
	printk("is_cs7146 = %d\n", is_cs7146);
	printk("%d %d %d %d %d %d %d\n", g_CalcParam.a1,
		g_CalcParam.b1, g_CalcParam.c1, g_CalcParam.a2, g_CalcParam.b2, g_CalcParam.c2, g_CalcParam.delta);
	*/
	
}

static int __init wmt_ts_init(void)
{
	int ret;
	int retval;
	unsigned char buf[80];
#ifdef UBOOT_PARA_USED
	int varlen = 80;
	char *varname1 = "wmt.io.bat";
	parse_arg();

#else
	retval = 1;
#endif
	if (disable_ts == 1)
		return -ENODEV;

#ifdef UBOOT_PARA_USED
	retval = wmt_getsyspara(varname1, buf, &varlen);
#else
	retval = 1;
#endif
	if (retval == 0) {
		retval = sscanf(buf,"%x", &detect_battery_en);
		if (detect_battery_en == 2)
			detect_battery_en = 1;
		else
			detect_battery_en = 0;
		if (*(buf + retval) != '\0')
			sscanf(buf+retval, ":%x", &battery_detect_time);
		
	} else
		detect_battery_en = 0;

	ret = platform_device_register(&wmt_ts_device);
	if (ret != 0)
		return -ENODEV;

	ret = platform_driver_register(&wmt_ts_driver);
	if (ret != 0) {
		platform_device_unregister(&wmt_ts_device);
		return -ENODEV;
	}

	return 0;
}

static void __exit wmt_ts_exit(void)
{
	platform_driver_unregister(&wmt_ts_driver);
	platform_device_unregister(&wmt_ts_device);
}

module_init(wmt_ts_init);
module_exit(wmt_ts_exit);

MODULE_LICENSE("GPL");
