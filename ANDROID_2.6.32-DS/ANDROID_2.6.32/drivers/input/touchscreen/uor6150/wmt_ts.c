#include <linux/unistd.h>
#include <linux/time.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
//#include <asm/semaphore.h>
#include <linux/proc_fs.h>
#include <linux/completion.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/suspend.h>
#include <linux/proc_fs.h>
#include <linux/input.h>
#include <linux/types.h>
#include <linux/platform_device.h>
#include <mach/hardware.h>
#include <asm/uaccess.h>

#include "wmt_ts.h"

///////////////////////////////////////////////////////////////
#define DEBUG_WMT
#undef dbg

#ifdef DEBUG_WMT
	#define dbg(format, arg...) printk(KERN_ALERT format, ## arg)
#else
	#define dbg(format, arg...)
#endif

/////////////////////////////////////////////////////////////////

// commands for ui
#define TS_IOC_MAGIC  't'

#define TS_IOCTL_CAL_START    _IO(TS_IOC_MAGIC,   1)
#define TS_IOCTL_CAL_DONE     _IOW(TS_IOC_MAGIC,  2, int*)
#define TS_IOCTL_GET_RAWDATA  _IOR(TS_IOC_MAGIC,  3, int*)
#define TS_IOCTL_CAL_QUIT	_IOW(TS_IOC_MAGIC,  4, int*)
#define TS_IOC_MAXNR          4

//
#define TS_MAJOR                	11
#define TS_DRIVER_NAME	 	"utk_touch"
#define TS_NAME          	 	"wmtts"

#define EXT_GPIO0   	0
#define EXT_GPIO1   	1
#define EXT_GPIO2   	2
#define EXT_GPIO3   	3
#define EXT_GPIO4	4
#define EXT_GPIO5	5
#define EXT_GPIO6  	6
#define EXT_GPIO7   	7

typedef struct {
    int   a1;
    int   b1;
    int   c1;
    int   a2;
    int   b2;
    int   c2;
    int   delta;
}CALIBRATION_PARAMETER, *PCALIBRATION_PARAMETER;

static int irq_gpio;
static int panelres_x;
static int panelres_y;
static DECLARE_WAIT_QUEUE_HEAD(queue);
static CALIBRATION_PARAMETER g_CalcParam = {
	.a1 = -268,
	.b1 = -30390,
	.c1 = 112163224,
	.a2 = 26358,
	.b2 = 113,
	.c2 = -25567835,
	.delta = 124910,
};
static volatile bool g_bCalibrating = false;
static TS_EVENT g_evLast;
static int g_ModuleInstalled = 1;

static struct class* l_dev_class = NULL;
static struct device *l_clsdevice = NULL;
static unsigned int g_is_uor6150;


/////////////////////////////////////////////////////
//   extrenal function
/////////////////////////////////////////////////////
extern int uor_init(void);
extern void uor_exit(void);
extern int uor6x5x_resume(void);
extern int uor6x5x_suspend(void);
extern int wmt_getsyspara(char *varname, unsigned char *varval, int *varlen);
extern int wmt_setsyspara(char *varname, unsigned char *varval);
/////////////////////////////////////////////////////


/*
static int wmt_getenv2int(char *varname, unsigned char *varval, int varlen, int *pInt);
*/

/*
static unsigned long wmt_strtoul(const char *cp,char **endp,unsigned int base)
{
	unsigned long result = 0,value;

	if (*cp == '0') 
		cp++;
	if (!base) 
		base = 10;
    	while ((*cp)!= ','){
		if(*cp>='0'&&*cp<='9')
			value = *cp - '0';
        	else if(*cp>='a'&&*cp<='f') 
			value = *cp - 'a' + 10;
        	else if(*cp>='A'&&*cp<='F') 
			value = *cp - 'A' + 10;
        	else 
			break;
        	if (value >= base) 
			break;

 		result = result*base + value;
 		cp++;
 	}

	if (endp)
		*endp = (char *)cp;
	
	return result;
}
*/

void TouchPanelCalibrateAPoint(
    int   UncalX,     //@PARM The uncalibrated X coordinate
    int   UncalY,     //@PARM The uncalibrated Y coordinate
    int   *pCalX,     //@PARM The calibrated X coordinate
    int   *pCalY      //@PARM The calibrated Y coordinate
    )
{
	int   x, y;

    	x = (g_CalcParam.a1 * UncalX + g_CalcParam.b1 * UncalY +
         	g_CalcParam.c1) / g_CalcParam.delta;
    	y = (g_CalcParam.a2 * UncalX + g_CalcParam.b2 * UncalY +
         	g_CalcParam.c2) / g_CalcParam.delta;
    	if ( x < 0 )
       	 x = 0;

    	if ( y < 0 )
        	y = 0;

    	*pCalX = x;
    	*pCalY = y;

	return;
}

#if 0
static int wmt_getenv2int(char *varname, unsigned char *varval, int varlen, int *pInt)
{
	int len = varlen;
  	wmt_getsyspara(varname,varval,&len);
    	if (!varval)
		return false;
    	*pInt = wmt_strtoul(varval, NULL, 0);
    	return true;
}
#endif

 int wmt_ts_get_gpionum(void)
{
	return irq_gpio;
}

int wmt_ts_iscalibrating(void)
{
	return g_bCalibrating;
}
int wmt_ts_get_resolvX(void)
{
	return panelres_x;
}

int wmt_ts_get_resolvY(void)
{
	return panelres_y;
}

int wmt_ts_set_rawcoord(unsigned short x, unsigned short y)
{
	g_evLast.x = x;
	g_evLast.y = y;
	return 0;
}

static void wmt_ts_platform_release(struct device *device)
{
    return;
}

static struct platform_device wmt_ts_plt_device = {
    .name           = TS_DRIVER_NAME,
    .id             = 0,
    .dev            = {
        .release = wmt_ts_platform_release,
    },
//    .num_resources  = ARRAY_SIZE(wm9715_ts_resources),
//    .resource       = wm9715_ts_resources,
};

static int wmt_ts_suspend(struct platform_device *pdev, pm_message_t state)
{
	return uor6x5x_suspend();
}
static int wmt_ts_resume(struct platform_device *pdev)
{
	return uor6x5x_resume();
}

static int wmt_ts_probe(struct platform_device *pdev)
{
	return 0;
}

static int wmt_ts_remove(struct platform_device *pdev)
{
	return 0;
}

static struct platform_driver wmt_ts_plt_driver = {
	.driver = {
	             .name = TS_DRIVER_NAME,
		     .owner	= THIS_MODULE,
	 },
	.probe = wmt_ts_probe,
	.remove = wmt_ts_remove,
	.suspend        = wmt_ts_suspend,
	.resume         = wmt_ts_resume,
};

static int wmt_ts_open(struct inode *inode, struct file *filp)
{
	int ret = 0;

	printk(KERN_ALERT "wmt ts driver opening...\n");

	//ts_clear();
	//try_module_get(THIS_MODULE);

    	return ret;
}

static int wmt_ts_close(struct inode *inode, struct file *filp)
{
	printk(KERN_ALERT "wmt ts driver closing...\n");
	//ts_clear();
    	//module_put(THIS_MODULE);

	return 0;
}

static unsigned int wmt_ts_poll(struct file *filp, struct poll_table_struct *wait)
{
#if 0
    	poll_wait(filp, &queue, wait);
    	if ( head != tail )
        	return (POLLIN | POLLRDNORM);
#endif
    return 0;
}

static int set_twoD_calibration_info(void) {
	int retval;
	char buf[160];
	char *varname = "wmt.io.ts";
	int varlen = 160;
	int i = 0;
	int j = 0;
	retval = wmt_getsyspara(varname, buf, &varlen);
	for (i = 0; i < varlen; ++i) {
		if (buf[i] == ':')
			++j;
		if (j >= 2)
			break;
	}
	++i;
	for (j = i; j < varlen; ++j)
		buf[j] = 0;
	sprintf((buf + i), "%d %d %d %d %d %d %d",
		g_CalcParam.a1,
		g_CalcParam.b1,
		g_CalcParam.c1,
		g_CalcParam.a2,
		g_CalcParam.b2,
		g_CalcParam.c2,
		g_CalcParam.delta
	);
	retval = wmt_setsyspara("wmt.io.ts", buf);
	return retval;
}

static int wmt_ts_ioctl(struct inode * node, struct file *dev, unsigned int cmd, unsigned long arg)
{
	int nBuff[8];
	char env_val[96]={0};
	//dbg("wmt_ts_ioctl(node=0x%p, dev=0x%p, cmd=0x%08x, arg=0x%08lx)\n", node, dev, cmd, arg);

	if (_IOC_TYPE(cmd) != TS_IOC_MAGIC){ 
		dbg("CMD ERROR!");
		return -ENOTTY;
	}
	
	if (_IOC_NR(cmd) > TS_IOC_MAXNR){ 
		dbg("NO SUCH IO CMD!\n");
		return -ENOTTY;
	}

	switch (cmd) {
		case TS_IOCTL_CAL_START:
			printk("wmt_ts_ioctl: TS_IOCTL_CAL_START\n");
			g_bCalibrating = true;
			return 0;
			
		case TS_IOCTL_CAL_DONE:
			printk("wmt_ts_ioctl: TS_IOCTL_CAL_DONE\n");
			copy_from_user(nBuff, (unsigned int*)arg, 8*sizeof(int));
			if (nBuff[7] == 0) {
				printk("calibration failed! Dont set\n");
				g_bCalibrating = false;
				return 0;
			}
			g_CalcParam.a1 = nBuff[0];
			g_CalcParam.b1 = nBuff[1];
			g_CalcParam.c1 = nBuff[2];
			g_CalcParam.a2 = nBuff[3];
			g_CalcParam.b2 = nBuff[4];
			g_CalcParam.c2 = nBuff[5];
			g_CalcParam.delta = nBuff[6];
			/*
			if(g_CalcParam.delta == 0)
				g_CalcParam.delta =1;//avoid divide by zero

			sprintf(env_val,"%d %d %d %d %d %d %d",nBuff[0],nBuff[1],nBuff[2],nBuff[3],nBuff[4],nBuff[5],nBuff[6]);
			wmt_setsyspara("wmt.io.ts.2dcal", env_val);
			*/

			set_twoD_calibration_info();
			sprintf(env_val,"%d %d %d %d %d %d %d",nBuff[0],
				nBuff[1],nBuff[2],nBuff[3],nBuff[4],nBuff[5],nBuff[6]);
			printk("Tsc calibrate done data: [%s]\n",env_val);
			
			g_bCalibrating = false;
			return 0;
			
		case TS_IOCTL_CAL_QUIT:
			printk("wmt_ts_ioctl: TS_IOCTL_CAL_QUIT\n");
			copy_from_user(nBuff, (unsigned int*)arg, 7*sizeof(int));
			g_CalcParam.a1 = nBuff[0];
			g_CalcParam.b1 = nBuff[1];
			g_CalcParam.c1 = nBuff[2];
			g_CalcParam.a2 = nBuff[3];
			g_CalcParam.b2 = nBuff[4];
			g_CalcParam.c2 = nBuff[5];
			g_CalcParam.delta = nBuff[6];

			if(g_CalcParam.delta == 0)
				g_CalcParam.delta =1;//avoid divide by zero
			
			printk("cal_quit g_CalcParam = %d, %d, %d, %d, %d, %d, %d\n",
				g_CalcParam.a1, g_CalcParam.b1, g_CalcParam.c1,
				g_CalcParam.a2, g_CalcParam.b2, g_CalcParam.c2, g_CalcParam.delta);
			
			g_bCalibrating = false;
			
			return 0;
			
		case TS_IOCTL_GET_RAWDATA:
			printk("wmt_ts_ioctl: TS_IOCTL_GET_RAWDATA\n");
			if (!g_bCalibrating) 
				return -EINVAL;
			
			nBuff[0] = g_evLast.x;
			nBuff[1] = g_evLast.y;
			copy_to_user((unsigned int*)arg, nBuff, 2*sizeof(int));
			printk("raw data: x=%d, y=%d\n", nBuff[0], nBuff[1]);
			
			return 0;
	}
	
	return -EINVAL;
}

static ssize_t wmt_ts_read(struct file *filp, char *buf, size_t count, loff_t *l)
{

	return 0;
}


static struct file_operations wmt_ts_fops = {
	.read           = wmt_ts_read,
	.poll           = wmt_ts_poll,
	.ioctl          = wmt_ts_ioctl,
	.open           = wmt_ts_open,
	.release        = wmt_ts_close,
};


static void parse_arg(void)
{
	int retval;
	unsigned char buf[80];
	unsigned char tmp_buf[80];
	int i = 0;
	int j = 0;
	int varlen = 80;
	char *varname = "wmt.io.ts";
	retval = wmt_getsyspara(varname, buf, &varlen);
	if (retval == 0) {
		for (i = 0; i < 80; ++i) {
			if (buf[i] == ':')
				break;
			g_ModuleInstalled = (buf[i] - '0' == 1)?1:0;
		}
		++i;
		for (; i < 80; ++i) {
			if (buf[i] == ':')
				break;
			tmp_buf[j] = buf[i];
			++j;
		}
		if (tmp_buf[0] == 'u' && tmp_buf[1] == 'o'
			&& tmp_buf[2] == 'r' && tmp_buf[3] == '6'
			&& tmp_buf[4] == '1' && tmp_buf[5] == '5'
			&& tmp_buf[6] == '0')
			g_is_uor6150 = 1;
		else
			g_ModuleInstalled = 0;
		++i;
	        sscanf((buf+i), "%d %d %d %d %d %d %d",
		    &g_CalcParam.a1,
		    &g_CalcParam.b1,
		    &g_CalcParam.c1,
		    &g_CalcParam.a2,
		    &g_CalcParam.b2,
		    &g_CalcParam.c2,
		    &g_CalcParam.delta
         	);

	} else
		g_ModuleInstalled = 0;
	if (g_is_uor6150 == 0)
		g_ModuleInstalled = 0;

	/*
	printk("disble_ts = %d\n", disable_ts);
	printk("is_cs7146 = %d\n", is_cs7146);
	printk("%d %d %d %d %d %d %d\n", g_CalcParam.a1,
		g_CalcParam.b1, g_CalcParam.c1, g_CalcParam.a2, g_CalcParam.b2, g_CalcParam.c2, g_CalcParam.delta);
	*/
	
}

static int __init wmt_ts_init(void)
{
	unsigned int i= 0;
	int ret = 0;
	//int nBuff[7] = {-96,-17228, 67129972, 11655, 72, -5164056, 78814};//Just for current debug
	
    	// Get u-boot parameter
	parse_arg();
	if (!g_ModuleInstalled || !g_is_uor6150)
		return -ENODEV;
	
	/*
    	wmt_getsyspara("touchirq", retval, &len);
	if(!strcmp(retval,"gpio5"))
		irq_gpio = EXT_GPIO5;
	else if(!strcmp(retval,"gpio4"))
		irq_gpio = EXT_GPIO4;
	else if(!strcmp(retval,"gpio6"))
		irq_gpio = EXT_GPIO6;
	else if(!strcmp(retval,"gpio3"))
		irq_gpio = EXT_GPIO3;	
	else if(!strcmp(retval,"gpio2"))
		irq_gpio = EXT_GPIO2;	
	else if(!strcmp(retval,"gpio1"))
		irq_gpio = EXT_GPIO1;	
	else if(!strcmp(retval,"gpio0"))
		irq_gpio = EXT_GPIO0;	
	else
		irq_gpio = EXT_GPIO0;
	*/
	
	irq_gpio = EXT_GPIO0;

	i = 0;

	g_bCalibrating = false;

	if(g_CalcParam.delta == 0)
		g_CalcParam.delta =1;//avoid divide by zero

	if (uor_init() < 0){
		printk(KERN_ERR "Errors to init uor ts IC!!!\n");
		return -1;
	}
	// Create device node
	if (register_chrdev (TS_MAJOR, TS_NAME, &wmt_ts_fops)) {
		printk (KERN_ERR "wmt touch: unable to get major %d\n", TS_MAJOR);
		return -EIO;
	}	
	
	l_dev_class = class_create(THIS_MODULE, TS_NAME);
	if (IS_ERR(l_dev_class)){
		ret = PTR_ERR(l_dev_class);
		printk(KERN_ERR "Can't class_create touch device !!\n");
		return ret;
	}
	l_clsdevice = device_create(l_dev_class, NULL, MKDEV(TS_MAJOR, 0), NULL, TS_NAME);
	if (IS_ERR(l_clsdevice)){
		ret = PTR_ERR(l_clsdevice);
		printk(KERN_ERR "Failed to create device %s !!!",TS_NAME);
		return ret;
	}
	
	// register device and driver of platform
    	ret = platform_device_register(&wmt_ts_plt_device);
    	if(ret){
		printk("wmt ts plat device register failed!\n");
		return ret;
    	}
    	ret = platform_driver_register(&wmt_ts_plt_driver);
    	if(ret){
        	printk("can not register wm9715 touchscreen driver\n");
        	platform_device_unregister(&wmt_ts_plt_device);
        	return ret;
    	}

	printk("wmt ts driver init ok!\n");

	return ret;
}

static void __exit wmt_ts_exit(void)
{
	dbg("%s\n",__FUNCTION__);
	if(!g_ModuleInstalled)
		return;
	
	uor_exit();
	
	platform_driver_unregister(&wmt_ts_plt_driver);
	platform_device_unregister(&wmt_ts_plt_device);
	device_destroy(l_dev_class, MKDEV(TS_MAJOR, 0));
	unregister_chrdev(TS_MAJOR, TS_NAME);
	class_destroy(l_dev_class);
}


module_init(wmt_ts_init);
module_exit(wmt_ts_exit);

MODULE_LICENSE("GPL");

