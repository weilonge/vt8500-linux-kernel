/*++
	drivers/input/touchscreen/wmt/i2c/wmt_ts.h

	Some descriptions of such software. Copyright (c) 2008  WonderMedia Technologies, Inc.

	This program is free software: you can redistribute it and/or modify it under the
	terms of the GNU General Public License as published by the Free Software Foundation,
	either version 2 of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE.  See the GNU General Public License for more details.
	You should have received a copy of the GNU General Public License along with
	this program.  If not, see <http://www.gnu.org/licenses/>.

	WonderMedia Technologies, Inc.
	10F, 529, Chung-Cheng Road, Hsin-Tien, Taipei 231, R.O.C.

	History:
		2009/04/27 First Version

--*/
#include <linux/errno.h>
#ifndef __WMT_TS_H
#define __WMT_TS_H

#ifdef DEBUG
#define DEBUG_TS(fmt...)   printk(fmt)
#else
#define DEBUG_TS(fmt...)   do { } while (0)
#endif

struct wmt_ts_t;

struct ts_device {
	int  (*probe)   (struct wmt_ts_t *);
	void (*read)    (unsigned int flag);
	void (*enable)  (struct wmt_ts_t *);
	void (*disable) (void);
	void (*remove)  (void);
	int  (*penup)  (void);
	void (*clear_irq) (void);
	void (*parse_data) (u16 *, u8, u8);
	void (*set_freq)  (unsigned int);
	unsigned char wbuf[3];
	unsigned char rbuf[3];
	unsigned int frequency;
};

struct wmt_ts_t{
	struct input_dev *inputdevice;
	struct timer_list ts_timer;      /* Timer for triggering acquisitions*/
	int touched;
	int irq;
	int irq_type;
	int irq_enabled;
	int ostimer_irq;
	int ostimer1_irq;	/*Use for battery detect*/
	unsigned int ts_delay_time; /*for release double check*/
	struct ts_device *dev;
	void (*callback)(unsigned char *);
	spinlock_t lock;
	unsigned int touch_use; /*1:use, 0:no use*/
};

struct calibration_parameter {
	int   a1;
	int   b1;
	int   c1;
	int   a2;
	int   b2;
	int   c2;
	int   delta;
};

extern struct ts_device hx_ts;

void hx_ts_clear_ostimer_irq(void);
void hx_ts_clear_ostimer1_irq(void);
int hx_ts_set_ostimer_irq(unsigned int delay_time);
int hx_ts_set_ostimer1_irq(unsigned int delay_time);

void hx_ts_enable_penirq(void);
void hx_ts_disable_penirq(void);
int hx_ts_read_irqstatus(void);
void hx_ts_set_callback(void (*callback)(unsigned char *));
int hx_ts_check_irq_en(void);

#endif /* __WMT_TS_H */
